import moment from 'moment'

export * from './i18n'
export const localStorage = window.localStorage

export { moment }

import i18n from 'i18next'
import { initReactI18next } from 'react-i18next'

import translationENG from '../../lang/ENG/ENG.json'
import translationINA from '../../lang/INA/INA.json'

const resources = {
  ENG: {
    translation: translationENG
  },
  INA: {
    translation: translationINA
  }
}

i18n.use(initReactI18next).init({
  resources,
  lng: localStorage.getItem('language')
    ? localStorage.getItem('language')
    : 'INA',
  interpolation: {
    escapeValue: false,
    formatSeparator: ','
  },
  react: {
    wait: true
  }
})

export default i18n

// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Pagination, Button, Image } from 'antd'
import { PilarLoading } from 'components'
import { textTranslate, convertDate } from 'utils'
import { GalleryBackground } from 'assets'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'
import { dummyHeaderGallery, dummyGalleryContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const PaginationButtonStyle = { color: '#B0B0B0', margin: '0 0.5em' }

const Gallery = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  // Header Content
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // Gallery Content
  const galleryPer = 3
  const [galleryList, setGalleryList] = useState([])
  const [loadingGalleryList, setLoadingGalleryList] = useState(false)
  const [errorGalleryList, setErrorGalleryList] = useState(false)
  const [galleryPage, setGalleryPage] = useState(1)
  const [galleryTotal, setGalleryTotal] = useState(0)
  // Gallery Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getGalleryList(galleryPage)
    }

    return () => {
      subscribe = false
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [galleryPage])

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      if (dummyHeaderGallery.data) {
        let data = dummyHeaderGallery.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getGalleryList = async (galleryPage) => {
    setLoadingGalleryList(true)
    try {
      if (dummyGalleryContent.data) {
        let data = dummyGalleryContent.data?.albums

        data.map((gallery, galleryIndex) => {
          let refactorImageUrls = []
          gallery.imageUrls.map((photo, photoIndex) => {
            if (photoIndex === galleryIndex) {
              refactorImageUrls.push({
                src: photo,
                class: 'PilarCardTall'
              })
            } else {
              refactorImageUrls.push({
                src: photo,
                class: ''
              })
            }
            return null
          })
          gallery.imageUrls = refactorImageUrls
          return null
        })

        setGalleryList(data)
        setGalleryTotal(getGalleryList?.data?.pagination?.totalCount)
        setErrorGalleryList(false)
      } else {
        setErrorGalleryList(true)
      }
    } catch (error) {
      setErrorGalleryList(true)
    }
    setLoadingGalleryList(false)
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl
                ? headerContent.imageUrl
                : GalleryBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderCustomGalleryGrid = ({ photos }) => {
    return (
      <Col span={24}>
        <div className='PilarPhotoGrid'>
          {photos.map((data, index) => {
            return (
              <div key={index} className={`PilarCard ${data.class}`}>
                <Image className='PilarCardImage' src={data.src} alt='' />
              </div>
            )
          })}
        </div>
      </Col>
    )
  }

  const RenderGalleryContent = (props) => {
    const { data, dataIndex } = props
    return (
      <Row className='WrapperContentClass'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Title level={2} style={{ ...TitleStyle, textAlign: 'center' }}>
            {textTranslate(
              language,
              data?.eng?.title ? data?.eng?.title : '',
              data?.ina?.title ? data?.ina?.title : ''
            )}
          </Title>
        </Col>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Title
            level={5}
            style={{ ...TitleStyle, textAlign: 'center', color: '#A6A6A6' }}
          >
            {convertDate(data.createdAt)}
          </Title>
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ textAlign: 'left' }}
        >
          <Text>
            <div
              dangerouslySetInnerHTML={{
                __html: textTranslate(
                  language,
                  data?.eng?.description ? data?.eng?.description : '',
                  data?.ina?.description ? data?.ina?.description : ''
                )
              }}
            ></div>
          </Text>
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ marginTop: '2em' }}
        >
          <Row>
            <Col span={24}>
              <RenderCustomGalleryGrid photos={data.imageUrls} />
            </Col>
          </Row>
        </Col>
        {dataIndex < galleryList.length - 1 ? (
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            className='WrapperDividerClass'
          />
        ) : null}
      </Row>
    )
  }

  const RenderGalleryList = () => {
    if (errorGalleryList) {
      return <RenderErrorGalleryList />
    } else if (!loadingGalleryList && !errorGalleryList) {
      return galleryList.map((data, index) => {
        return (
          <div key={index}>
            <RenderGalleryContent data={data} dataIndex={index} />
          </div>
        )
      })
    } else {
      return <RenderLoadingGalleryList />
    }
  }

  const RenderLoadingGalleryList = () => {
    let dummyLoading = [0, 1]
    return (
      <Row className='WrapperContentClass' gutter={[16, 16]}>
        <Col offset={6} span={12}>
          <PilarLoading type='black' height='20px' borderRadius='5px' />
        </Col>
        <Col offset={9} span={6}>
          <PilarLoading type='black' height='20px' borderRadius='5px' />
        </Col>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          {dummyLoading.map((data) => {
            return (
              <div key={data} style={{ marginBottom: '10px' }}>
                <PilarLoading type='black' height='20px' borderRadius='5px' />
              </div>
            )
          })}
        </Col>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <PilarLoading type='black' height='200px' borderRadius='10px' />
        </Col>
      </Row>
    )
  }

  const RenderErrorGalleryList = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col span={24} className='PilarErrorLoadingWrapper'>
          <RenderLoadingGalleryList />
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingGalleryList}
              onClick={() => getGalleryList(galleryPage)}
            >
              {t('button.reload')}
            </Button>
          </div>
        </Col>
      </Row>
    )
  }

  const itemRender = (current, type, originalElement) => {
    if (type === 'prev') {
      return <span style={PaginationButtonStyle}>{t('pagination.prev')}</span>
    }
    if (type === 'next') {
      return <span style={PaginationButtonStyle}>{t('pagination.next')}</span>
    }
    return originalElement
  }

  const RenderPagination = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ textAlign: 'center' }}
        >
          <Pagination
            className='PilarPagination'
            responsive
            hideOnSinglePage
            current={galleryPage}
            total={galleryTotal}
            pageSize={galleryPer}
            itemRender={itemRender}
            onChange={(page) => {
              setGalleryPage(page)
            }}
          />
        </Col>
      </Row>
    )
  }

  return (
    <div>
      <RenderHeaderContent />
      <RenderGalleryList />
      <RenderPagination />
    </div>
  )
}

export default Gallery

import { Gallery1, Gallery2, Gallery3, Gallery4, Gallery5 } from 'assets'

const GalleryDummy = [
  {
    galleryId: 1,
    galleryName: 'Bandung Gathering Program',
    galleryDate: 'December 2019',
    galleryDesc:
      'Bandung Gathering program adalah acara tahunan kantor yang berguna untuk bertemu, bermain bersama dan membangun komunikasi tim yang lebih bagus kedepannya. Semakin kompak dan semakin bersemangat untuk memulai tahun depan',
    galleryPhoto: [
      { src: Gallery1, class: 'PilarCardTall' },
      { src: Gallery2, class: '' },
      { src: Gallery3, class: '' },
      { src: Gallery4, class: '' },
      { src: Gallery5, class: '' }
    ]
  },
  {
    galleryId: 2,
    galleryName: 'Malang Gathering Program',
    galleryDate: 'December 2018',
    galleryDesc:
      'Malang Gathering program adalah acara tahunan kantor yang berguna untuk bertemu, bermain bersama dan membangun komunikasi tim yang lebih bagus kedepannya. Semakin kompak dan semakin bersemangat untuk memulai tahun depan',
    galleryPhoto: [
      { src: Gallery2, class: '' },
      { src: Gallery3, class: 'PilarCardTall' },
      { src: Gallery1, class: '' },
      { src: Gallery4, class: '' },
      { src: Gallery5, class: '' }
    ]
  },
  {
    galleryId: 3,
    galleryName: 'Yogyakarta Gathering Program',
    galleryDate: 'November 2017',
    galleryDesc:
      'Yogyakarta Gathering program adalah acara tahunan kantor yang berguna untuk bertemu, bermain bersama dan membangun komunikasi tim yang lebih bagus kedepannya. Semakin kompak dan semakin bersemangat untuk memulai tahun depan',
    galleryPhoto: [
      { src: Gallery2, class: '' },
      { src: Gallery1, class: '' },
      { src: Gallery3, class: 'PilarCardTall' },
      { src: Gallery4, class: '' },
      { src: Gallery5, class: '' }
    ]
  }
]

export { GalleryDummy }

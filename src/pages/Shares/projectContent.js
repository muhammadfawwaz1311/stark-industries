// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Image } from 'antd'
import { PilarLoading } from 'components'
import { textTranslate, goTo } from 'utils'
import { useTranslation } from 'react-i18next'
import { LandingPageServices } from 'services'
import { LanguageContext } from 'context'
import { ProjectHomeCard } from 'assets'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const PilarButtonStyle = {
  marginTop: '1em',
  padding: '0.5em 1em',
  height: 'auto',
  width: 'auto',
  backgroundColor: '#E7B12E',
  color: '#fff',
  border: 0
}
const ProjectContentStyle = {
  position: 'relative',
  width: '100%',
  height: '100%',
  backgroundImage: `url(${ProjectHomeCard})`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center'
}

const ProjectContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Project Content
  const [landing, setLanding] = useState({})
  const [loadingLanding, setLoadingLanding] = useState(false)
  const [errorLanding, setErrorLanding] = useState(false)
  // Project Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getProjectContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getProjectContent = async () => {
    setLoadingLanding(true)
    try {
      const getProjectContent = await LandingPageServices.getProjectImagesContent()
      if (getProjectContent && getProjectContent.status === 200) {
        let data = getProjectContent?.data?.data
        data.imageUrls = data?.imageUrls.slice(0, 1)
        setLanding(data)
        setErrorLanding(false)
      } else {
        setErrorLanding(true)
      }
    } catch (error) {
      setErrorLanding(true)
    }
    setLoadingLanding(false)
  }

  const handleClickMenu = () => {
    const params = {
      location: '/project'
    }
    goTo(params)
  }

  const RenderProjectContent = () => {
    if (!loadingLanding && !errorLanding) {
      if (Object.keys(landing).length !== 0) {
        return (
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            style={{ marginTop: '2em', marginBottom: '1em' }}
          >
            <Row gutter={[16, 16]}>
              <Col xs={24} md={12} lg={12}>
                <Row style={{ width: '100%', height: '100%' }}>
                  {landing.imageUrls.map((data, index) => {
                    return (
                      <Image
                        className='ProjectHomeImageWrap'
                        key={index}
                        src={data}
                        alt=''
                      />
                    )
                  })}
                </Row>
              </Col>
              <Col xs={24} md={12} lg={12} style={{ marginBottom: '1em' }}>
                <Col span={24}>
                  <Title level={4} style={{ ...TitleStyle, color: '#fff' }}>
                    {textTranslate(
                      language,
                      landing?.eng?.title,
                      landing?.ina?.title
                    )}
                  </Title>
                </Col>
                <Col span={24}>
                  <Text style={{ color: '#fff', textAlign: 'left' }}>
                    <div
                      dangerouslySetInnerHTML={{
                        __html: textTranslate(
                          language,
                          landing?.eng?.description,
                          landing?.ina?.description
                        )
                      }}
                    ></div>
                  </Text>
                </Col>
                <Col span={24}>
                  <Button
                    shape='round'
                    size={'small'}
                    style={PilarButtonStyle}
                    onClick={() => {
                      handleClickMenu()
                    }}
                  >
                    <Text style={{ color: '#fff' }}>
                      {t('button.seeOurProject')}
                    </Text>
                  </Button>
                </Col>
              </Col>
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorProjectContent />
    }
  }

  const RenderErrorProjectContent = () => {
    let dummyLoading = [0, 1, 2, 3]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Row gutter={[16, 16]} style={{ margin: 0 }}>
          <Col
            xs={{ span: 24, order: 1 }}
            md={{ span: 8, order: 2 }}
            lg={{ span: 8, order: 2 }}
          >
            <Row>
              <Col span={12} style={{ marginBottom: '30px' }}>
                <PilarLoading type='black' height='20px' borderRadius='5px' />
              </Col>
              <Col span={24} style={{ color: '#565656', textAlign: 'left' }}>
                {dummyLoading.map((data) => {
                  return (
                    <div
                      key={data}
                      style={
                        data < dummyLoading.length - 1
                          ? { marginBottom: '10px' }
                          : null
                      }
                    >
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </div>
                  )
                })}
              </Col>
            </Row>
          </Col>
          <Col
            xs={{ span: 24, order: 2 }}
            md={{ span: 16, order: 1 }}
            lg={{ span: 16, order: 1 }}
            className='PilarErrorLoadingWrapper'
          >
            <PilarLoading type='black' height='160px' borderRadius='5px' />
          </Col>
        </Row>
        {errorLanding ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLanding}
              onClick={() => getProjectContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingLanding && !errorLanding && Object.keys(landing).length === 0) {
      return null
    } else {
      return (
        <Row
          className='WrapperContentClass ProjectHomeContentClass'
          style={ProjectContentStyle}
        >
          <RenderProjectContent />
        </Row>
      )
    }
  }

  return <RenderWrapperContent />
}

export default ProjectContent

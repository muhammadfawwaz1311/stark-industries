// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Carousel, Image } from 'antd'
import { PilarLoading } from 'components'
import { checkDevice } from 'utils'
import { useTranslation } from 'react-i18next'
import { dummyPartnerContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }

const PartnerContent = () => {
  const { t } = useTranslation()

  // Partner Content
  const [partner, setPartner] = useState({})
  const [loadingPartner, setLoadingPartner] = useState(false)
  const [errorPartner, setErrorPartner] = useState(false)
  // Partner Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getPartner()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getPartner = async () => {
    setLoadingPartner(true)
    try {
      if (dummyPartnerContent.data) {
        let data = dummyPartnerContent.data.partners
        let partnerArr = [],
          maxSize = 5

        // Split Partner
        while (data.length) {
          partnerArr.push(data.splice(0, maxSize))
        }
        // Split Partner

        setPartner(partnerArr)
        setErrorPartner(false)
      } else {
        setErrorPartner(true)
      }
    } catch (error) {
      setErrorPartner(true)
    }
    setLoadingPartner(false)
  }

  const RenderPartnerContent = () => {
    if (!loadingPartner && !errorPartner) {
      if (Object.keys(partner).length !== 0) {
        return (
          <Row className='WrapperContentClass'>
            <Col xs={24} md={12} lg={{ offset: 2, span: 10 }}>
              <Title level={3} style={{ ...TitleStyle, color: '#E7B12E' }}>
                {t('partner.title')}
              </Title>
            </Col>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Carousel
                className='PilarCarouselClass MissionCarouselClass'
                autoplay
                autoplaySpeed={5000}
              >
                {partner.map((slider, index) => {
                  return (
                    <Col key={index} span={24}>
                      <Row
                        gutter={[32, 32]}
                        style={{ justifyContent: 'center' }}
                      >
                        {slider.map((partner) => {
                          return (
                            <Col
                              flex='0 0 20%'
                              key={partner.id}
                              style={{
                                textAlign: 'center',
                                alignSelf: 'center'
                              }}
                            >
                              <Image
                                src={partner.imageUrl}
                                preview={false}
                              ></Image>
                            </Col>
                          )
                        })}
                      </Row>
                    </Col>
                  )
                })}
              </Carousel>
            </Col>
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingPartnerContent />
    }
  }

  const RenderErrorLoadingPartnerContent = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2, 3, 4]
    let dummyText = [0, 1, 2]

    return (
      <div className='PilarErrorLoadingWrapper'>
        <Row className='WrapperContentClass'>
          <Col offset={8} span={8} style={{ marginBottom: '20px' }}>
            <PilarLoading type='black' height='20px' borderRadius='5px' />
          </Col>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Row gutter={[32, 16]}>
              {dummyLoading.map((data) => {
                return (
                  <Col
                    flex={checkDevice() === 'Mobile' ? '0 0 100%' : '0 0 20%'}
                    key={data}
                  >
                    {dummyText.map((text) => {
                      return (
                        <div key={text} style={{ marginBottom: '10px' }}>
                          <PilarLoading
                            type='black'
                            height='20px'
                            borderRadius='5px'
                          />
                        </div>
                      )
                    })}
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
        {errorPartner ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingPartner}
              onClick={() => getPartner()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </div>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingPartner && !errorPartner && Object.keys(partner).length === 0) {
      return null
    } else {
      return <RenderPartnerContent />
    }
  }

  return <RenderWrapperContent />
}

export default PartnerContent

// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Image } from 'antd'
import { PilarLoading } from 'components'
import { goTo } from 'utils'
import { useTranslation } from 'react-i18next'
import { RightOutlined } from '@ant-design/icons'
import { dummyGalleryHome } from 'assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const MoreContentStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  height: '100%',
  width: 'fit-content',
  cursor: 'pointer',
  marginLeft: 'auto'
}

const GalleryContent = () => {
  const { t } = useTranslation()

  // Gallery Content
  // const galleryPage = 1
  // const galleryPer = 1
  const [gallery, setGallery] = useState([])
  const [loadingGallery, setLoadingGallery] = useState(false)
  const [errorGallery, setErrorGallery] = useState(false)
  // Gallery Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getGallery()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getGallery = async () => {
    setLoadingGallery(true)
    try {
      if (dummyGalleryHome.data) {
        let data = dummyGalleryHome.data.albums
        data.map((gallery, galleryIndex) => {
          let refactorImageUrls = []
          gallery.imageUrls.map((photo, photoIndex) => {
            if (photoIndex === galleryIndex) {
              refactorImageUrls.push({
                src: photo,
                class: 'PilarCardTall'
              })
            } else {
              refactorImageUrls.push({
                src: photo,
                class: ''
              })
            }
            return null
          })
          gallery.imageUrls = refactorImageUrls
          return null
        })
        setGallery(data)
        setErrorGallery(false)
      } else {
        setErrorGallery(true)
      }
    } catch (error) {
      setErrorGallery(true)
    }
    setLoadingGallery(false)
  }

  const handleClickMenu = () => {
    const params = {
      location: '/gallery'
    }
    goTo(params)
  }

  const RenderGalleryContent = () => {
    if (!loadingGallery && !errorGallery) {
      if (gallery.length !== 0) {
        return (
          <Row className='WrapperContentClass'>
            <Col xs={12} md={12} lg={{ offset: 2, span: 10 }}>
              <Title level={2} style={TitleStyle}>
                {t('gallery.title')}
              </Title>
            </Col>
            <Col xs={12} md={12} lg={10} onClick={() => handleClickMenu()}>
              <div style={MoreContentStyle}>
                <Text style={{ color: '#e7b12e' }}>{t('button.seeAll')}</Text>
                <RightOutlined
                  style={{ color: '#e7b12e', marginLeft: '0.5em' }}
                />
              </div>
            </Col>
            {gallery?.map((data) => {
              return (
                <Col xs={24} md={24} lg={{ offset: 2, span: 20 }} key={data.id}>
                  <RenderCustomGalleryGrid photos={data?.imageUrls} />
                </Col>
              )
            })}
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingGalleryContent />
    }
  }

  const RenderCustomGalleryGrid = ({ photos }) => {
    return (
      <Col span={24}>
        <div className='PilarPhotoGrid'>
          {photos?.map((data, index) => {
            return (
              <div key={index} className={`PilarCard ${data.class}`}>
                <Image className='PilarCardImage' src={data.src} alt='' />
              </div>
            )
          })}
        </div>
      </Col>
    )
  }

  const RenderErrorLoadingGalleryContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <Row className='WrapperContentClass'>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <PilarLoading type='black' height='200px' borderRadius='10px' />
          </Col>
        </Row>
        {errorGallery ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingGallery}
              onClick={() => getGallery()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </div>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingGallery && !errorGallery && gallery.length === 0) {
      return null
    } else {
      return <RenderGalleryContent />
    }
  }

  return <RenderWrapperContent />
}

export default GalleryContent

// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button } from 'antd'
import { PilarLoading, CSRCard } from 'components'
import { checkDevice, goTo } from 'utils'
import { useTranslation } from 'react-i18next'
import { CSRService } from 'services'
import { RightOutlined } from '@ant-design/icons'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const MoreContentStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  height: '100%',
  width: 'fit-content',
  cursor: 'pointer',
  marginLeft: 'auto'
}

const CSRContent = () => {
  const { t } = useTranslation()

  // CSR Content
  const csrPage = 1
  const csrPer = 3
  const orderBy = 'createdAt'
  const dir = 'desc'
  const [csr, setCSR] = useState([])
  const [loadingCSR, setLoadingCSR] = useState(false)
  const [errorCSR, setErrorCSR] = useState(false)
  // CSR Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getCSR()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getCSR = async () => {
    setLoadingCSR(true)
    try {
      const getCSR = await CSRService.getCSRList(csrPage, csrPer, orderBy, dir)
      if (getCSR && getCSR.status === 200) {
        let data = getCSR?.data?.data?.csrs
        setCSR(data)
        setErrorCSR(false)
      } else {
        setErrorCSR(true)
      }
    } catch (error) {
      setErrorCSR(true)
    }
    setLoadingCSR(false)
  }

  const handleClickMenu = () => {
    const params = {
      location: '/csr'
    }
    goTo(params)
  }

  const RenderCSRContent = () => {
    if (!loadingCSR && !errorCSR) {
      if (csr.length !== 0) {
        return (
          <Row className='WrapperContentClass'>
            <Col xs={12} md={12} lg={{ offset: 2, span: 10 }}>
              <Title level={2} style={TitleStyle}>
                {t('CSR.title')}
              </Title>
            </Col>
            <Col xs={12} md={12} lg={10} onClick={() => handleClickMenu()}>
              <div style={MoreContentStyle}>
                <Text style={{ color: '#e7b12e' }}>{t('button.seeAll')}</Text>
                <RightOutlined
                  style={{ color: '#e7b12e', marginLeft: '0.5em' }}
                />
              </div>
            </Col>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Row gutter={[16, 16]}>
                {csr.map((data) => {
                  return (
                    <Col xs={24} md={8} lg={8} key={data.id}>
                      <CSRCard data={data} />
                    </Col>
                  )
                })}
              </Row>
            </Col>
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingCSRContent />
    }
  }

  const RenderErrorLoadingCSRContent = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2]
    return (
      <div className='PilarErrorLoadingWrapper'>
        <Row className='WrapperContentClass'>
          <Col span={24}>
            <Row gutter={[16, 16]}>
              {dummyLoading.map((data) => {
                return (
                  <Col xs={24} md={8} lg={8} key={data}>
                    <Row>
                      <Col span={24}>
                        <PilarLoading
                          type='black'
                          height='150px'
                          borderRadius='10px'
                        />
                      </Col>
                      <Col span={24}>
                        {[0, 1].map((item) => {
                          return (
                            <Col
                              span={24}
                              style={{ marginTop: '10px' }}
                              key={item}
                            >
                              <PilarLoading
                                type='black'
                                height='30px'
                                borderRadius='5px'
                              />
                            </Col>
                          )
                        })}
                      </Col>
                    </Row>
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
        {errorCSR ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingCSR}
              onClick={() => getCSR()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </div>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingCSR && !errorCSR && csr.length === 0) {
      return null
    } else {
      return <RenderCSRContent />
    }
  }

  return <RenderWrapperContent />
}

export default CSRContent

// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button } from 'antd'
import { PilarLoading, PilarCenterSlider } from 'components'
import { textTranslate, goTo } from 'utils'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { dummyPerformanceContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const PilarButtonStyle = {
  marginTop: '1em',
  padding: '0.5em 1em',
  height: 'auto',
  width: 'auto',
  backgroundColor: '#E7B12E',
  color: '#fff',
  border: 0
}

const PerformanceContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Performance Content
  const [landing, setLanding] = useState({})
  const [loadingLanding, setLoadingLanding] = useState(false)
  const [errorLanding, setErrorLanding] = useState(false)
  // Performance Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getPerformanceContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getPerformanceContent = async () => {
    setLoadingLanding(true)
    try {
      if (dummyPerformanceContent.data) {
        setLanding(dummyPerformanceContent.data)
        setErrorLanding(false)
      } else {
        setErrorLanding(true)
      }
    } catch (error) {
      setErrorLanding(true)
    }
    setLoadingLanding(false)
  }

  const handleClickMenu = () => {
    const params = {
      location: '/project'
    }
    goTo(params)
  }

  const RenderPerformanceContent = () => {
    if (!loadingLanding && !errorLanding) {
      if (Object.keys(landing).length !== 0) {
        return (
          <Col span={24}>
            <Row gutter={[16, 32]}>
              <Col xs={24} md={12} lg={12}>
                <Row>
                  <Col span={24}>
                    <Title level={4} style={TitleStyle}>
                      {textTranslate(
                        language,
                        landing?.eng?.title,
                        landing?.ina?.title
                      )}
                    </Title>
                  </Col>
                  <Col span={24} style={{ textAlign: 'left' }}>
                    <Text style={{ color: '#565656' }}>
                      <div
                        dangerouslySetInnerHTML={{
                          __html: textTranslate(
                            language,
                            landing?.eng?.description,
                            landing?.ina?.description
                          )
                        }}
                      ></div>
                    </Text>
                  </Col>
                  <Col
                    span={24}
                    style={{ marginTop: '1em', textAlign: 'left' }}
                  >
                    <Button
                      shape='round'
                      size={'small'}
                      style={PilarButtonStyle}
                      onClick={() => handleClickMenu()}
                    >
                      <Text style={{ color: '#fff' }}>
                        {t('button.seeOurProject')}
                      </Text>
                    </Button>
                  </Col>
                </Row>
              </Col>
              <Col xs={24} md={12} lg={12}>
                <Row>
                  <PilarCenterSlider gallery={landing.imageUrls} />
                </Row>
              </Col>
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorPerformanceContent />
    }
  }

  const RenderErrorPerformanceContent = () => {
    let dummyLoading = [0, 1, 2, 3]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Row gutter={[16, 16]}>
          <Col
            xs={{ span: 24, order: 2 }}
            md={{ span: 8, order: 1 }}
            lg={{ span: 8, order: 1 }}
          >
            <Row>
              <Col span={12} style={{ marginBottom: '30px' }}>
                <PilarLoading type='black' height='20px' borderRadius='5px' />
              </Col>
              <Col span={24} style={{ color: '#565656', textAlign: 'left' }}>
                {dummyLoading.map((data) => {
                  return (
                    <div
                      key={data}
                      style={
                        data < dummyLoading.length - 1
                          ? { marginBottom: '10px' }
                          : null
                      }
                    >
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </div>
                  )
                })}
              </Col>
            </Row>
          </Col>
          <Col
            xs={{ span: 24, order: 1 }}
            md={{ span: 16, order: 2 }}
            lg={{ span: 16, order: 2 }}
            className='PilarErrorLoadingWrapper'
          >
            <PilarLoading type='black' height='160px' borderRadius='5px' />
          </Col>
        </Row>
        {errorLanding ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLanding}
              onClick={() => getPerformanceContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingLanding && !errorLanding && Object.keys(landing).length === 0) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Row gutter={[16, 16]}>
              <RenderPerformanceContent />
            </Row>
          </Col>
        </Row>
      )
    }
  }

  return <RenderWrapperContent />
}

export default PerformanceContent

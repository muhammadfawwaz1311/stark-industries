// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Image } from 'antd'
import { PilarLoading } from 'components'
import { textTranslate, checkDevice, goTo } from 'utils'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { RightOutlined } from '@ant-design/icons'
import {
  dummyBusinessContent,
  dummyOurBusinessContent
} from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const NewsHeaderTitleStyle = { marginBottom: '1em' }
const NewsContentStyle = {
  color: '#565656',
  textAlign: 'left',
  marginBottom: '1em'
}

const BusinessContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Business Content
  const [landing, setLanding] = useState({})
  const [loadingLanding, setLoadingLanding] = useState(false)
  const [errorLanding, setErrorLanding] = useState(false)
  // Business Content

  // Business Image
  const [landingImage, setLandingImage] = useState([])
  const [loadingLandingImage, setLoadingLandingImage] = useState(false)
  const [errorLandingImage, setErrorLandingImage] = useState(false)
  // Business Image

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getBusinessContent()
      getBusinessImage()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getBusinessContent = async () => {
    setLoadingLanding(true)
    try {
      if (dummyOurBusinessContent.data) {
        let data = dummyOurBusinessContent.data
        setLanding(data)
        setErrorLanding(false)
      } else {
        setErrorLanding(true)
      }
    } catch (error) {
      setErrorLanding(true)
    }
    setLoadingLanding(false)
  }

  const getBusinessImage = async () => {
    setLoadingLandingImage(true)
    try {
      if (dummyBusinessContent.data) {
        let data = dummyBusinessContent.data.businesses
        setLandingImage(data)
        setErrorLandingImage(false)
      } else {
        setErrorLandingImage(true)
      }
    } catch (error) {
      setErrorLandingImage(true)
    }
    setLoadingLandingImage(false)
  }

  const handleClickMenu = () => {
    const params = {
      location: '/business'
    }
    goTo(params)
  }

  const RenderBusinessContent = () => {
    if (!loadingLanding && !errorLanding) {
      if (Object.keys(landing).length !== 0) {
        return (
          <Row>
            <Col span={24}>
              <Title level={2} style={{ ...TitleStyle, textAlign: 'center' }}>
                {textTranslate(
                  language,
                  landing?.eng?.title,
                  landing?.ina?.title
                )}
              </Title>
            </Col>
            <Col
              span={24}
              style={{
                textAlign: 'center',
                marginBottom: '2em'
              }}
            >
              <Text>
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      landing?.eng?.description,
                      landing?.ina?.description
                    )
                  }}
                ></div>
              </Text>
            </Col>
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingBusinessContent />
    }
  }

  const RenderErrorLoadingBusinessContent = () => {
    let dummyLoading = [0, 1, 2]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col span={24}>
          <Row>
            <Col offset={8} span={8} style={{ marginBottom: '30px' }}>
              <PilarLoading type='black' height='20px' borderRadius='5px' />
            </Col>
            <Col span={24} style={{ color: '#565656', textAlign: 'left' }}>
              {dummyLoading.map((data) => {
                return (
                  <div
                    key={data}
                    style={
                      data < dummyLoading.length - 1
                        ? { marginBottom: '10px' }
                        : null
                    }
                  >
                    <PilarLoading
                      type='black'
                      height='20px'
                      borderRadius='5px'
                    />
                  </div>
                )
              })}
            </Col>
          </Row>
        </Col>
        {errorLanding ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLanding}
              onClick={() => getBusinessContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderBusinessImage = () => {
    if (!loadingLandingImage && !errorLandingImage) {
      if (landingImage.length !== 0) {
        return (
          <Col span={24}>
            <Row gutter={[16, 32]}>
              {landingImage.map((data) => {
                return (
                  <Col xs={24} md={8} lg={8} key={data.id}>
                    <div className='pilar-card-wrapper'>
                      <div
                        className='business-card-image-wrapper'
                        style={NewsHeaderTitleStyle}
                      >
                        <Image src={data?.projectImages[0]} alt='' />
                      </div>
                      <div className='pilar-card-wrapper'>
                        <div style={{ flex: 1 }}>
                          <Title level={4} style={TitleStyle}>
                            {textTranslate(
                              language,
                              data?.eng?.name,
                              data?.ina?.name
                            )}
                          </Title>
                        </div>
                        <div style={NewsContentStyle}>
                          <Text className='trimString'>
                            <div
                              dangerouslySetInnerHTML={{
                                __html: textTranslate(
                                  language,
                                  data?.eng?.description,
                                  data?.ina?.description
                                )
                              }}
                            ></div>
                          </Text>
                        </div>
                        <div
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            handleClickMenu()
                          }}
                        >
                          <Text style={{ color: '#e7b12e' }}>
                            {t('button.seeMore')}
                          </Text>
                          <RightOutlined
                            style={{ color: '#e7b12e', marginLeft: '0.5em' }}
                          />
                        </div>
                      </div>
                    </div>
                  </Col>
                )
              })}
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorLoadingBusinessImage />
    }
  }

  const RenderErrorLoadingBusinessImage = () => {
    let dummyLoading = [0, 1, 2]
    let dummyHeight = checkDevice() === 'Mobile' ? '100px' : '20vh'
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col span={24}>
          <Row gutter={[16, 16]}>
            {dummyLoading.map((data) => {
              return (
                <Col span={8} key={data}>
                  <PilarLoading
                    type='black'
                    height={dummyHeight}
                    borderRadius='5px'
                  />
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorLandingImage ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLandingImage}
              onClick={() => getBusinessImage()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (
      !loadingLanding &&
      !errorLanding &&
      !loadingLandingImage &&
      !errorLandingImage &&
      Object.keys(landing).length === 0 &&
      landingImage.length === 0
    ) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Row gutter={[16, 16]}>
              <RenderBusinessContent />
              <RenderBusinessImage />
            </Row>
          </Col>
        </Row>
      )
    }
  }

  return <RenderWrapperContent />
}

export default BusinessContent

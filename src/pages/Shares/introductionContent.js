// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button } from 'antd'
import { ImageOverlay, PilarLoading } from 'components'
import { textTranslate, checkDevice, goTo } from 'utils'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { dummyWelcomeContent, dummyBusinessContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const PilarButtonStyle = {
  marginTop: '1em',
  padding: '0.5em 1em',
  height: 'auto',
  width: 'auto',
  backgroundColor: '#E7B12E',
  color: '#fff',
  border: 0
}

const IntroductionContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Introduction Content
  const [intro, setIntro] = useState({})
  const [loadingIntro, setLoadingIntro] = useState(false)
  const [errorIntro, setErrorIntro] = useState(false)
  // Introduction Content

  // Introduction Image
  const [introImage, setIntroImage] = useState([])
  const [loadingIntroImage, setLoadingIntroImage] = useState(false)
  const [errorIntroImage, setErrorIntroImage] = useState(false)
  // Introduction Image

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getIntroductionContent()
      getIntroductionImage()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getIntroductionContent = async () => {
    setLoadingIntro(true)
    try {
      if (dummyWelcomeContent.data) {
        let data = dummyWelcomeContent.data
        setIntro(data)
        setErrorIntro(false)
      } else {
        setErrorIntro(true)
      }
    } catch (error) {
      setErrorIntro(true)
    }
    setLoadingIntro(false)
  }

  const getIntroductionImage = async () => {
    setLoadingIntroImage(true)
    try {
      if (dummyBusinessContent.data) {
        let data = dummyBusinessContent.data.businesses
        setIntroImage(data)
        setErrorIntroImage(false)
      } else {
        setErrorIntroImage(true)
      }
    } catch (error) {
      setErrorIntroImage(true)
    }
    setLoadingIntroImage(false)
  }

  const handleClickMenu = () => {
    const params = {
      location: '/business'
    }
    goTo(params)
  }

  const RenderIntroductionContent = () => {
    if (!loadingIntro && !errorIntro) {
      if (Object.keys(intro).length !== 0) {
        return (
          <Col
            xs={{ span: 24, order: 2 }}
            md={{ span: 8, order: 1 }}
            lg={{ span: 8, order: 1 }}
          >
            <Row>
              <Col span={24}>
                <Text style={{ color: '#E7B12E' }}>
                  {t(`text.${intro.code}`)}
                </Text>
              </Col>
              <Col span={24}>
                <Title level={4} style={TitleStyle}>
                  {textTranslate(
                    language,
                    intro?.eng?.title,
                    intro?.ina?.title
                  )}
                </Title>
              </Col>
              <Col span={24} style={{ color: '#565656', textAlign: 'left' }}>
                <Text>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: textTranslate(
                        language,
                        intro?.eng?.description,
                        intro?.ina?.description
                      )
                    }}
                  ></div>
                </Text>
              </Col>
              <Col span={24} style={{ color: '#565656', textAlign: 'left' }}>
                <Button
                  shape='round'
                  size={'small'}
                  style={PilarButtonStyle}
                  onClick={() => {
                    handleClickMenu()
                  }}
                >
                  <Text style={{ color: '#fff' }}>{t('button.seeMore')}</Text>
                </Button>
              </Col>
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorLoadingIntroductionContent />
    }
  }

  const RenderErrorLoadingIntroductionContent = () => {
    let dummyLoading = [0, 1, 2, 3, 4]
    return (
      <Col
        xs={{ span: 24, order: 2 }}
        md={{ span: 8, order: 1 }}
        lg={{ span: 8, order: 1 }}
        className='PilarErrorLoadingWrapper'
      >
        <Col span={24}>
          <Row>
            <Col span={12} style={{ marginBottom: '30px' }}>
              <PilarLoading type='black' height='20px' borderRadius='5px' />
            </Col>
            <Col span={24} style={{ color: '#565656', textAlign: 'left' }}>
              {dummyLoading.map((data) => {
                return (
                  <div
                    key={data}
                    style={
                      data < dummyLoading.length - 1
                        ? { marginBottom: '10px' }
                        : null
                    }
                  >
                    <PilarLoading
                      type='black'
                      height='20px'
                      borderRadius='5px'
                    />
                  </div>
                )
              })}
            </Col>
          </Row>
        </Col>
        {errorIntro ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingIntro}
              onClick={() => getIntroductionContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderIntroductionImage = () => {
    if (!loadingIntroImage && !errorIntroImage) {
      if (introImage.length !== 0) {
        return (
          <Col
            xs={{ span: 24, order: 1 }}
            md={{ span: 16, order: 2 }}
            lg={{ span: 16, order: 2 }}
          >
            <Row className='IntroductionImage'>
              {introImage.map((data) => {
                return (
                  <Col span={8} key={data.id} style={{ padding: '0.5em' }}>
                    <ImageOverlay ImageData={data} />
                  </Col>
                )
              })}
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorLoadingIntroductionImage />
    }
  }

  const RenderErrorLoadingIntroductionImage = () => {
    let dummyLoading = [0, 1, 2]
    let dummyHeight = checkDevice() === 'Mobile' ? '100px' : '30vh'
    return (
      <Col
        xs={{ span: 24, order: 1 }}
        md={{ span: 16, order: 2 }}
        lg={{ span: 16, order: 2 }}
        className='PilarErrorLoadingWrapper'
      >
        <Col span={24}>
          <Row gutter={[16, 16]}>
            {dummyLoading.map((data) => {
              return (
                <Col span={8} key={data}>
                  <PilarLoading
                    type='black'
                    height={dummyHeight}
                    borderRadius='5px'
                  />
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorIntroImage ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingIntroImage}
              onClick={() => getIntroductionImage()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (
      !loadingIntro &&
      !errorIntro &&
      !loadingIntroImage &&
      !errorIntroImage &&
      Object.keys(intro).length === 0 &&
      introImage.length === 0
    ) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Row gutter={[16, 16]}>
              <RenderIntroductionContent />
              <RenderIntroductionImage />
            </Row>
          </Col>
        </Row>
      )
    }
  }

  return <RenderWrapperContent />
}

export default IntroductionContent

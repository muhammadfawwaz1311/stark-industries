// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button } from 'antd'
import { PilarLoading } from 'components'
import { useTranslation } from 'react-i18next'
import { dummyContactContent } from '../../assets/dummy'

const { Text } = Typography

const VideoOption = {
  width: '100%',
  frameBorder: '0',
  allow:
    'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture',
  allowFullScreen: true
}

const VideoContent = () => {
  const { t } = useTranslation()

  // Video Content
  const [landing, setLanding] = useState('')
  const [loadingLanding, setLoadingLanding] = useState(false)
  const [errorLanding, setErrorLanding] = useState(false)
  // Video Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getVideoContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getVideoContent = async () => {
    setLoadingLanding(true)
    try {
      if (dummyContactContent.data) {
        let data = dummyContactContent.data
        setLanding(data?.eng?.videoUrl?.value || data?.ina?.videoUrl?.value)
        setErrorLanding(false)
      } else {
        setErrorLanding(true)
      }
    } catch (error) {
      setErrorLanding(true)
    }
    setLoadingLanding(false)
  }

  const RenderVideoContent = () => {
    if (!loadingLanding && !errorLanding) {
      if (landing) {
        return (
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <iframe
              {...VideoOption}
              src={landing}
              title='About Video'
              className='PilarVideo'
            />
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorVideoContent />
    }
  }

  const RenderErrorVideoContent = () => {
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <PilarLoading type='black' height='480px' borderRadius='5px' />
        {errorLanding ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLanding}
              onClick={() => getVideoContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingLanding && !errorLanding && !landing) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <RenderVideoContent />
        </Row>
      )
    }
  }

  return <RenderWrapperContent />
}

export default VideoContent

// dependencies
import React from 'react'
import { Row, Col, Typography, Card, Button, Divider } from 'antd'
import { BusinessBackground } from 'assets'
import { checkDevice, goTo } from 'utils'
import { useTranslation } from 'react-i18next'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const PilarButtonStyle = {
  marginTop: '1em',
  padding: '0.5em 1em',
  height: 'auto',
  width: 'auto',
  backgroundColor: '#E7B12E',
  color: '#fff',
  border: 0
}
const CareerContactContentStyle = {
  position: 'relative',
  width: '100%',
  height: '100%',
  padding: 0,
  marginTop: '3em',
  backgroundImage: `url(${BusinessBackground})`,
  backgroundSize: 'cover',
  backgroundRepeat: 'no-repeat',
  backgroundPosition: 'center'
}
const CardWrapper = {
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  height: '100%'
}

const CareerContactContent = () => {
  const { t } = useTranslation()

  const CareerContactDividerStyle = (device) => {
    if (device === 'Mobile') {
      return {
        type: 'horizontal',
        style: { borderColor: 'rgb(255, 255, 255, 0.4)' }
      }
    }
    return {
      type: 'vertical',
      style: {
        borderColor: 'rgb(255, 255, 255, 0.4)',
        height: '100%',
        margin: '0 50%'
      }
    }
  }

  const handleClickMenu = (menu) => {
    const params = {
      location: `/${menu}`
    }
    goTo(params)
  }

  const RenderCareerContactContent = () => {
    return (
      <Row
        className='ProjectHomeContentClass'
        style={CareerContactContentStyle}
      >
        <Col xs={24} md={24} lg={24}>
          <Card style={{ background: 'transparent', border: 0 }}>
            <Row gutter={[16, 16]}>
              <Col xs={24} md={11} lg={11} style={{ textAlign: 'center' }}>
                <div style={CardWrapper}>
                  <div style={{ flex: 1 }}>
                    <Title level={4} style={{ ...TitleStyle, color: '#fff' }}>
                      {t('career.title')}
                    </Title>
                  </div>
                  <div style={{ flex: 1 }}>
                    <Text style={{ color: '#fff' }}>{t('career.desc')}</Text>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <Button
                      shape='round'
                      size={'small'}
                      style={PilarButtonStyle}
                      onClick={() => {
                        handleClickMenu('career')
                      }}
                    >
                      <Text style={{ color: '#fff' }}>
                        {t('button.joinUs')}
                      </Text>
                    </Button>
                  </div>
                </div>
              </Col>
              <Col xs={24} md={2} lg={2} style={{ width: '100%' }}>
                <Divider {...CareerContactDividerStyle(checkDevice())} />
              </Col>
              <Col xs={24} md={11} lg={11} style={{ textAlign: 'center' }}>
                <div style={CardWrapper}>
                  <div style={{ flex: 1 }}>
                    <Title level={4} style={{ ...TitleStyle, color: '#fff' }}>
                      {t('contact.title')}
                    </Title>
                  </div>
                  <div style={{ flex: 1 }}>
                    <Text style={{ color: '#fff' }}>{t('contact.desc')}</Text>
                  </div>
                  <div style={{ textAlign: 'center' }}>
                    <Button
                      shape='round'
                      size={'small'}
                      style={PilarButtonStyle}
                      onClick={() => {
                        handleClickMenu('contact')
                      }}
                    >
                      <Text style={{ color: '#fff' }}>
                        {t('button.contactUs')}
                      </Text>
                    </Button>
                  </div>
                </div>
              </Col>
            </Row>
          </Card>
        </Col>
      </Row>
    )
  }
  return <RenderCareerContactContent />
}

export default CareerContactContent

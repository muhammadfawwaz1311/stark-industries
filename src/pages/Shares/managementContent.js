// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Card, Image } from 'antd'
import { PilarLoading } from 'components'
import { checkDevice, textTranslate } from 'utils'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { dummyManagementKey, dummyManagementTeam } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const CSRCardWrapper = {
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  height: '100%'
}

const ManagementContent = (props) => {
  const { type } = props
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Management Content
  const [landing, setLanding] = useState({})
  const [loadingLanding, setLoadingLanding] = useState(false)
  const [errorLanding, setErrorLanding] = useState(false)
  // Management Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getManagementContent(type)
    }

    return () => {
      subscribe = false
    }
  }, [type])
  // Initial

  const getManagementContent = async (type) => {
    setLoadingLanding(true)
    try {
      const getManagementContent =
        type === 'key' ? dummyManagementKey : dummyManagementTeam
      if (getManagementContent.data) {
        let data = getManagementContent?.data?.managements
        setLanding(data)
        setErrorLanding(false)
      } else {
        setErrorLanding(true)
      }
    } catch (error) {
      setErrorLanding(true)
    }
    setLoadingLanding(false)
  }

  const RenderManagementContent = () => {
    if (!loadingLanding && !errorLanding) {
      if (Object.keys(landing).length !== 0) {
        if (type === 'key') {
          return (
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Row style={{ justifyContent: 'center' }}>
                {landing.map((data) => {
                  return (
                    <Col
                      xs={24}
                      md={12}
                      lg={6}
                      key={data?.id}
                      style={{ padding: '0.5em' }}
                    >
                      <div className='pilar-card-wrapper'>
                        <Card
                          hoverable
                          style={{ height: '100%' }}
                          bodyStyle={{ ...CSRCardWrapper, padding: 0 }}
                        >
                          <div className='management-card-image-wrapper'>
                            <Image src={data?.imageUrl} alt={data?.name} />
                          </div>
                          <div style={{ ...CSRCardWrapper, padding: '1em' }}>
                            <div style={{ flex: 1, textAlign: 'center' }}>
                              <Title level={5} style={TitleStyle}>
                                {data?.name}
                              </Title>
                            </div>
                            <div style={{ textAlign: 'center' }}>
                              <Text>
                                {textTranslate(
                                  language,
                                  data?.eng?.position,
                                  data?.eng?.position
                                )}
                              </Text>
                            </div>
                          </div>
                        </Card>
                      </div>
                    </Col>
                  )
                })}
              </Row>
            </Col>
          )
        } else {
          return (
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              {landing?.map((data, dataIndex) => {
                return (
                  <Image src={data?.imageUrl} alt={data.name} key={dataIndex} />
                )
              })}
            </Col>
          )
        }
      } else return null
    } else {
      return <RenderErrorLoadingManagementContent />
    }
  }

  const RenderErrorLoadingManagementContent = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2, 3]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row>
            {dummyLoading.map((data) => {
              return (
                <Col
                  xs={24}
                  md={12}
                  lg={6}
                  key={data}
                  style={{ padding: '0.5em' }}
                >
                  <Card
                    hoverable
                    cover={
                      <PilarLoading
                        type='black'
                        height='150px'
                        borderRadius='0'
                      />
                    }
                  >
                    <Row>
                      <Col
                        offset={6}
                        span={12}
                        style={{ marginBottom: '10px' }}
                      >
                        <PilarLoading
                          type='black'
                          height='20px'
                          borderRadius='5px'
                        />
                      </Col>
                      <Col span={24}>
                        <PilarLoading
                          type='black'
                          height='20px'
                          borderRadius='5px'
                        />
                      </Col>
                    </Row>
                  </Card>
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorLanding ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLanding}
              onClick={() => getManagementContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingLanding && !errorLanding && Object.keys(landing).length === 0) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Title
              level={2}
              className='CarouselTitleClass'
              style={{
                ...TitleStyle,
                textAlign: 'center',
                paddingTop: '0.5em'
              }}
            >
              {type === 'key'
                ? t('management.keyTitle')
                : t('management.teamTitle')}
            </Title>
          </Col>
          <RenderManagementContent />
        </Row>
      )
    }
  }

  return <RenderWrapperContent />
}

export default ManagementContent

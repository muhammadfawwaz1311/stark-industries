// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Card } from 'antd'
import { PilarLoading } from 'components'
import { checkDevice, textTranslate } from 'utils'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { dummySummaryContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const CSRCardWrapper = {
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  height: '100%',
  padding: '1em'
}

const SummaryContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Summary Content
  const [landing, setLanding] = useState({})
  const [loadingLanding, setLoadingLanding] = useState(false)
  const [errorLanding, setErrorLanding] = useState(false)
  // Summary Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getSummaryContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getSummaryContent = async () => {
    setLoadingLanding(true)
    try {
      if (dummySummaryContent.data) {
        setLanding(dummySummaryContent.data)
        setErrorLanding(false)
      } else {
        setErrorLanding(true)
      }
    } catch (error) {
      setErrorLanding(true)
    }
    setLoadingLanding(false)
  }

  const RenderSummaryContent = () => {
    let summaries =
      language === 'ENG' ? landing?.eng?.summaries : landing?.ina?.summaries
    if (!loadingLanding && !errorLanding) {
      if (Object.keys(landing).length !== 0) {
        return (
          <Row>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Title level={2} style={TitleStyle}>
                {textTranslate(
                  language,
                  landing?.eng?.title,
                  landing?.ina?.title
                )}
              </Title>
            </Col>
            <Col
              xs={24}
              md={24}
              lg={{ offset: 2, span: 20 }}
              style={{ textAlign: 'left' }}
            >
              <Text>
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      landing?.eng?.description,
                      landing?.ina?.description
                    )
                  }}
                ></div>
              </Text>
            </Col>
            <Col
              xs={24}
              md={24}
              lg={{ offset: 2, span: 20 }}
              style={{ marginTop: '2em' }}
            >
              <Row style={{ backgroundColor: '#2C2C2C', padding: '1.5em' }}>
                {summaries?.map((char, index) => {
                  return (
                    <Col
                      xs={24}
                      md={6}
                      lg={6}
                      key={index}
                      style={{ padding: '0.5em' }}
                    >
                      <div className='pilar-card-wrapper'>
                        <Card
                          hoverable
                          style={{ height: '100%' }}
                          bodyStyle={CSRCardWrapper}
                        >
                          <div style={{ textAlign: 'center', flex: 1 }}>
                            <Title
                              level={1}
                              style={{ ...TitleStyle, color: '#E7B12E' }}
                            >
                              {char.value}
                            </Title>
                          </div>
                          <div style={{ textAlign: 'center', flex: 1 }}>
                            <Text style={{ color: '#787878' }}>
                              {char.label}
                            </Text>
                          </div>
                        </Card>
                      </div>
                    </Col>
                  )
                })}
              </Row>
            </Col>
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingSummaryContent />
    }
  }

  const RenderErrorLoadingSummaryContent = () => {
    let dummyLoading = [0, 1, 2]
    let dummyLoadingImage = checkDevice() === 'Mobile' ? [0] : [0, 1, 2, 3]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col span={24}>
          <Row>
            <Col offset={8} span={8} style={{ marginBottom: '30px' }}>
              <PilarLoading type='black' height='20px' borderRadius='5px' />
            </Col>
            <Col
              span={24}
              style={{
                color: '#565656',
                textAlign: 'left',
                marginBottom: '30px'
              }}
            >
              {dummyLoading.map((data) => {
                return (
                  <div
                    key={data}
                    style={
                      data < dummyLoading.length - 1
                        ? { marginBottom: '10px' }
                        : null
                    }
                  >
                    <PilarLoading
                      type='black'
                      height='20px'
                      borderRadius='5px'
                    />
                  </div>
                )
              })}
            </Col>
            <Col span={24}>
              <Row gutter={[16]}>
                {dummyLoadingImage.map((data) => {
                  return (
                    <Col xs={24} md={6} lg={6} key={data}>
                      <div key={data} style={{ marginBottom: '10px' }}>
                        <PilarLoading
                          type='black'
                          height='100px'
                          borderRadius='5px'
                        />
                      </div>
                    </Col>
                  )
                })}
              </Row>
            </Col>
          </Row>
        </Col>
        {errorLanding ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLanding}
              onClick={() => getSummaryContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingLanding && !errorLanding && Object.keys(landing).length === 0) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <Col span={24}>
            <RenderSummaryContent />
          </Col>
        </Row>
      )
    }
  }

  return <RenderWrapperContent />
}

export default SummaryContent

// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button } from 'antd'
import { PilarLoading, NewsCard } from 'components'
import { goTo, checkDevice } from 'utils'
import { RightOutlined } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import { NewsService } from 'services'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const NewsCardWrapper = {
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  height: '100%'
}

const NewsContent = () => {
  const { t } = useTranslation()

  // News Content
  const newsPage = 1
  const newsPer = 2
  const [news, setNews] = useState([])
  const [loadingNews, setLoadingNews] = useState(false)
  const [errorNews, setErrorNews] = useState(false)
  // News Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getNews()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getNews = async () => {
    setLoadingNews(true)
    try {
      const getNews = await NewsService.getNewsList(newsPage, newsPer)
      if (getNews && getNews.status === 200) {
        let data = getNews?.data?.data?.news
        setNews(data)
        setErrorNews(false)
      } else {
        setErrorNews(true)
      }
    } catch (error) {
      setErrorNews(true)
    }
    setLoadingNews(false)
  }

  const handleClickMenu = (url) => {
    let param = {
      location: `/news/detail/${url}`
    }
    goTo(param)
  }

  const RenderNewsContent = () => {
    if (!loadingNews && !errorNews) {
      if (news.length !== 0) {
        return (
          <Row className='WrapperContentClass'>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Title level={2} style={TitleStyle}>
                {t('news.title')}
              </Title>
            </Col>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Row gutter={[16, 16]}>
                {news.map((data) => {
                  return (
                    <Col xs={24} md={12} lg={12} key={data.id}>
                      <div style={NewsCardWrapper}>
                        <NewsCard data={data} />
                        <div
                          style={{ cursor: 'pointer' }}
                          onClick={() => {
                            handleClickMenu(data.id)
                          }}
                        >
                          <Text style={{ color: '#e7b12e' }}>
                            {t('button.seeMore')}
                          </Text>
                          <RightOutlined
                            style={{ color: '#e7b12e', marginLeft: '0.5em' }}
                          />
                        </div>
                      </div>
                    </Col>
                  )
                })}
              </Row>
            </Col>
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingNewsContent />
    }
  }

  const RenderErrorLoadingNewsContent = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1]
    return (
      <div className='PilarErrorLoadingWrapper'>
        <Row className='WrapperContentClass'>
          <Col span={24}>
            <Row gutter={[16, 16]}>
              {dummyLoading.map((data) => {
                return (
                  <Col xs={24} md={12} lg={12} key={data}>
                    <Row>
                      <Col span={24}>
                        <PilarLoading
                          type='black'
                          height='150px'
                          borderRadius='10px'
                        />
                      </Col>
                      <Col span={24}>
                        {[0, 1].map((item) => {
                          return (
                            <Col
                              span={24}
                              style={{ marginTop: '10px' }}
                              key={item}
                            >
                              <PilarLoading
                                type='black'
                                height='30px'
                                borderRadius='5px'
                              />
                            </Col>
                          )
                        })}
                      </Col>
                    </Row>
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
        {errorNews ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingNews}
              onClick={() => getNews()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </div>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingNews && !errorNews && news.length === 0) {
      return null
    } else {
      return <RenderNewsContent />
    }
  }

  return <RenderWrapperContent />
}

export default NewsContent

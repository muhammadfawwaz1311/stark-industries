// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button } from 'antd'
import { PilarLoading } from 'components'
import { textTranslate } from 'utils'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import Fade from 'react-reveal/Fade'
import { dummyPrefaceContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const ForewordTextStyle = { color: '#000' }
const ForewordBorderStyle = {
  marginBottom: '1rem',
  paddingBottom: '1rem',
  borderBottom: '1px solid #000'
}

const PrefaceContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Preface Content
  const [landing, setLanding] = useState({})
  const [loadingLanding, setLoadingLanding] = useState(false)
  const [errorLanding, setErrorLanding] = useState(false)
  // Preface Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getPrefaceContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getPrefaceContent = async () => {
    setLoadingLanding(true)
    try {
      if (dummyPrefaceContent.data) {
        setLanding(dummyPrefaceContent.data)
        setErrorLanding(false)
      } else {
        setErrorLanding(true)
      }
    } catch (error) {
      setErrorLanding(true)
    }
    setLoadingLanding(false)
  }

  const RenderPrefaceContent = () => {
    if (!loadingLanding && !errorLanding) {
      if (Object.keys(landing).length !== 0) {
        return (
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            className='PilarForewordContentWrapSmall'
          >
            <Row gutter={[32, 32]}>
              <Col
                xs={{ span: 24, order: 2 }}
                md={{ span: 16, order: 1 }}
                lg={{ span: 14, order: 1, offset: 2 }}
                style={{ height: '100%' }}
              >
                <Fade left>
                  <div
                    style={ForewordBorderStyle}
                    className='PilarForewordTextClass'
                  >
                    <Text style={ForewordTextStyle}>
                      <div
                        dangerouslySetInnerHTML={{
                          __html: textTranslate(
                            language,
                            landing?.eng?.preface ? landing?.eng?.preface : '',
                            landing?.ina?.preface ? landing?.ina?.preface : ''
                          )
                        }}
                      ></div>
                    </Text>
                  </div>
                  <div>
                    <Title
                      level={4}
                      className='PilarForewordTextClass'
                      style={{ ...TitleStyle, ...ForewordTextStyle }}
                    >
                      {landing?.name}
                    </Title>
                  </div>
                  <div className='PilarForewordTextClass'>
                    <Text style={ForewordTextStyle}>
                      {textTranslate(
                        language,
                        landing?.eng?.position ? landing?.eng?.position : '',
                        landing?.ina?.position ? landing?.ina?.position : ''
                      )}
                    </Text>
                  </div>
                </Fade>
              </Col>
              <Col
                xs={{ span: 12, order: 1 }}
                md={{ span: 8, order: 2 }}
                lg={{ span: 6, order: 2 }}
              >
                <Fade right>
                  <div className='PilarForewordWrap'>
                    <img
                      src={landing?.imageUrl}
                      alt=''
                      className='PilarForewordImage'
                    />
                  </div>
                </Fade>
              </Col>
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorLoadingPrefaceContent />
    }
  }

  const RenderErrorLoadingPrefaceContent = () => {
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
        {errorLanding ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingLanding}
              onClick={() => getPrefaceContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperContent = () => {
    if (!loadingLanding && !errorLanding && Object.keys(landing).length === 0) {
      return null
    } else {
      return (
        <div>
          <Row className='WrapperContentClass'>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Title level={2} style={{ ...TitleStyle, textAlign: 'center' }}>
                {t('preface.title')}
              </Title>
            </Col>
            <RenderPrefaceContent />
          </Row>
        </div>
      )
    }
  }

  return <RenderWrapperContent />
}

export default PrefaceContent

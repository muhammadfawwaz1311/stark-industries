// dependencies
import React, { useState, useEffect, useRef } from 'react'
import { Row, Col, Typography, Button, Carousel } from 'antd'
import { PilarLoading } from 'components'
import { checkDevice } from 'utils'
import { CaretLeftFilled, CaretRightFilled } from '@ant-design/icons'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { dummyObjectiveContent } from '../../assets/dummy'

const { Title, Text } = Typography
const ContentBackgroundStyle = { backgroundColor: '#FAFAFA' }
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }

const ObjectiveContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Company Objective Content
  const [companyObj, setCompanyObj] = useState({})
  const [currentCompanyObj, setCurrentCompanyObj] = useState([])
  const [loadingCompanyObj, setLoadingCompanyObj] = useState(false)
  const [errorCompanyObj, setErrorCompanyObj] = useState(false)
  const companyObjRef = useRef()
  // Company Objective Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getCompanyObjective()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  useEffect(() => {
    let subscribe = true
    let currentData = {}

    if (subscribe) {
      if (language === 'ENG') {
        currentData = companyObj?.eng ? companyObj?.eng : {}
      } else {
        currentData = companyObj?.ina ? companyObj?.ina : {}
      }
      setCurrentCompanyObj(currentData)
    }

    return () => {
      subscribe = false
    }
  }, [companyObj, language])

  const getCompanyObjective = async () => {
    setLoadingCompanyObj(true)
    try {
      if (dummyObjectiveContent.data) {
        setCompanyObj(dummyObjectiveContent.data)
        setErrorCompanyObj(false)
      } else {
        setErrorCompanyObj(true)
      }
    } catch (error) {
      setErrorCompanyObj(true)
    }
    setLoadingCompanyObj(false)
  }

  const RenderCompanyObjective = () => {
    const vission = currentCompanyObj?.vission
    const mission = currentCompanyObj?.mission
    const value = currentCompanyObj?.value
    if (!loadingCompanyObj && !errorCompanyObj) {
      if (Object.keys(companyObj).length !== 0) {
        return (
          <Row className='WrapperContentClass' style={ContentBackgroundStyle}>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Carousel
                className='PilarCarouselClass MissionCarouselClass'
                // autoplay
                // autoplaySpeed={5000}
                ref={companyObjRef}
              >
                {/* Vission */}
                {vission?.values?.length > 0 ? (
                  <Row key={vission.id}>
                    <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
                      <Title
                        level={2}
                        style={{
                          ...TitleStyle,
                          textAlign: 'center',
                          color: '#E7B12E'
                        }}
                      >
                        {vission.label}
                      </Title>
                    </Col>
                    <Col span={24}>
                      <Row style={{ justifyContent: 'center' }}>
                        {vission.values.map((sliderValues, sliderIndex) => {
                          return (
                            <Col
                              flex='1 1 100%'
                              key={sliderIndex}
                              style={{
                                padding: '1em',
                                textAlign: 'center'
                              }}
                            >
                              <Text className='vmv'>{sliderValues}</Text>
                            </Col>
                          )
                        })}
                      </Row>
                    </Col>
                  </Row>
                ) : null}
                {/* Vission */}
                {/* Mission */}
                {mission?.values?.length > 0 ? (
                  <Row key={mission.id}>
                    <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
                      <Title
                        level={2}
                        style={{
                          ...TitleStyle,
                          textAlign: 'center',
                          color: '#E7B12E'
                        }}
                      >
                        {mission.label}
                      </Title>
                    </Col>
                    <Col span={24}>
                      <Row style={{ justifyContent: 'center' }}>
                        {mission.values.map((sliderValues, sliderIndex) => {
                          return (
                            <Col
                              flex={1}
                              key={sliderIndex}
                              style={{
                                padding: '1em',
                                textAlign: 'center'
                              }}
                            >
                              <Text>{sliderValues}</Text>
                            </Col>
                          )
                        })}
                      </Row>
                    </Col>
                  </Row>
                ) : null}
                {/* Mission */}
                {/* Value */}
                {value?.values?.length > 0 ? (
                  <Row key={value.id} className='MissionRow'>
                    <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
                      <Title
                        level={2}
                        style={{
                          ...TitleStyle,
                          textAlign: 'center',
                          color: '#E7B12E'
                        }}
                      >
                        {value.label}
                      </Title>
                    </Col>
                    <Col span={24} style={{ height: '100%' }}>
                      <Row className='MissionChildRow' gutter={[16, 16]}>
                        {value.values.map((sliderValues, sliderIndex) => {
                          return (
                            <Col
                              span={8}
                              key={sliderIndex}
                              style={{
                                padding: '1em',
                                textAlign: 'center'
                              }}
                            >
                              <Text className='vmv'>{sliderValues}</Text>
                            </Col>
                          )
                        })}
                      </Row>
                    </Col>
                  </Row>
                ) : null}
                {/* Value */}
              </Carousel>
            </Col>
            <Col
              xs={24}
              md={24}
              lg={{ offset: 2, span: 20 }}
              style={{ textAlign: 'center' }}
            >
              <Button
                className='PilarButton'
                shape='circle'
                icon={<CaretLeftFilled />}
                size='small'
                onClick={() => companyObjRef.current.prev()}
              />
              <Button
                className='PilarButton'
                shape='circle'
                icon={<CaretRightFilled />}
                size='small'
                onClick={() => companyObjRef.current.next()}
              />
            </Col>
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingCompanyObjective />
    }
  }

  const RenderErrorLoadingCompanyObjective = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2, 3, 4]
    let dummyText = [0, 1, 2]

    return (
      <div className='PilarErrorLoadingWrapper'>
        <Row className='WrapperContentClass'>
          <Col offset={8} span={8} style={{ marginBottom: '20px' }}>
            <PilarLoading type='black' height='20px' borderRadius='5px' />
          </Col>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Row gutter={[32, 16]}>
              {dummyLoading.map((data) => {
                return (
                  <Col
                    flex={checkDevice() === 'Mobile' ? '0 0 100%' : '0 0 20%'}
                    key={data}
                  >
                    {dummyText.map((text) => {
                      return (
                        <div key={text} style={{ marginBottom: '10px' }}>
                          <PilarLoading
                            type='black'
                            height='20px'
                            borderRadius='5px'
                          />
                        </div>
                      )
                    })}
                  </Col>
                )
              })}
            </Row>
          </Col>
        </Row>
        {errorCompanyObj ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingCompanyObj}
              onClick={() => getCompanyObjective()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </div>
    )
  }

  const RenderWrapperContent = () => {
    if (
      !loadingCompanyObj &&
      !errorCompanyObj &&
      Object.keys(companyObj).length === 0
    ) {
      return null
    } else {
      return <RenderCompanyObjective />
    }
  }

  return <RenderWrapperContent />
}

export default ObjectiveContent

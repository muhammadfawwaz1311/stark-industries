// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Input, Empty, Pagination } from 'antd'
import { NewsCard, PilarLoading } from 'components'
import { textTranslate, checkDevice, goTo } from 'utils'
import { HomeBackground } from 'assets'
import { SearchOutlined } from '@ant-design/icons'
import { HeaderService, ProjectService, NewsService } from 'services'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'
import { CareerContactContent } from 'pages/Shares'
import { useHistory, useLocation } from 'react-router-dom'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const SearchInputStyle = { borderRadius: '5px' }
const SearchRedirectStyle = { cursor: 'pointer' }
const SearchContentStyle = {
  color: '#565656',
  textAlign: 'justify'
}
const PaginationButtonStyle = { color: '#B0B0B0', margin: '0 0.5em' }

const RenderSearchInput = ({
  setSearchValue,
  searchValue,
  searchPlaceholder,
  isSearchProject,
  handleSearchProject,
  handleSearchNews
}) => {
  return (
    <Row className='WrapperContentClass'>
      <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
        <Input
          allowClear
          placeholder={searchPlaceholder}
          value={searchValue}
          onChange={(event) => setSearchValue(event.target.value)}
          onPressEnter={(event) =>
            isSearchProject()
              ? handleSearchProject(event.target.value)
              : handleSearchNews(event.target.value)
          }
          prefix={<SearchOutlined />}
          style={SearchInputStyle}
        />
      </Col>
    </Row>
  )
}

const Search = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  const { state } = useLocation()
  const history = useHistory()

  // Header Content
  const page = 'search'
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  const [search, setSearch] = useState(state?.searchValue)

  const per = 6
  // Project Content
  const [projectSearch, setProjectSearch] = useState([])
  const [loadingProjectSearch, setLoadingProjectSearch] = useState(false)
  const [errorProjectSearch, setErrorProjectSearch] = useState(false)
  const [pageProjectSearch, setPageProjectSearch] = useState(1)
  const [totalProjectSearch, setTotalProjectSearch] = useState(0)
  // Project Content

  // News Content
  const [newsSearch, setNewsSearch] = useState([])
  const [loadingNewsSearch, setLoadingNewsSearch] = useState(false)
  const [errorNewsSearch, setErrorNewsSearch] = useState(false)
  const [pageNewsSearch, setPageNewsSearch] = useState(1)
  const [totalNewsSearch, setTotalNewsSearch] = useState(0)
  // News Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      if (state && state?.type && state?.searchValue) {
        switch (state?.type) {
          case 'project':
            getProjectSearch(pageProjectSearch, state?.searchValue, language)
            break
          default:
            getNewsSearch(pageNewsSearch, state?.searchValue, language)
            break
        }
      }
    }

    return () => {
      subscribe = false
    }
  }, [state, language, pageProjectSearch, pageNewsSearch])

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      const getHeaderContent = await HeaderService.getHeaderContent(page)
      if (getHeaderContent && getHeaderContent.status === 200) {
        let data = getHeaderContent?.data?.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getProjectSearch = async (pageProjectSearch, search, language) => {
    setLoadingProjectSearch(true)
    try {
      const getProjectSearch = await ProjectService.getProjectSearch(
        pageProjectSearch,
        per,
        search,
        language
      )
      if (getProjectSearch && getProjectSearch.status === 200) {
        let data = getProjectSearch?.data?.data?.projects
        let total = getProjectSearch?.data?.pagination?.totalCount
        setProjectSearch(data)
        setTotalProjectSearch(total)
        setErrorProjectSearch(false)
      } else {
        setErrorProjectSearch(true)
      }
    } catch (error) {
      setErrorProjectSearch(true)
    }
    setLoadingProjectSearch(false)
  }

  const getNewsSearch = async (pageNewsSearch, search, language) => {
    setLoadingNewsSearch(true)
    try {
      const getNewsSearch = await NewsService.getNewsSearch(
        pageNewsSearch,
        per,
        search,
        language
      )
      if (getNewsSearch && getNewsSearch.status === 200) {
        let data = getNewsSearch?.data?.data?.news
        let total = getNewsSearch?.data?.pagination?.totalCount
        setNewsSearch(data)
        setTotalNewsSearch(total)
        setErrorNewsSearch(false)
      } else {
        setErrorNewsSearch(true)
      }
    } catch (error) {
      setErrorNewsSearch(true)
    }
    setLoadingNewsSearch(false)
  }

  const isSearchProject = () => {
    return state?.type === 'project'
  }

  const RedirectPage = (url) => {
    let param = {
      location: `/news/detail/${url}`
    }
    goTo(param)
  }

  const handleSearchProject = (value) => {
    if (value) {
      getProjectSearch(pageProjectSearch, value, language)
      history.push({ state: { ...state, searchValue: value } })
    }
  }

  const handleSearchNews = (value) => {
    if (value) {
      getProjectSearch(pageNewsSearch, value, language)
      history.push({ state: { ...state, searchValue: value } })
    }
  }

  const handleOnChange = (value) => {
    setSearch(value)
    if (!value) {
      setProjectSearch([])
      setTotalProjectSearch(0)
      setPageProjectSearch(1)
      setNewsSearch([])
      setTotalNewsSearch(0)
      setPageNewsSearch(1)
      history.push({ state: { ...state, searchValue: value } })
    }
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : HomeBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderProjectSearch = () => {
    if (!loadingProjectSearch && !errorProjectSearch) {
      if (projectSearch.length !== 0) {
        return (
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Row gutter={[0, 32]}>
              {projectSearch.map((item) => {
                return (
                  <Col key={item.id} span={24}>
                    <Row>
                      <Col span={24}>
                        <Title
                          level={4}
                          style={{
                            ...TitleStyle,
                            ...SearchRedirectStyle,
                            color: '#E7B12E'
                          }}
                        >
                          {textTranslate(
                            language,
                            item?.eng?.title,
                            item?.ina?.title
                          )}
                        </Title>
                      </Col>
                      <Col span={24} style={SearchContentStyle}>
                        <Text>
                          <div
                            dangerouslySetInnerHTML={{
                              __html: textTranslate(
                                language,
                                item?.eng?.description,
                                item?.ina?.description
                              )
                            }}
                          ></div>
                        </Text>
                      </Col>
                    </Row>
                  </Col>
                )
              })}
            </Row>
          </Col>
        )
      } else {
        return (
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <RenderNoData />
          </Col>
        )
      }
    } else {
      return <RenderErrorLoadingProjectSearch />
    }
  }

  const RenderErrorLoadingProjectSearch = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1]
    let dummyRow = [0, 1, 2, 3]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[0, 16]}>
            {dummyLoading.map((item) => {
              return (
                <Col key={item} span={24}>
                  <Row gutter={[0, 16]}>
                    <Col span={12}>
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </Col>
                    {dummyRow.map((data) => {
                      return (
                        <Col key={data} span={24}>
                          <PilarLoading
                            type='black'
                            height='20px'
                            borderRadius='5px'
                          />
                        </Col>
                      )
                    })}
                  </Row>
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorProjectSearch ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingProjectSearch}
              onClick={() =>
                getProjectSearch(pageProjectSearch, search, language)
              }
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderPaginationProjectSearch = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ textAlign: 'center' }}
        >
          <Pagination
            className='PilarPagination'
            responsive
            hideOnSinglePage
            current={pageProjectSearch}
            total={totalProjectSearch}
            pageSize={per}
            itemRender={itemRender}
            onChange={(page) => {
              setPageProjectSearch(page)
            }}
          />
        </Col>
      </Row>
    )
  }

  const RenderNewsSearch = () => {
    if (!loadingNewsSearch && !errorNewsSearch) {
      if (newsSearch.length !== 0) {
        return (
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Row gutter={[16, 16]}>
              {newsSearch.map((data) => {
                return (
                  <Col
                    xs={24}
                    md={8}
                    lg={8}
                    key={data.id}
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      RedirectPage(data.id)
                    }}
                  >
                    <NewsCard data={data} />
                  </Col>
                )
              })}
            </Row>
          </Col>
        )
      } else {
        return (
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <RenderNoData />
          </Col>
        )
      }
    } else {
      return <RenderErrorLoadingNewsSearch />
    }
  }

  const RenderErrorLoadingNewsSearch = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2, 3, 4, 5]
    let dummyRow = [0, 1, 2]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[16, 16]}>
            {dummyLoading.map((data) => {
              return (
                <Col xs={24} md={8} lg={8} key={data}>
                  <Row>
                    <Col span={24} style={{ marginBottom: '1em' }}>
                      <PilarLoading
                        type='black'
                        height='150px'
                        borderRadius='10px'
                      />
                    </Col>
                    <Col span={24}>
                      {dummyRow.map((item) => {
                        return (
                          <Col
                            span={24}
                            style={item < 2 ? { marginBottom: '10px' } : null}
                            key={item}
                          >
                            <PilarLoading
                              type='black'
                              height='20px'
                              borderRadius='5px'
                            />
                          </Col>
                        )
                      })}
                    </Col>
                  </Row>
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorNewsSearch ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingNewsSearch}
              onClick={() => getNewsSearch(pageNewsSearch, search, language)}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderPaginationNewsSearch = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ textAlign: 'center' }}
        >
          <Pagination
            className='PilarPagination'
            responsive
            hideOnSinglePage
            current={pageNewsSearch}
            total={totalNewsSearch}
            pageSize={per}
            itemRender={itemRender}
            onChange={(page) => {
              setPageNewsSearch(page)
            }}
          />
        </Col>
      </Row>
    )
  }

  const itemRender = (current, type, originalElement) => {
    if (type === 'prev') {
      return <span style={PaginationButtonStyle}>{t('pagination.prev')}</span>
    }
    if (type === 'next') {
      return <span style={PaginationButtonStyle}>{t('pagination.next')}</span>
    }
    return originalElement
  }

  const RenderNoData = () => {
    let message = `${t(`menu.${state.type}`)} ${t('text.notFound')}`
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={message} />
  }

  const RenderSearchResult = () => {
    return (
      <>
        <Row className='WrapperContentClass' style={{ paddingTop: 0 }}>
          {isSearchProject() ? <RenderProjectSearch /> : <RenderNewsSearch />}
        </Row>
        {isSearchProject() ? (
          <RenderPaginationProjectSearch />
        ) : (
          <RenderPaginationNewsSearch />
        )}
      </>
    )
  }

  return (
    <div>
      <RenderHeaderContent />
      <RenderSearchInput
        setSearchValue={handleOnChange}
        searchValue={search}
        searchPlaceholder={
          isSearchProject()
            ? t('project.searchPlaceholder')
            : t('news.searchPlaceholder')
        }
        isSearchProject={isSearchProject}
        handleSearchProject={handleSearchProject}
        handleSearchNews={handleSearchNews}
      />
      <RenderSearchResult />
      <CareerContactContent />
    </div>
  )
}

export default Search

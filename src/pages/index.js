import { lazy } from 'react'

export const HomePage = lazy(() => import('./Home'))
export const AboutPage = lazy(() => import('./About'))
export const BusinessPage = lazy(() => import('./Business'))
export const CareerPage = lazy(() => import('./Career'))
export const ContactPage = lazy(() => import('./Contact'))
export const GalleryPage = lazy(() => import('./Gallery'))
export const NewsPage = lazy(() => import('./News'))
export const NewsDetailPage = lazy(() => import('./News/NewsDetail'))
export const ProjectPage = lazy(() => import('./Project'))
export const CSRPage = lazy(() => import('./CSR'))
export const CSRDetailPage = lazy(() => import('./CSR/CSRDetail'))
export const SearchPage = lazy(() => import('./Search'))

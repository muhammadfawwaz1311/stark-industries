// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Pagination, Input, Button } from 'antd'
import { NewsCard, PilarLoading } from 'components'
import { goTo, textTranslate, convertDate, checkDevice } from 'utils'
import { NewsBackground } from 'assets'
import { SearchOutlined } from '@ant-design/icons'
import { HeaderService, NewsService } from 'services'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const UpdatedNewsDeviderStyle = { borderBottom: '2px dashed #B2B2B2' }
const PaginationButtonStyle = { color: '#B0B0B0', margin: '0 0.5em' }
const NewsHeaderTitleStyle = { marginBottom: '1em' }
const NewsRedirectStyle = { cursor: 'pointer' }
const NewsDateStyle = { color: '#A6A6A6' }
const NewsContentStyle = {
  color: '#565656',
  textAlign: 'justify'
}
const NewsGutterContentStyle = { marginBottom: '3em' }
const LoadingSpace = { marginBottom: '10px' }

const News = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Header Content
  const page = 'news'
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // const [loadingPage, setLoadingPage] = useState(true)

  // News Content
  const per = 4
  const [breakingNews, setBreakingNews] = useState({})
  const [updatedNews, setUpdatedNews] = useState([])
  const [loadingNewsList, setLoadingNewsList] = useState(false)
  const [errorNewsList, setErrorNewsList] = useState(false)
  const [totalNewsList, setTotalNewsList] = useState(0)
  const [allNews, setAllNews] = useState([])
  const [loadingAllNews, setLoadingAllNews] = useState(false)
  const [errorAllNews, setErrorAllNews] = useState(false)
  const [pageAllNews, setPageAllNews] = useState(1)
  const [totalAllNews, setTotalAllNews] = useState(0)
  // News Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
      getNewsList()

      // setTimeout(() => {
      //   setLoadingPage(false)
      // }, 1000)
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getAllNews(pageAllNews)
    }

    return () => {
      subscribe = false
    }
  }, [pageAllNews])

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      const getHeaderContent = await HeaderService.getHeaderContent(page)
      if (getHeaderContent && getHeaderContent.status === 200) {
        let data = getHeaderContent?.data?.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getNewsList = async () => {
    const page = 1
    setLoadingNewsList(true)
    try {
      const getNewsList = await NewsService.getNewsList(page, per)
      if (getNewsList && getNewsList.status === 200) {
        let newsList = getNewsList?.data?.data?.news
        let totalNewsList = newsList.length
        let updated = []
        newsList?.map((news, index) => {
          if (index === 0) {
            setBreakingNews(news)
          } else {
            updated.push(news)
          }
          return null
        })
        setUpdatedNews(updated)
        setTotalNewsList(totalNewsList)
        setErrorNewsList(false)
      } else {
        setErrorNewsList(true)
      }
    } catch (error) {
      setErrorNewsList(true)
    }
    setLoadingNewsList(false)
  }

  const getAllNews = async (pageAllNews) => {
    setLoadingAllNews(true)
    try {
      const getAllNews = await NewsService.getNewsList(pageAllNews + 1, per)
      if (getAllNews && getAllNews.status === 200) {
        let allNews = getAllNews?.data?.data?.news
        let totalAllNews = getAllNews?.data?.pagination?.totalCount
        setAllNews(allNews)
        setTotalAllNews(totalAllNews)
        setErrorAllNews(false)
      } else {
        setErrorAllNews(true)
      }
    } catch (error) {
      setErrorAllNews(true)
    }
    setLoadingAllNews(false)
  }

  const RedirectPage = (url) => {
    let param = {
      location: `/news/detail/${url}`
    }
    goTo(param)
  }

  const handleSearch = (value) => {
    const params = {
      location: `/search`,
      state: {
        type: 'news',
        searchValue: value
      }
    }
    goTo(params)
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : NewsBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
              <div>
                <Input
                  allowClear
                  className='PilarSearchAbsolute'
                  placeholder={t('news.searchPlaceholder')}
                  prefix={<SearchOutlined />}
                  onPressEnter={(event) => handleSearch(event.target.value)}
                />
              </div>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderBreakingNewsContent = () => {
    return (
      <Col
        xs={24}
        md={24}
        lg={{ offset: 2, span: 20 }}
        onClick={() => {
          RedirectPage(breakingNews.id)
        }}
        style={NewsRedirectStyle}
      >
        <Row gutter={[16, 16]}>
          <Col
            xs={{ span: 24, order: 2 }}
            md={{ span: 12, order: 1 }}
            lg={{ span: 12, order: 1 }}
          >
            <Row>
              <Col span={24}>
                <Title level={4} style={TitleStyle}>
                  {textTranslate(
                    language,
                    breakingNews?.eng?.title ? breakingNews?.eng?.title : '',
                    breakingNews?.ina?.title ? breakingNews?.ina?.title : ''
                  )}
                </Title>
              </Col>
              <Col span={24}>
                <Text style={NewsDateStyle}>
                  {`${t('text.uploaded')} ${convertDate(
                    breakingNews.createdAt
                  )}`}
                </Text>
              </Col>
              <Col span={24} style={NewsContentStyle}>
                <Text className='trimString-6'>
                  <div
                    dangerouslySetInnerHTML={{
                      __html: textTranslate(
                        language,
                        breakingNews?.eng?.body ? breakingNews?.eng?.body : '',
                        breakingNews?.ina?.body ? breakingNews?.ina?.body : ''
                      )
                    }}
                  ></div>
                </Text>
              </Col>
            </Row>
          </Col>
          <Col
            xs={{ span: 24, order: 1 }}
            md={{ span: 12, order: 2 }}
            lg={{ span: 12, order: 2 }}
          >
            <div className='news-card-image-wrapper'>
              <img src={breakingNews.imageUrl} alt='' />
            </div>
          </Col>
        </Row>
      </Col>
    )
  }

  const RenderUpdatedNews = () => {
    return (
      <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
        <Row gutter={[16, 16]} style={UpdatedNewsDeviderStyle}>
          {updatedNews.map((data) => {
            return (
              <Col
                xs={24}
                md={8}
                lg={8}
                key={data.id}
                style={{ ...NewsRedirectStyle, ...NewsGutterContentStyle }}
                onClick={() => {
                  RedirectPage(data.id)
                }}
              >
                <NewsCard data={data} />
              </Col>
            )
          })}
        </Row>
      </Col>
    )
  }

  const RenderBreakingUpdatedNews = () => {
    if (errorNewsList) {
      return <RenderErrorNewsList />
    } else if (!loadingNewsList && !errorNewsList) {
      return (
        <>
          <RenderBreakingNewsContent />
          <RenderUpdatedNews />
        </>
      )
    } else {
      return <RenderLoadingNewsList />
    }
  }

  const RenderLoadingNewsList = () => {
    return (
      <>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[16, 16]}>
            <Col
              xs={{ span: 24, order: 2 }}
              md={{ span: 12, order: 1 }}
              lg={{ span: 12, order: 1 }}
            >
              <Row>
                <Col span={24} style={NewsContentStyle}>
                  {[0, 1, 2, 3].map((item) => {
                    return (
                      <Col
                        span={24}
                        style={item < 3 ? LoadingSpace : null}
                        key={item}
                      >
                        <PilarLoading
                          type='black'
                          height='30px'
                          borderRadius='5px'
                        />
                      </Col>
                    )
                  })}
                </Col>
              </Row>
            </Col>
            <Col
              xs={{ span: 24, order: 1 }}
              md={{ span: 12, order: 2 }}
              lg={{ span: 12, order: 2 }}
            >
              <PilarLoading type='black' height='150px' borderRadius='10px' />
            </Col>
          </Row>
        </Col>
        <Col xs={0} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[16, 16]} style={UpdatedNewsDeviderStyle}>
            {[0, 1, 2].map((data) => {
              return (
                <Col
                  xs={24}
                  md={8}
                  lg={8}
                  key={data}
                  style={NewsGutterContentStyle}
                >
                  <Row>
                    <Col span={24} style={NewsHeaderTitleStyle}>
                      <PilarLoading
                        type='black'
                        height='150px'
                        borderRadius='10px'
                      />
                    </Col>
                    <Col span={24} style={NewsContentStyle}>
                      {[0, 1, 2].map((item) => {
                        return (
                          <Col
                            span={24}
                            style={item < 2 ? LoadingSpace : null}
                            key={item}
                          >
                            <PilarLoading
                              type='black'
                              height='30px'
                              borderRadius='5px'
                            />
                          </Col>
                        )
                      })}
                    </Col>
                  </Row>
                </Col>
              )
            })}
          </Row>
        </Col>
      </>
    )
  }

  const RenderErrorNewsList = () => {
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <RenderLoadingNewsList />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingNewsList}
            onClick={() => getNewsList()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </Col>
    )
  }

  const RenderAllNews = () => {
    if (errorAllNews) {
      return <RenderErrorAllNews />
    } else if (!loadingAllNews && !errorAllNews) {
      return (
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          {allNews.map((data) => {
            return (
              <Row
                gutter={[16, 16]}
                style={{ ...NewsRedirectStyle, ...NewsGutterContentStyle }}
                key={data.id}
                onClick={() => {
                  RedirectPage(data.id)
                }}
              >
                <Col xs={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }}>
                  <div className='news-card-image-wrapper'>
                    <img src={data.imageUrl} alt='' />
                  </div>
                </Col>
                <Col xs={{ span: 24 }} md={{ span: 16 }} lg={{ span: 16 }}>
                  <Row>
                    <Col span={24}>
                      <Title
                        level={4}
                        style={TitleStyle}
                        className='trimString-2'
                      >
                        {textTranslate(
                          language,
                          data?.eng?.title ? data?.eng?.title : '',
                          data?.ina?.title ? data?.ina?.title : ''
                        )}
                      </Title>
                    </Col>
                    <Col span={24}>
                      <Text style={NewsDateStyle}>
                        {`${t('text.uploaded')} ${convertDate(data.createdAt)}`}
                      </Text>
                    </Col>
                    <Col span={24} style={NewsContentStyle}>
                      <Text className='trimString-5'>
                        <div
                          dangerouslySetInnerHTML={{
                            __html: textTranslate(
                              language,
                              data?.eng?.body ? data?.eng?.body : '',
                              data?.ina?.body ? data?.ina?.body : ''
                            )
                          }}
                        ></div>
                      </Text>
                    </Col>
                  </Row>
                </Col>
              </Row>
            )
          })}
        </Col>
      )
    } else {
      return <RenderLoadingAllNews />
    }
  }

  const RenderLoadingAllNews = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1]
    return (
      <>
        {dummyLoading.map((data) => {
          return (
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }} key={data}>
              <Row gutter={[16, 16]} style={NewsGutterContentStyle}>
                <Col xs={{ span: 24 }} md={{ span: 8 }} lg={{ span: 8 }}>
                  <PilarLoading
                    type='black'
                    height='150px'
                    borderRadius='10px'
                  />
                </Col>
                <Col xs={{ span: 24 }} md={{ span: 16 }} lg={{ span: 16 }}>
                  <Row>
                    {[0, 1, 2, 3].map((item) => {
                      return (
                        <Col
                          span={24}
                          style={item < 3 ? LoadingSpace : null}
                          key={item}
                        >
                          <PilarLoading
                            type='black'
                            height='30px'
                            borderRadius='5px'
                          />
                        </Col>
                      )
                    })}
                  </Row>
                </Col>
              </Row>
            </Col>
          )
        })}
      </>
    )
  }

  const RenderErrorAllNews = () => {
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <RenderLoadingAllNews />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingAllNews}
            onClick={() => getAllNews(pageAllNews)}
          >
            {t('button.reload')}
          </Button>
        </div>
      </Col>
    )
  }

  const itemRender = (current, type, originalElement) => {
    if (type === 'prev') {
      return <span style={PaginationButtonStyle}>{t('pagination.prev')}</span>
    }
    if (type === 'next') {
      return <span style={PaginationButtonStyle}>{t('pagination.next')}</span>
    }
    return originalElement
  }

  const RenderPagination = () => {
    return (
      <Col
        xs={24}
        md={24}
        lg={{ offset: 2, span: 20 }}
        style={{ textAlign: 'center' }}
      >
        <Pagination
          className='PilarPagination'
          responsive
          hideOnSinglePage
          current={pageAllNews}
          total={totalAllNews - totalNewsList}
          pageSize={per}
          itemRender={itemRender}
          onChange={(page) => {
            setPageAllNews(page)
          }}
        />
      </Col>
    )
  }

  // if (loadingPage) {
  //   return <PilarSplashScreen />
  // }
  return (
    <div>
      {/* <PilarSEO type={page} /> */}
      <RenderHeaderContent />
      <Row className='WrapperContentClass' gutter={[0, 16]}>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Title
            level={3}
            style={{ ...TitleStyle, color: '#E7B12E', marginBottom: 0 }}
          >
            {t('news.title')}
          </Title>
        </Col>
        <RenderBreakingUpdatedNews />
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Title level={3} style={{ ...TitleStyle, color: '#E7B12E' }}>
            {t('news.allArticles')}
          </Title>
        </Col>
        <RenderAllNews />
        <RenderPagination />
      </Row>
    </div>
  )
}

export default News

import { AboutBackground, BusinessBackground } from 'assets'

const NewsDummy = [
  {
    newsId: 1,
    newsTitle: 'Ini Penyebab Produksi Semen Juli 2020 Merosot',
    newsDate: '12 Oktober 2020',
    newsContent: `<p>Emiten kontruksi PT Total Bangun Persada Tbk. (TOTL) membidik kontrak proyek konstruksi senilai Rp5,45 triliun sepanjang 2019. Direktur Total Bangun Persada Moeljati Soetrisno menyatakan pada 2019 perseroan akan menyelesaikan 20 proyek dari perjanjian kontruksi pada tahun lalu. Selain itu, TOTL juga mengincar beberapa kontrak baru.</p>
      <p>&ldquo;Fokus perusahaan masih menggarap highrise building. Jadi proyek masih on going itu sekitar 20-25 proyek. Itu kan sudah mau selesai, nanti ada proyek baru lagi," kata Moeljati di Jakarta, Kamis (2/5).</p>
      <p>Moeljati mengungkapkan proyek tersebut terdiri atas apartemen senilai Rp1,6 triliun, mixed used development Rp2,3 trriliun, shopping center Rp980 miliar, hotel Rp400 miliar, dan sekolah sekitar Rp170 miliar.</p>
      <p>"Jadi pipeline project kami ini terdiri atas private project, dengan porsi pelanggan berulang 38% dan pelanggan baru 68%," ucapnya.</p>
      <p>Beberapa proyek yang sedang dibangun TOTL tahun ini di antaranya Orange County City Centre Residental di kawasan Meikarta Cikarang, Lavie All Suites Jakarta, Milleniyn Village Lippo karawaci, dan The Smith Alam Sutera.</p>
      <p>Sementara, Moeljati optimistis pada tahun ini perusahaan bisa merealisasikan kontrak senilai Rp4 triliun dari total pipeline project tersebut. Perhitungan itu diambil berdasarkan realitas yang ada.</p>
      <p>Menurut dia, usai pemilihan umum (pemilu) pelaku pasar akan kembali melanjutkan proyek pembangunan dan tidak bersikap wait and see.</p>
      <p>"Pembangunan itu kan pasti terus berlangsung, jadi saya optimistis dan memandang bahwa sektor konstruksi tetap positif tahun ini," kata Moeljati.</p>`,
    newsGallery: AboutBackground
  },
  {
    newsId: 2,
    newsTitle:
      'Ada 140 Ribu Pekerja, Kementerian PUPR Jamin Keberlanjutan Sektor Konstruksi',
    newsDate: '12 Oktober 2020',
    newsContent:
      'Kementerian Pekerjaan Umum dan Perumahan Rakyat (PUPR) menjamin keberlanjutan sektor konstruksi di tengah pandemi Covid-19 apalagi sektor ini menyerap ribuan pekerja. Menteri PUPR Basuki Hadimuljono mengatak.',
    newsGallery: BusinessBackground
  },
  {
    newsId: 3,
    newsTitle:
      'Konstruksi Rumah Sakit Mata Pusat Manado Tuntas Akhir Tahun 2020',
    newsDate: '12 Oktober 2020',
    newsContent:
      'Hingga saat ini, progres konstruksi Rumah Sakit Mata Manado mencapai 57,63 persen per September dan ditargetkan rampung akhir tahun ini atau tepatnya 27 Desember 2020.',
    newsGallery: AboutBackground
  },
  {
    newsId: 4,
    newsTitle:
      'Cegah banjir, Bendungan Ciawi dan Sukamahi ditarget selesai 2020',
    newsDate: '12 Oktober 2020',
    newsContent:
      'Kementerian Pekerjaan Umum dan Perumahan Rakyat (PUPR) melalui Balai Besar Wilayah Sungai (BBWS) Ciliwung-Cisadane menargetkan pembangunan Bendungan Ciawi.',
    newsGallery: AboutBackground
  },
  {
    newsId: 5,
    newsTitle: 'Erick Thohir genjot proyek kereta cepat Jakarta-Bandung',
    newsDate: '12 Oktober 2020',
    newsContent:
      'Menteri Badan Usaha Milik Negara (BUMN) Erick Thohir mengatakan pemerintah tengah menggenjot pembangunan kereta cepat Jakarta-Bandung dengan pembentukan tim satuan tugas khusus (task force) agar mencapai target operasi pada 2021. Dia melanjutkan, tim satuan tugas khusus untuk percepatan pembangunan jalur kereta.',
    newsGallery: AboutBackground
  },
  {
    newsId: 6,
    newsTitle: 'Pemerintah cari dana asing biayai proyek strategis nasional',
    newsDate: '12 Oktober 2020',
    newsContent:
      'Komite Percepatan Penyediaan Infrastruktur Prioritas (KPPIP) menyatakan pemerintah akan mencari pendanaan swasta terutama dari luar negeri untyk membangun proyek strategis nasional (PSN) dalam lima tahun ke depan. Ketua Tim Pelaksana KPPIP Wahyu Utomo mengatakan kebutuhan investasi untuk membiayai PSN hingga 2024.',
    newsGallery: AboutBackground
  },
  {
    newsId: 7,
    newsTitle: 'Formula E di Jakarta, Jakpro butuh duit ratusan miliar',
    newsDate: '12 Oktober 2020',
    newsContent:
      'PT Jakarta Propertindo (Jakpro) mengaku akan mengebut pembangunan infrastruktur Formula E setelah uang Pernyataan Modal Daerah (PMD) cair dari Pemprov DKI Jakarta. Direktur Utama PT Jakarta Properti Dwi Wahyu Daryanto mengaku memiliki tenggat waktu dalam pengerjaan infrastruktur tersebut. Tenggat waktu itu telah ditentukan oleh.',
    newsGallery: AboutBackground
  },
  {
    newsId: 8,
    newsTitle: 'Total Bangun Persada bidik kontrak Rp5,45 triliun tahun ini',
    newsDate: '12 Oktober 2020',
    newsContent:
      'Emiten kontruksi PT Total Bangun Persada Tbk. (TOTL) membidik kontrak proyek konstruksi senilai Rp5,45 triliun sepanjang 2019. Direktur Total Bangun Persada Moeljati Soetrisno menyatakan pada 2019 perseroan akan menyelesaikan 20 proyek dari perjanjian kontruksi pada tahun lalu. Selain itu, TOTL juga mengincar.',
    newsGallery: AboutBackground
  }
]

export { NewsDummy }

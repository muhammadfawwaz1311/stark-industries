// dependencies
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Row, Col, Typography, Button, Image } from 'antd'
import { NewsCard, PilarLoading } from 'components'
import { goTo, textTranslate, convertDate, checkDevice } from 'utils'
import { NewsBackground } from 'assets'
import { HeaderService, NewsService } from 'services'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const NewsHeaderTitleStyle = { marginBottom: '1em' }
const NewsRedirectStyle = { cursor: 'pointer' }
const NewsDateStyle = { color: '#E7B12E' }
const NewsContentStyle = {
  color: '#565656',
  textAlign: 'justify'
}
const NewsGutterContentStyle = { marginBottom: '3em' }
const LoadingSpace = { marginBottom: '10px' }

const NewsDetail = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  const { paramId } = useParams()

  // Header Content
  const page = 'news'
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // News Detail Content
  const [newsDetail, setNewsDetail] = useState({})
  const [loadingNewsDetail, setLoadingNewsDetail] = useState(false)
  const [errorNewsDetail, setErrorNewsDetail] = useState(false)
  const [newsAnother, setNewsAnother] = useState([])
  const [loadingNewsAnother, setLoadingNewsAnother] = useState(false)
  const [errorNewsAnother, setErrorNewsAnother] = useState(false)
  // News Detail Content

  // Initial

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
    }

    return () => {
      subscribe = false
    }
  }, [])

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getNewsDetail(paramId)
      getNewsAnother(paramId)
    }

    return () => {
      subscribe = false
    }
  }, [paramId])
  // Initial

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      const getHeaderContent = await HeaderService.getHeaderContent(page)
      if (getHeaderContent && getHeaderContent.status === 200) {
        let data = getHeaderContent?.data?.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getNewsDetail = async (paramId) => {
    setLoadingNewsDetail(true)
    try {
      const getNewsDetail = await NewsService.getNewsDetail(paramId)
      if (getNewsDetail && getNewsDetail.status === 200) {
        let data = getNewsDetail?.data?.data
        setNewsDetail(data)
        setErrorNewsDetail(false)
      } else {
        setErrorNewsDetail(true)
      }
    } catch (error) {
      setErrorNewsDetail(true)
    }
    setLoadingNewsDetail(false)
  }

  const getNewsAnother = async (paramId) => {
    setLoadingNewsAnother(true)
    try {
      const getNewsAnother = await NewsService.getNewsAnother(paramId)
      if (getNewsAnother && getNewsAnother.status === 200) {
        let data = getNewsAnother?.data?.data?.news
        setNewsAnother(data)
        setErrorNewsAnother(false)
      } else {
        setErrorNewsAnother(true)
      }
    } catch (error) {
      setErrorNewsAnother(true)
    }
    setLoadingNewsAnother(false)
  }

  const RedirectPage = (url) => {
    let param = {
      location: `/news/detail/${url}`
    }
    goTo(param)
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : NewsBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderNewsDetailContent = () => {
    if (errorNewsDetail) {
      return <RenderErrorNewsDetail />
    } else if (!loadingNewsDetail && !errorNewsDetail) {
      return (
        <Row gutter={[0, 16]} className='WrapperContentClass'>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Title level={2} style={{ ...TitleStyle, textAlign: 'center' }}>
              {textTranslate(
                language,
                newsDetail?.eng?.title ? newsDetail?.eng?.title : '',
                newsDetail?.ina?.title ? newsDetail?.ina?.title : ''
              )}
            </Title>
          </Col>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Image
              src={newsDetail.imageUrl ? newsDetail.imageUrl : ''}
              alt=''
              className='detail-image-size'
            />
          </Col>
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            style={{ marginBottom: '0.5em' }}
          >
            <Text style={NewsDateStyle}>
              {`${t('text.uploaded')} ${convertDate(newsDetail.createdAt)}`}
            </Text>
          </Col>
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            style={NewsContentStyle}
          >
            <Text>
              <div
                dangerouslySetInnerHTML={{
                  __html: textTranslate(
                    language,
                    newsDetail?.eng?.body ? newsDetail?.eng?.body : '',
                    newsDetail?.ina?.body ? newsDetail?.ina?.body : ''
                  )
                }}
              ></div>
            </Text>
          </Col>
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            className='WrapperDividerClass'
          />
        </Row>
      )
    } else {
      return <RenderLoadingNewsDetail />
    }
  }

  const RenderLoadingNewsDetail = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col span={24} style={LoadingSpace}>
          <PilarLoading type='black' height='30vh' borderRadius='0' />
        </Col>
        <Col offset={8} span={8} style={LoadingSpace}>
          <PilarLoading type='black' height='30px' borderRadius='5px' />
        </Col>
        <Col offset={2} span={20} style={NewsContentStyle}>
          {[0, 1, 2, 3, 4, 5].map((item) => {
            return (
              <Col span={24} style={item < 5 ? LoadingSpace : null} key={item}>
                <PilarLoading type='black' height='30px' borderRadius='5px' />
              </Col>
            )
          })}
        </Col>
      </Row>
    )
  }

  const RenderErrorNewsDetail = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingNewsDetail />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingNewsDetail}
            onClick={() => getNewsDetail(paramId)}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderNewsAnotherContent = () => {
    if (errorNewsAnother) {
      return <RenderErrorNewsAnother />
    } else if (!loadingNewsAnother && !errorNewsAnother) {
      return (
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[16, 16]}>
            {newsAnother?.map((data) => {
              return (
                <Col
                  xs={24}
                  md={8}
                  lg={8}
                  key={data.id}
                  style={{ ...NewsRedirectStyle, ...NewsGutterContentStyle }}
                  onClick={() => {
                    RedirectPage(data.id)
                  }}
                >
                  <NewsCard data={data} />
                </Col>
              )
            })}
          </Row>
        </Col>
      )
    } else {
      return <RenderLoadingNewsAnother />
    }
  }

  const RenderLoadingNewsAnother = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2]
    return (
      <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
        <Row gutter={[16, 16]}>
          {dummyLoading.map((data) => {
            return (
              <Col
                xs={24}
                md={8}
                lg={8}
                key={data}
                style={NewsGutterContentStyle}
              >
                <Row>
                  <Col span={24} style={NewsHeaderTitleStyle}>
                    <PilarLoading
                      type='black'
                      height='150px'
                      borderRadius='10px'
                    />
                  </Col>
                  <Col span={24} style={NewsContentStyle}>
                    {[0, 1, 2].map((item) => {
                      return (
                        <Col
                          span={24}
                          style={item < 2 ? LoadingSpace : null}
                          key={item}
                        >
                          <PilarLoading
                            type='black'
                            height='30px'
                            borderRadius='5px'
                          />
                        </Col>
                      )
                    })}
                  </Col>
                </Row>
              </Col>
            )
          })}
        </Row>
      </Col>
    )
  }

  const RenderErrorNewsAnother = () => {
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <RenderLoadingNewsAnother />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingNewsAnother}
            onClick={() => getNewsAnother(paramId)}
          >
            {t('button.reload')}
          </Button>
        </div>
      </Col>
    )
  }

  // if (loadingPage) {
  //   return <PilarSplashScreen />
  // }
  return (
    <div>
      <RenderHeaderContent />
      <RenderNewsDetailContent />
      <Row className='WrapperContentClass' gutter={[0, 16]}>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={NewsHeaderTitleStyle}
        >
          <Title
            level={3}
            style={{ ...TitleStyle, color: '#E7B12E', marginBottom: 0 }}
          >
            {t('news.allArticles')}
          </Title>
        </Col>
        <RenderNewsAnotherContent />
      </Row>
    </div>
  )
}

export default NewsDetail

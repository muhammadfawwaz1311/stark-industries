// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Pagination, Card, Select, Button } from 'antd'
import { CSRCard, PilarLoading } from 'components'
import { textTranslate, checkDevice } from 'utils'
import { CSRBackground } from 'assets'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { HeaderService, CSRService } from 'services'

const { Title } = Typography
const { Option } = Select
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const PaginationButtonStyle = { color: '#B0B0B0', margin: '0 0.5em' }
const CSRHeaderTitleStyle = { marginBottom: '1em' }
const CSRRedirectStyle = { cursor: 'pointer' }
const CSRDividerStyle = {
  borderBottom: '1px dashed #B2B2B2',
  paddingBottom: '1em'
}
const CSRGutterContentStyle = { marginBottom: '3em' }
const CSRCardStyle = { padding: 0 }
const CSRCardContentStyle = { padding: '0 1em' }
const MoreContentStyle = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'flex-end',
  height: '100%',
  width: 'fit-content',
  cursor: 'pointer',
  marginLeft: 'auto'
}

const CSR = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Header Content
  const page = 'csr'
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // const [loadingPage, setLoadingPage] = useState(true)

  // CSR Content
  const per = 6
  const defaultOrderBy = 'createdAt'
  const defaultDir = 'desc'
  const [cSRList, setCSRList] = useState([])
  const [loadingCSRList, setLoadingCSRList] = useState(false)
  const [errorCSRList, setErrorCSRList] = useState(false)
  const [totalCSR, setTotalCSR] = useState(0)
  const [pageCSR, setPageCSR] = useState(1)
  const [orderBy, setOrderBy] = useState(defaultOrderBy)
  const [dir, setDir] = useState(defaultDir)
  const [sortBy, setSortBy] = useState()
  const optionList = [
    { title: t('sortBy.AZ'), value: 'title-asc' },
    { title: t('sortBy.ZA'), value: 'title-desc' },
    { title: t('sortBy.newest'), value: 'createdAt-desc' },
    { title: t('sortBy.oldest'), value: 'createdAt-asc' }
  ]
  // CSR Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()

      // setTimeout(() => {
      //   setLoadingPage(false)
      // }, 1000)
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getCSRList(pageCSR, per, orderBy, dir)
    }

    return () => {
      subscribe = false
    }
  }, [pageCSR, orderBy, dir])

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      const getHeaderContent = await HeaderService.getHeaderContent(page)
      if (getHeaderContent && getHeaderContent.status === 200) {
        let data = getHeaderContent?.data?.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getCSRList = async (pageCSR, per, orderBy, dir) => {
    setLoadingCSRList(true)
    try {
      const getCSRList = await CSRService.getCSRList(pageCSR, per, orderBy, dir)
      if (getCSRList && getCSRList.status === 200) {
        let CSR = getCSRList?.data?.data?.csrs
        let totalCSR = getCSRList?.data?.pagination?.totalCount
        setCSRList(CSR)
        setTotalCSR(totalCSR)
        setErrorCSRList(false)
      } else {
        setErrorCSRList(true)
      }
    } catch (error) {
      setErrorCSRList(true)
    }
    setLoadingCSRList(false)
  }

  const handleChangeSort = (value) => {
    setSortBy(value)
    if (!value) {
      setOrderBy(defaultOrderBy)
      setDir(defaultDir)
    } else {
      let sortArr = []
      sortArr = value.split('-')
      if (sortArr[0] === 'title') {
        setOrderBy(language === 'ENG' ? 'titleEng' : 'titleIna')
        setDir(sortArr[1])
      } else {
        setOrderBy(sortArr[0])
        setDir(sortArr[1])
      }
    }
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : CSRBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderSort = () => {
    return (
      <Select
        placeholder={t('sortBy.placeholder')}
        style={{ width: checkDevice() === 'Mobile' ? 100 : 150 }}
        value={sortBy}
        onChange={handleChangeSort}
        allowClear
      >
        {optionList.map((data, index) => {
          return (
            <Option key={index} value={data.value}>
              {data.title}
            </Option>
          )
        })}
      </Select>
    )
  }

  const itemRender = (current, type, originalElement) => {
    if (type === 'prev') {
      return <span style={PaginationButtonStyle}>{t('pagination.prev')}</span>
    }
    if (type === 'next') {
      return <span style={PaginationButtonStyle}>{t('pagination.next')}</span>
    }
    return originalElement
  }

  const RenderPagination = () => {
    return (
      <Col
        xs={24}
        md={24}
        lg={{ offset: 2, span: 20 }}
        style={{ textAlign: 'center', marginTop: '3em' }}
      >
        <Pagination
          className='PilarPagination'
          responsive
          hideOnSinglePage
          current={pageCSR}
          total={totalCSR}
          pageSize={per}
          itemRender={itemRender}
          onChange={(page) => setPageCSR(page)}
        />
      </Col>
    )
  }

  const RenderCSRContent = () => {
    if (errorCSRList) {
      return <RenderErrorCSRContent />
    } else if (!loadingCSRList && !errorCSRList) {
      return (
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[16, 16]}>
            {cSRList.map((data) => {
              return (
                <Col xs={24} md={8} lg={8} key={data.id}>
                  <CSRCard data={data} />
                </Col>
              )
            })}
          </Row>
        </Col>
      )
    } else {
      return <RenderLoadingCSRContent />
    }
  }

  const RenderLoadingCSRContent = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2, 3, 4, 5]
    return (
      <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
        <Row gutter={[16, 16]}>
          {dummyLoading.map((data) => {
            return (
              <Col
                xs={24}
                md={8}
                lg={8}
                key={data}
                style={CSRGutterContentStyle}
              >
                <Card hoverable bodyStyle={CSRCardStyle}>
                  <Col
                    span={24}
                    style={{ ...CSRHeaderTitleStyle, ...CSRCardStyle }}
                  >
                    <PilarLoading
                      type='black'
                      height='150px'
                      borderRadius='0px'
                    />
                  </Col>
                  <Row style={CSRCardContentStyle}>
                    <Col span={24} style={{ marginBottom: '10px' }}>
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </Col>
                    <Col span={24} style={CSRDividerStyle}>
                      <PilarLoading
                        type='black'
                        height='10px'
                        borderRadius='5px'
                      />
                    </Col>
                    <Col
                      span={24}
                      style={{ ...CSRRedirectStyle, padding: '1em 0' }}
                    >
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </Col>
                  </Row>
                </Card>
              </Col>
            )
          })}
        </Row>
      </Col>
    )
  }

  const RenderErrorCSRContent = () => {
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <RenderLoadingCSRContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingCSRList}
            onClick={() => getCSRList(pageCSR, per, orderBy, dir)}
          >
            {t('button.reload')}
          </Button>
        </div>
      </Col>
    )
  }

  // if (loadingPage) {
  //   return <PilarSplashScreen />
  // }
  return (
    <div>
      {/* <PilarSEO type={page} /> */}
      <RenderHeaderContent />
      <Row className='WrapperContentClass'>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={CSRHeaderTitleStyle}
        >
          <Row>
            <Col xs={12} md={12} lg={12}>
              <Title level={3} style={TitleStyle}>
                {t('CSR.title')}
              </Title>
            </Col>
            <Col xs={12} md={12} lg={12}>
              <div style={MoreContentStyle}>
                <RenderSort />
              </div>
            </Col>
          </Row>
        </Col>
        <RenderCSRContent />
        <RenderPagination />
      </Row>
    </div>
  )
}

export default CSR

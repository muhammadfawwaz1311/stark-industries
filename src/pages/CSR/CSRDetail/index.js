// dependencies
import React, { useState, useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { Row, Col, Typography, Card, Button, Image } from 'antd'
import { PilarLoading, CSRCard } from 'components'
import { textTranslate, convertDate, checkDevice } from 'utils'
import { CSRBackground } from 'assets'
import { HeaderService, CSRService } from 'services'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const CSRDividerStyle = {
  borderBottom: '1px dashed #B2B2B2',
  paddingBottom: '1em'
}
const CSRHeaderTitleStyle = { marginBottom: '1em' }
const CSRRedirectStyle = { cursor: 'pointer' }
const CSRDateStyle = { color: '#E7B12E' }
const CSRContentStyle = {
  color: '#565656',
  textAlign: 'justify'
}
const CSRGutterContentStyle = { marginBottom: '3em' }
const CSRCardStyle = { padding: 0 }
const CSRCardContentStyle = { padding: '0 1em' }
const LoadingSpace = { marginBottom: '10px' }

const CSRDetail = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  const { paramId } = useParams()

  // Header Content
  const page = 'csr'
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // CSR Detail Content
  const [cSRDetail, setCSRDetail] = useState({})
  const [loadingCSRDetail, setLoadingCSRDetail] = useState(false)
  const [errorCSRDetail, setErrorCSRDetail] = useState(false)
  const [cSRAnother, setCSRAnother] = useState([])
  const [loadingCSRAnother, setLoadingCSRAnother] = useState(false)
  const [errorCSRAnother, setErrorCSRAnother] = useState(false)
  // CSR Detail Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
    }

    return () => {
      subscribe = false
    }
  }, [])

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getCSRDetail(paramId)
      getCSRAnother(paramId)
    }

    return () => {
      subscribe = false
    }
  }, [paramId])
  // Initial

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      const getHeaderContent = await HeaderService.getHeaderContent(page)
      if (getHeaderContent && getHeaderContent.status === 200) {
        let data = getHeaderContent?.data?.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getCSRDetail = async (paramId) => {
    setLoadingCSRDetail(true)
    try {
      const getCSRDetail = await CSRService.getCSRDetail(paramId)
      if (getCSRDetail && getCSRDetail.status === 200) {
        let data = getCSRDetail?.data?.data
        setCSRDetail(data)
        setErrorCSRDetail(false)
      } else {
        setErrorCSRDetail(true)
      }
    } catch (error) {
      setErrorCSRDetail(true)
    }
    setLoadingCSRDetail(false)
  }

  const getCSRAnother = async (paramId) => {
    setLoadingCSRAnother(true)
    try {
      const getCSRAnother = await CSRService.getCSRAnother(paramId)
      if (getCSRAnother && getCSRAnother.status === 200) {
        let data = getCSRAnother?.data?.data?.csrs
        setCSRAnother(data)
        setErrorCSRAnother(false)
      } else {
        setErrorCSRAnother(true)
      }
    } catch (error) {
      setErrorCSRAnother(true)
    }
    setLoadingCSRAnother(false)
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : CSRBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderCSRDetailContent = () => {
    if (errorCSRDetail) {
      return <RenderErrorCSRDetail />
    } else if (!loadingCSRDetail && !errorCSRAnother) {
      return (
        <Row gutter={[0, 16]} className='WrapperContentClass'>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Title level={2} style={{ ...TitleStyle, textAlign: 'center' }}>
              {textTranslate(
                language,
                cSRDetail?.eng?.title ? cSRDetail?.eng?.title : '',
                cSRDetail?.ina?.title ? cSRDetail?.ina?.title : ''
              )}
            </Title>
          </Col>
          <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
            <Image
              src={cSRDetail.imageUrl ? cSRDetail.imageUrl : ''}
              alt=''
              className='detail-image-size'
            />
          </Col>
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            style={{ marginBottom: '0.5em' }}
          >
            <Text style={CSRDateStyle}>
              {`${t('text.uploaded')} ${convertDate(cSRDetail.createdAt)}`}
            </Text>
          </Col>
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            style={CSRContentStyle}
          >
            <Text>
              <div
                dangerouslySetInnerHTML={{
                  __html: textTranslate(
                    language,
                    cSRDetail?.eng?.body ? cSRDetail?.eng?.body : '',
                    cSRDetail?.ina?.body ? cSRDetail?.ina?.body : ''
                  )
                }}
              ></div>
            </Text>
          </Col>
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            className='WrapperDividerClass'
          />
        </Row>
      )
    } else {
      return <RenderLoadingCSRDetail />
    }
  }

  const RenderLoadingCSRDetail = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col span={24} style={LoadingSpace}>
          <PilarLoading type='black' height='30vh' borderRadius='0' />
        </Col>
        <Col offset={8} span={8} style={LoadingSpace}>
          <PilarLoading type='black' height='30px' borderRadius='5px' />
        </Col>
        <Col offset={2} span={20} style={CSRContentStyle}>
          {[0, 1, 2].map((item) => {
            return (
              <Col span={24} style={item < 2 ? LoadingSpace : null} key={item}>
                <PilarLoading type='black' height='30px' borderRadius='5px' />
              </Col>
            )
          })}
        </Col>
      </Row>
    )
  }

  const RenderErrorCSRDetail = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingCSRDetail />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingCSRDetail}
            onClick={() => getCSRDetail(paramId)}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderAllCSR = () => {
    if (errorCSRAnother) {
      return <RenderErrorCSRAnother />
    } else if (!loadingCSRAnother && !errorCSRAnother) {
      return (
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[16, 16]}>
            {cSRAnother?.map((data) => {
              return (
                <Col xs={24} md={8} lg={8} key={data.id}>
                  <CSRCard data={data} />
                </Col>
              )
            })}
          </Row>
        </Col>
      )
    } else {
      return <RenderLoadingCSRAnother />
    }
  }

  const RenderLoadingCSRAnother = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2]
    return (
      <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
        <Row gutter={[16, 16]}>
          {dummyLoading.map((data) => {
            return (
              <Col
                xs={24}
                md={8}
                lg={8}
                key={data}
                style={CSRGutterContentStyle}
              >
                <Card hoverable bodyStyle={CSRCardStyle}>
                  <Col
                    span={24}
                    style={{ ...CSRHeaderTitleStyle, ...CSRCardStyle }}
                  >
                    <PilarLoading
                      type='black'
                      height='150px'
                      borderRadius='0px'
                    />
                  </Col>
                  <Row style={CSRCardContentStyle}>
                    <Col span={24} style={{ marginBottom: '10px' }}>
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </Col>
                    <Col span={24} style={CSRDividerStyle}>
                      <PilarLoading
                        type='black'
                        height='10px'
                        borderRadius='5px'
                      />
                    </Col>
                    <Col
                      span={24}
                      style={{ ...CSRRedirectStyle, padding: '1em 0' }}
                    >
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </Col>
                  </Row>
                </Card>
              </Col>
            )
          })}
        </Row>
      </Col>
    )
  }

  const RenderErrorCSRAnother = () => {
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <RenderLoadingCSRAnother />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingCSRAnother}
            onClick={() => getCSRAnother(paramId)}
          >
            {t('button.reload')}
          </Button>
        </div>
      </Col>
    )
  }

  // if (loadingPage) {
  //   return <PilarSplashScreen />
  // }
  return (
    <div>
      <RenderHeaderContent />
      <RenderCSRDetailContent />
      <Row className='WrapperContentClass'>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={CSRHeaderTitleStyle}
        >
          <Title level={3} style={{ ...TitleStyle, color: '#E7B12E' }}>
            {t('CSR.allCSRArticles')}
          </Title>
        </Col>
        <RenderAllCSR />
      </Row>
    </div>
  )
}

export default CSRDetail

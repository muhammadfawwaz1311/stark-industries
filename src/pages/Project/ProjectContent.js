// dependencies
import React, { useState, useEffect } from 'react'
import { Tabs, Row, Col } from 'antd'
import { ProjectLayout } from './ProjectLayout'
import { ProjectService } from 'services'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { TabPane } = Tabs

const ProjectContent = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  // Project Tab
  const projectTab = ['comingSoon', 'inProgress', 'finished']
  const per = 3
  const defaultOrderBy = 'createdAt'
  const defaultDir = 'desc'
  const [currentActiveTab, setCurrentActiveTab] = useState('comingSoon')
  const [projectList, setProjectList] = useState([])
  const [loadingProjectList, setLoadingProjectList] = useState(false)
  const [errorProjectList, setErrorProjectList] = useState(false)
  const [totalProjectList, setTotalProjectList] = useState(0)
  const [pageProjectList, setPageProjectList] = useState(1)
  const [orderBy, setOrderBy] = useState(defaultOrderBy)
  const [dir, setDir] = useState(defaultDir)
  // Project Tab

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getProjectList(pageProjectList, per, currentActiveTab, orderBy, dir)
    }

    return () => {
      subscribe = false
    }
  }, [pageProjectList, currentActiveTab, orderBy, dir])
  // Initial

  const getProjectList = async (
    pageProjectList,
    per,
    currentActiveTab,
    orderBy,
    dir
  ) => {
    setLoadingProjectList(true)
    try {
      const getProjectList = await ProjectService.getProjectList(
        pageProjectList,
        per,
        currentActiveTab,
        orderBy,
        dir
      )
      if (getProjectList && getProjectList.status === 200) {
        let data = getProjectList?.data?.data?.projects
        let total = getProjectList?.data?.pagination?.totalCount
        setProjectList(data)
        setTotalProjectList(total)
        setErrorProjectList(false)
      } else {
        setErrorProjectList(true)
      }
    } catch (error) {
      setErrorProjectList(true)
    }
    setLoadingProjectList(false)
  }

  const handleChangeSort = (value) => {
    if (!value) {
      setOrderBy(defaultOrderBy)
      setDir(defaultDir)
    } else {
      let sortArr = []
      sortArr = value.split('-')
      if (sortArr[0] === 'title') {
        setOrderBy(language === 'ENG' ? 'titleEng' : 'titleIna')
        setDir(sortArr[1])
      } else {
        setOrderBy(sortArr[0])
        setDir(sortArr[1])
      }
    }
  }

  const onChangePage = (page) => {
    setPageProjectList(page)
  }

  return (
    <Row>
      <Col span={24}>
        <Row className='WrapperContentClassTB'>
          <Col span={24}>
            <Tabs
              className='PilarTabs'
              centered
              activeKey={currentActiveTab}
              onChange={(activeKey) => {
                setCurrentActiveTab(activeKey)
                setPageProjectList(1)
              }}
            >
              {projectTab.map((tabItem) => {
                return (
                  <TabPane tab={t(`project.status.${tabItem}`)} key={tabItem}>
                    <ProjectLayout
                      projectList={projectList}
                      current={pageProjectList}
                      total={totalProjectList}
                      pageSize={per}
                      loading={loadingProjectList}
                      error={errorProjectList}
                      onChangePage={onChangePage}
                      onChangeSort={handleChangeSort}
                      refetch={() =>
                        getProjectList(
                          pageProjectList,
                          per,
                          currentActiveTab,
                          orderBy,
                          dir
                        )
                      }
                    />
                  </TabPane>
                )
              })}
            </Tabs>
          </Col>
        </Row>
      </Col>
    </Row>
  )
}

export { ProjectContent }

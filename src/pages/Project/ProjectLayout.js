// dependencies
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Typography, Pagination, Select, Button, Empty } from 'antd'
import { PilarThumb, PilarLoading } from 'components'
import { textTranslate, checkDevice, goToLink } from 'utils'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { Title, Text } = Typography
const { Option } = Select
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const PaginationButtonStyle = { color: '#B0B0B0', margin: '0 0.5em' }
const galleryApi = {
  spaceBetween: 30,
  lazy: true,
  centeredSlides: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false
  }
}
const thumbApi = {
  spaceBetween: 5,
  slidesPerView: 4,
  touchRatio: 0.2,
  slideToClickedSlide: true,
  freeMode: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true
}

const ProjectLayout = (props) => {
  const {
    projectList,
    current,
    total,
    pageSize,
    currentActiveString,
    onChangePage,
    onChangeSort,
    loading,
    error,
    refetch
  } = props
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  const [sortBy, setSortBy] = useState()
  const optionList = [
    { title: t('sortBy.AZ'), value: 'title-asc' },
    { title: t('sortBy.ZA'), value: 'title-desc' },
    { title: t('sortBy.newest'), value: 'createdAt-desc' },
    { title: t('sortBy.oldest'), value: 'createdAt-asc' }
  ]

  const handleChangeSort = (value) => {
    setSortBy(value)
    onChangeSort(value)
  }

  const ProjectContent = (props) => {
    const { data, dataIndex } = props
    return (
      <Row className='WrapperContentClass'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Title
            level={2}
            style={TitleStyle}
            className={data?.linkUrl ? 'ProjectWithLink' : ''}
            onClick={() => {
              goToLink(data?.linkUrl)
            }}
          >
            {textTranslate(
              language,
              data?.eng?.title ? data?.eng?.title : '',
              data?.ina?.title ? data?.ina?.title : ''
            )}
          </Title>
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ textAlign: 'left' }}
        >
          <Text>
            <div
              dangerouslySetInnerHTML={{
                __html: textTranslate(
                  language,
                  data?.eng?.description ? data?.eng?.description : '',
                  data?.ina?.description ? data?.ina?.description : ''
                )
              }}
            ></div>
          </Text>
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ marginTop: '2em' }}
        >
          <Row>
            <Col span={24}>
              <PilarThumb
                gallery={data?.imageUrls}
                galleryApi={galleryApi}
                thumbApi={thumbApi}
              />
            </Col>
          </Row>
        </Col>
        {dataIndex < projectList.length - 1 ? (
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            className='WrapperDividerClass'
          />
        ) : null}
      </Row>
    )
  }

  const RenderProjectLayout = () => {
    if (error) {
      return <RenderErrorProjectLayout />
    } else if (!loading && !error) {
      if (projectList.length !== 0) {
        return (
          <>
            <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
              <Row gutter={16} className='ProjectFilterSortWrap'>
                <Col span='auto'>
                  <RenderSort />
                </Col>
              </Row>
            </Col>
            {projectList.map((data, index) => {
              return (
                <div key={index}>
                  <ProjectContent data={data} dataIndex={index} />
                </div>
              )
            })}
          </>
        )
      } else
        return (
          <RenderNoData content={currentActiveString || t('menu.project')} />
        )
    } else {
      return <RenderLoadingProjectLayout />
    }
  }

  const RenderLoadingProjectLayout = () => {
    let dummyLoading = [0, 1]
    return (
      <Row className='WrapperContentClass'>
        <Col
          xs={12}
          md={12}
          lg={{ offset: 2, span: 10 }}
          style={{ marginTop: '1em' }}
        >
          <PilarLoading type='black' height='20px' borderRadius='5px' />
        </Col>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          {dummyLoading.map((item) => {
            return (
              <div key={item} style={{ marginTop: '20px' }}>
                <PilarLoading type='black' height='20px' borderRadius='5px' />
              </div>
            )
          })}
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ marginTop: '20px' }}
        >
          <PilarLoading type='black' height='200px' borderRadius='5px' />
        </Col>
      </Row>
    )
  }

  const RenderErrorProjectLayout = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingProjectLayout />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loading}
            onClick={() => refetch()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderSort = () => {
    return (
      <Select
        placeholder={t('sortBy.placeholder')}
        style={{ width: checkDevice() === 'Mobile' ? 100 : 150 }}
        value={sortBy}
        onChange={handleChangeSort}
        allowClear
      >
        {optionList.map((data, index) => {
          return (
            <Option key={index} value={data.value}>
              {data.title}
            </Option>
          )
        })}
      </Select>
    )
  }

  const itemRender = (current, type, originalElement) => {
    if (type === 'prev') {
      return <span style={PaginationButtonStyle}>{t('pagination.prev')}</span>
    }
    if (type === 'next') {
      return <span style={PaginationButtonStyle}>{t('pagination.next')}</span>
    }
    return originalElement
  }

  const RenderPagination = () => {
    if (total <= pageSize) return null
    return (
      <Row className='WrapperContentClass'>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ textAlign: 'center' }}
        >
          <Pagination
            className='PilarPagination'
            responsive
            hideOnSinglePage
            current={current}
            total={total}
            pageSize={pageSize}
            itemRender={itemRender}
            onChange={(page) => onChangePage(page)}
          />
        </Col>
      </Row>
    )
  }

  const RenderNoData = ({ content }) => {
    let message = `${content} ${t('text.notFound')}`
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={message} />
  }

  return (
    <div>
      <RenderProjectLayout />
      <RenderPagination />
    </div>
  )
}

export { ProjectLayout }

ProjectLayout.propTypes = {
  projectList: PropTypes.array,
  current: PropTypes.number,
  total: PropTypes.number,
  pageSize: PropTypes.number,
  currentActiveString: PropTypes.string,
  loading: PropTypes.bool,
  error: PropTypes.bool,
  onChangePage: PropTypes.func,
  onChangeSort: PropTypes.func,
  refetch: PropTypes.func
}

ProjectLayout.defaultProps = {
  current: 1,
  total: 0,
  pageSize: 1,
  loading: false,
  error: false
}

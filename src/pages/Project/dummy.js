import { AboutBackground, BusinessBackground } from 'assets'

const PresentProject = [
  {
    projectId: 1,
    projectName: 'Proyek Apartment Duren Sawit',
    projectDate: 'July 2013',
    projectDesc:
      'Pembangunan Apartment Duret Sawit, terletak dipusat kota duren sawit. Dengan desain elegan western modern style. On progress, melibatkan 1400 tenaga kerja yang memiliki pengalaman yang sangat bagus dalam menangani proyek hotel .Konsep yang menarik perhatian calon pengunjung dikarenakan memiliki banyak fasilitas yang mempuni',
    projectPhoto: [AboutBackground]
  },
  {
    projectId: 2,
    projectName: 'Proyek Pembangunan Jembatan',
    projectDate: 'May 2013',
    projectDesc:
      'Proyek Pembangunan Jembatan didaerah Ternate. Membantu menghubungkan beberapa kota sehingga dapat mempermudah akses kerja warga sekitar. Terletak dikawasan yang dihuni oleh banyak orang adalah tantang utama kami dalam menjaga lingkungan serta memerhatikan warga sekitar',
    projectPhoto: [BusinessBackground]
  },
  {
    projectId: 3,
    projectName: 'Proyek Pembangunan Taman Kota Bogor',
    projectDate: 'January 2013',
    projectDesc:
      'Pembangunan Taman Kota Bogor sangatlah memiliki tantangan yang besar dikarenakan harus membuat taman yang berada di tengah kota Bogor dan harus menjaga lingkungan sekitar. Konsep yang ditawarkan adalah konsep modern style futuristic.',
    projectPhoto: [AboutBackground]
  }
]

const FinishedProject = [
  {
    projectId: 1,
    projectName: 'Proyek Jakarta Islamic School',
    projectDate: 'July 2013',
    projectDesc:
      'Alhamdulillah, sesuai dengan rencana yang sudah ditetapkan. Bangunan Wisma Khalifah pada komplek Jakarta Islamic School cabang Kodam sudah selesai pembangunannya dan sudah digunakan untuk kegiatan pendidikan & pengajaran pada bulan Juli ini. Semoga ikut memberi pencerahan bagi semuanya. Sedangkan untuk penambahan lokal/lantai di JISc Joglo dapat  diselesaikan lebih cepat dari jadwal yang telah ditetapkan dan bersamaan dengan tahun ajaran baru ini dapat dipakai untuk proses belajar mengajar.',
    projectPhoto: [
      AboutBackground,
      BusinessBackground,
      AboutBackground,
      BusinessBackground,
      AboutBackground
    ]
  },
  {
    projectId: 2,
    projectName: 'Projek Investasi Pertama',
    projectDate: 'May 2013',
    projectDesc:
      'Untuk pertama kalinya, PT. Cipta Pilar Persada melaksanakan proyek investasi. Proyek yang berlokasi di Talang Duku Jambi ini berupa pembangunan dermaga berikut fasilitas loading batubara berupa “pengadaan barge loader”. Insya Allah, produksi / muatan pertama akan dimulai pada awal bulan Oktober ini dan kerjasama ini akan berlangsung selama 5 tahun.',
    projectPhoto: [
      AboutBackground,
      BusinessBackground,
      AboutBackground,
      BusinessBackground,
      AboutBackground
    ]
  },
  {
    projectId: 3,
    projectName: 'Projek SPBU Palembang',
    projectDate: 'January 2013',
    projectDesc:
      'Pembangunan SPBU menjadi pencapain yang pertama kali bagi PT. Cipta Pilar Persada dalam mengembangkan area bisnisnya di Palembang. Renovasi total SPBU di Jl. Merdeka, Palembang akan dilaksanakan dalam jangka waktu 2,5 bulan. dengan konsep desain yang berbeda diharapkan akan memberikan kesan yang membekas ketika berkunjung ke SPBU tersebut.',
    projectPhoto: [
      AboutBackground,
      BusinessBackground,
      AboutBackground,
      BusinessBackground,
      AboutBackground
    ]
  }
]

export { PresentProject, FinishedProject }

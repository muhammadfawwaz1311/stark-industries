// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Input, Button } from 'antd'
import { PilarLoading } from 'components'
import { goTo, textTranslate } from 'utils'
import { ProjectBackground } from 'assets'
import { SearchOutlined } from '@ant-design/icons'
import { HeaderService } from 'services'
import { ProjectFeatureContent } from './ProjectFeatureContent'
import { ProjectContent } from './ProjectContent'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { Title } = Typography
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}

const Project = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  // Header Content
  const page = 'project'
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // const [loadingPage, setLoadingPage] = useState(true)

  const [showAll, setShowAll] = useState(false)

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()

      // setTimeout(() => {
      //   setLoadingPage(false)
      // }, 1000)
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      const getHeaderContent = await HeaderService.getHeaderContent(page)
      if (getHeaderContent && getHeaderContent.status === 200) {
        let data = getHeaderContent?.data?.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const changeShowAll = (value) => {
    setShowAll(value)
  }

  const handleSearch = (value) => {
    const params = {
      location: `/search`,
      state: {
        type: 'project',
        searchValue: value
      }
    }
    goTo(params)
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl
                ? headerContent.imageUrl
                : ProjectBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
              <div>
                <Input
                  allowClear
                  className='PilarSearchAbsolute'
                  placeholder={t('project.searchPlaceholder')}
                  prefix={<SearchOutlined />}
                  onPressEnter={(event) => handleSearch(event.target.value)}
                />
              </div>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  // if (loadingPage) {
  //   return <PilarSplashScreen />
  // }
  return (
    <div>
      {/* <PilarSEO type={page} /> */}
      <RenderHeaderContent />
      <ProjectFeatureContent showAll={showAll} changeShowAll={changeShowAll} />
      {showAll ? null : <ProjectContent />}
    </div>
  )
}

export default Project

// dependencies
import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Typography, Tabs, Button, Empty } from 'antd'
import { PilarMultiContentSlider, PilarLoading } from 'components'
import { textTranslate, checkDevice } from 'utils'
import { ProjectLayout } from './ProjectLayout'
import { BusinessService, ProjectService } from 'services'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { Text } = Typography
const { TabPane } = Tabs
const showAllStyle = {
  color: '#e7b12e',
  fontWeight: 'bold',
  cursor: 'pointer'
}
const closeStyle = {
  color: '#FF1B1B',
  fontWeight: 'bold',
  cursor: 'pointer',
  textDecoration: 'underline'
}

const ProjectFeatureContent = (props) => {
  const { showAll, changeShowAll } = props
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  // Tabs List
  const [currentActiveTab, setCurrentActiveTab] = useState('')
  const [currentActiveString, setCurrentActiveString] = useState('')
  const [tabList, setTabList] = useState([])
  const [loadingTabList, setLoadingTabList] = useState(false)
  const [errorTabList, setErrorTabList] = useState(false)
  // Tabs List

  // Featured Project List
  const featuredPage = 1
  const featuredPer = 4
  const [featuredList, setFeaturedList] = useState([])
  const [loadingFeaturedList, setLoadingFeaturedList] = useState(false)
  const [errorFeaturedList, setErrorFeaturedList] = useState(false)
  // Featured Project List

  // All Featured Project List
  const per = 3
  const defaultOrderBy = 'createdAt'
  const defaultDir = 'desc'
  const [allFeaturedList, setAllFeaturedList] = useState([])
  const [loadingAllFeaturedList, setLoadingAllFeaturedList] = useState(false)
  const [errorAllFeaturedList, setErrorAllFeaturedList] = useState(false)
  const [totalAllFeaturedList, setTotalAllFeaturedList] = useState(0)
  const [pageAllFeaturedList, setPageAllFeaturedList] = useState(1)
  const [orderBy, setOrderBy] = useState(defaultOrderBy)
  const [dir, setDir] = useState(defaultDir)
  // All Featured Project List

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getTabList(language)
    }

    return () => {
      subscribe = false
    }
  }, [language])
  // Initial

  useEffect(() => {
    let subscribe = true

    if (subscribe && currentActiveTab) {
      if (showAll) {
        getAllFeaturedList(
          pageAllFeaturedList,
          per,
          currentActiveTab,
          orderBy,
          dir
        )
      } else {
        getFeaturedList(currentActiveTab)
      }
    }

    return () => {
      subscribe = false
    }
  }, [currentActiveTab, pageAllFeaturedList, orderBy, dir, showAll])

  const getTabList = async (language) => {
    setLoadingTabList(true)
    try {
      const getTabList = await BusinessService.getBusinessNameList()
      if (getTabList && getTabList.status === 200) {
        let data = getTabList?.data?.data?.businesses
        setTabList(data)
        setCurrentActiveTab(data[0]?.id ? data[0]?.id : '')
        setCurrentActiveString(
          textTranslate(language, data[0]?.eng?.name, data[0]?.ina.name)
        )
        setErrorTabList(false)
      } else {
        setErrorTabList(true)
      }
    } catch (error) {
      setErrorTabList(true)
    }
    setLoadingTabList(false)
  }

  const getFeaturedList = async (business) => {
    setLoadingFeaturedList(true)
    try {
      const getFeaturedList = await ProjectService.getFeaturedProjectList(
        featuredPage,
        featuredPer,
        business
      )
      if (getFeaturedList && getFeaturedList.status === 200) {
        let data = getFeaturedList?.data?.data?.projects
        setFeaturedList(data)
        setErrorFeaturedList(false)
      } else {
        setErrorFeaturedList(true)
      }
    } catch (error) {
      setErrorFeaturedList(true)
    }
    setLoadingFeaturedList(false)
  }

  const getAllFeaturedList = async (
    pageAllFeaturedList,
    per,
    business,
    orderBy,
    dir
  ) => {
    setLoadingAllFeaturedList(true)
    try {
      const getAllFeaturedList = await ProjectService.getAllFeaturedProjectList(
        pageAllFeaturedList,
        per,
        business,
        orderBy,
        dir
      )
      if (getAllFeaturedList && getAllFeaturedList.status === 200) {
        let data = getAllFeaturedList?.data?.data?.projects
        let total = getAllFeaturedList?.data?.pagination?.totalCount
        setAllFeaturedList(data)
        setTotalAllFeaturedList(total)
        setErrorAllFeaturedList(false)
      } else {
        setErrorAllFeaturedList(true)
      }
    } catch (error) {
      setErrorAllFeaturedList(true)
    }
    setLoadingAllFeaturedList(false)
  }

  const handleChangeSort = (value) => {
    if (!value) {
      setOrderBy(defaultOrderBy)
      setDir(defaultDir)
    } else {
      let sortArr = []
      sortArr = value.split('-')
      if (sortArr[0] === 'title') {
        setOrderBy(language === 'ENG' ? 'titleEng' : 'titleIna')
        setDir(sortArr[1])
      } else {
        setOrderBy(sortArr[0])
        setDir(sortArr[1])
      }
    }
  }

  const onChangePage = (page) => {
    setPageAllFeaturedList(page)
  }

  const onChangeTab = (activeKey, text) => {
    setCurrentActiveTab(activeKey)
    setCurrentActiveString(text)
  }

  const RenderTabList = () => {
    if (errorTabList) {
      return <RenderErrorTabList />
    } else if (!loadingTabList && !errorTabList) {
      return (
        <Row className='WrapperContentClass'>
          <Col span={24}>
            <Tabs
              className='PilarTabs'
              activeKey={currentActiveTab}
              centered
              onTabClick={(activeKey, event) =>
                onChangeTab(activeKey, event.target.innerText)
              }
            >
              {tabList?.map((tab) => {
                return (
                  <TabPane
                    tab={textTranslate(
                      language,
                      tab?.eng?.name,
                      tab?.ina?.name
                    )}
                    key={tab?.id}
                  >
                    {showAll ? (
                      <RenderAllFeaturedList />
                    ) : (
                      <RenderFeaturedList />
                    )}
                  </TabPane>
                )
              })}
            </Tabs>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingTabList />
    }
  }

  const RenderLoadingTabList = () => {
    let dummyLoading = [0, 1, 2]
    return (
      <Row gutter={[16]} className='WrapperContentClass'>
        {dummyLoading.map((data) => {
          return (
            <Col key={data} span={8}>
              <PilarLoading type='black' height='30px' borderRadius='5px' />
            </Col>
          )
        })}
      </Row>
    )
  }

  const RenderErrorTabList = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingTabList />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingTabList}
            onClick={() => getTabList()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderFeaturedList = () => {
    if (errorFeaturedList) {
      return <RenderErrorFeaturedList />
    } else if (!loadingFeaturedList && !errorFeaturedList) {
      if (featuredList.length !== 0) {
        return (
          <Row gutter={[16, 32]}>
            <Col span={24}>
              <PilarMultiContentSlider gallery={featuredList} />
            </Col>
            <Col offset={6} span={12} style={{ textAlign: 'center' }}>
              <Text style={showAllStyle} onClick={() => changeShowAll(true)}>
                {t('button.seeAll')}
              </Text>
            </Col>
          </Row>
        )
      } else return <RenderNoData content={currentActiveString} />
    } else {
      return <RenderLoadingFeaturedList />
    }
  }

  const RenderLoadingFeaturedList = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2]
    return (
      <Row gutter={[16]}>
        {dummyLoading.map((data) => {
          return (
            <Col key={data} flex={1}>
              <PilarLoading type='black' height='200px' borderRadius='5px' />
            </Col>
          )
        })}
      </Row>
    )
  }

  const RenderErrorFeaturedList = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingFeaturedList />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingFeaturedList}
            onClick={() => getFeaturedList(currentActiveTab)}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderAllFeaturedList = () => {
    return (
      <>
        <ProjectLayout
          projectList={allFeaturedList}
          current={pageAllFeaturedList}
          total={totalAllFeaturedList}
          pageSize={per}
          loading={loadingAllFeaturedList}
          error={errorAllFeaturedList}
          onChangePage={onChangePage}
          onChangeSort={handleChangeSort}
          currentActiveString={currentActiveString}
          refetch={() =>
            getAllFeaturedList(
              pageAllFeaturedList,
              per,
              currentActiveTab,
              orderBy,
              dir
            )
          }
        />
      </>
    )
  }

  const RenderNoData = ({ content }) => {
    let message = `${content} ${t('text.notFound')}`
    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={message} />
  }

  return (
    <div>
      <RenderTabList />
      {showAll && !loadingAllFeaturedList && !errorAllFeaturedList ? (
        <Row className='WrapperContentClass'>
          <Col offset={6} span={12} style={{ textAlign: 'center' }}>
            <Text style={closeStyle} onClick={() => changeShowAll(false)}>
              {t('button.closePage')}
            </Text>
          </Col>
        </Row>
      ) : null}
    </div>
  )
}

export { ProjectFeatureContent }

ProjectFeatureContent.propTypes = {
  showAll: PropTypes.bool,
  changeShowAll: PropTypes.func
}

ProjectFeatureContent.defaultProps = {
  showAll: false
}

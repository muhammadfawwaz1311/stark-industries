import { AboutBackground, BusinessBackground } from 'assets'

const BusinessDummy = [
  {
    businessId: 1,
    businessName: 'Pilar Development',
    businessDesc:
      'Perusahaan pengembang properti yang mengembangkan lahan sehingga menjadi tempat tinggal dan tempat beraktivitas manusia yang layak dan nyaman. Setiap Proyeknya mempunyai konsep yang kaut dan competitive advantage yang kuat terhadap pesaing sehingga dapat menjadi yang terbaik di kategorinya. Memaksimalkan perwujudan value untuk stakeholder. Menciptakan harmoni disetiap proyeknya baik harmoni di dalam hunian sehingga menciptakan peace of mind bagi customer, ataupun harmoni dengan masyarakat sekitar. Provera development sebagai salah satu perusahaan afiliasi dari Pilar Corp sedang membangun Office Tower di Kuningan, Jakarta diatas tanah wakaf yayasan.',
    businessCharacter: [
      {
        characterName: 'Sumber Daya Manusia',
        characterValue: '200+'
      },
      {
        characterName: 'Pembangunan Gedung',
        characterValue: '15'
      },
      {
        characterName: 'Pembangunan Apartment',
        characterValue: '20'
      }
    ],
    businessGallery: [
      AboutBackground,
      BusinessBackground,
      AboutBackground,
      BusinessBackground,
      AboutBackground
    ]
  },
  {
    businessId: 2,
    businessName: 'Pilar Construction',
    businessDesc:
      'Pilar Construction adalah anak perusahaan dari Pilar Corporation yang bergerak dibidang pengembangan konstruksi, Seperti apartemen, gedung dan aula. Fokus untuk memberikan dampak yang bagus untuk masyarakat dan linkungan serta memberikan lapangan kerja yang lebih banyak kepada warga sekitar. Melakukan pekerjaan dengan tepat, teliti dan memiliki hasil yang selalu memuaskan.',
    businessCharacter: [
      {
        characterName: 'Pembangunan Jembatan',
        characterValue: '10'
      },
      {
        characterName: 'Sumber Daya Manusia',
        characterValue: '250'
      },
      {
        characterName: 'Investor',
        characterValue: '5'
      }
    ],
    businessGallery: [
      AboutBackground,
      BusinessBackground,
      AboutBackground,
      BusinessBackground,
      AboutBackground
    ]
  },
  {
    businessId: 3,
    businessName: 'Pilar Property',
    businessDesc:
      'Pilar Property adalah anak perusahaan dari Pilar Corporation yang bergerak dibidang pengembangan konstruksi, Seperti apartemen, gedung dan aula. Fokus untuk memberikan dampak yang bagus untuk masyarakat dan linkungan serta memberikan lapangan kerja yang lebih banyak kepada warga sekitar. Melakukan pekerjaan dengan tepat, teliti dan memiliki hasil yang selalu memuaskan.',
    businessCharacter: [
      {
        characterName: 'Sumber Daya Manusia',
        characterValue: '120'
      },
      {
        characterName: 'Client',
        characterValue: '50'
      },
      {
        characterName: 'Pembangunan Gedung',
        characterValue: '10'
      }
    ],
    businessGallery: [
      AboutBackground,
      BusinessBackground,
      AboutBackground,
      BusinessBackground,
      AboutBackground
    ]
  }
]

export { BusinessDummy }

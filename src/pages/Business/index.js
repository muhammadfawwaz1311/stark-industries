// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Card, Button } from 'antd'
import { BusinessBackground } from 'assets'
import { PilarThumb, PilarLoading } from 'components'
import { textTranslate } from 'utils'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'
import { dummyHeaderBusiness, dummyBusinessContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const CSRCardWrapper = {
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  height: '100%',
  padding: '1em'
}

const galleryApi = {
  spaceBetween: 30,
  lazy: true,
  centeredSlides: true,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false
  }
}
const thumbApi = {
  spaceBetween: 5,
  slidesPerView: 4,
  touchRatio: 0.2,
  slideToClickedSlide: true,
  freeMode: true,
  watchSlidesVisibility: true,
  watchSlidesProgress: true
}

const Business = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  // Header Content
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // Business Content
  const [businessList, setBusinessList] = useState([])
  const [loadingBusinessList, setLoadingBusinessList] = useState(false)
  const [errorBusinessList, setErrorBusinessList] = useState(false)
  // Business Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
      getBusinessList()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      if (dummyHeaderBusiness.data) {
        let data = dummyHeaderBusiness.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getBusinessList = async () => {
    setLoadingBusinessList(true)
    try {
      if (dummyBusinessContent.data) {
        let data = dummyBusinessContent.data.businesses
        setBusinessList(data)
        setErrorBusinessList(false)
      } else {
        setErrorBusinessList(true)
      }
    } catch (error) {
      setErrorBusinessList(true)
    }
    setLoadingBusinessList(false)
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl
                ? headerContent.imageUrl
                : BusinessBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderBusinessContent = (props) => {
    const { data, dataIndex } = props
    let summaries =
      language === 'ENG' ? data?.eng?.summaries : data?.ina?.summaries
    return (
      <Row className='WrapperContentClass'>
        {data?.logo ? (
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            style={{ marginBottom: '1rem' }}
          >
            <div className='businessLogo-card-image-wrapper'>
              <img src={data.logo} alt='' />
            </div>
          </Col>
        ) : null}
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Title level={2} style={TitleStyle}>
            {textTranslate(
              language,
              data?.eng?.name ? data?.eng?.name : '',
              data?.ina?.name ? data?.ina?.name : ''
            )}
          </Title>
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ textAlign: 'left' }}
        >
          <Text>
            <div
              dangerouslySetInnerHTML={{
                __html: textTranslate(
                  language,
                  data?.eng?.description ? data?.eng?.description : '',
                  data?.ina?.description ? data?.ina?.description : ''
                )
              }}
            ></div>
          </Text>
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ marginTop: '2rem' }}
        >
          <Row style={{ padding: '1rem', justifyContent: 'center' }}>
            {summaries?.map((char, index) => {
              return (
                <Col xs={24} md={6} key={index} style={{ padding: '0.5rem' }}>
                  <div className='pilar-card-wrapper'>
                    <Card
                      hoverable
                      style={{ height: '100%' }}
                      bodyStyle={CSRCardWrapper}
                    >
                      <div style={{ textAlign: 'center', flex: 1 }}>
                        <Title
                          level={5}
                          style={{ ...TitleStyle, color: '#E7B12E' }}
                        >
                          {char.value}
                        </Title>
                      </div>
                      <div style={{ textAlign: 'center', flex: 1 }}>
                        <Text style={{ color: '#787878' }}>{char.label}</Text>
                      </div>
                    </Card>
                  </div>
                </Col>
              )
            })}
          </Row>
        </Col>
        <Col
          xs={24}
          md={24}
          lg={{ offset: 2, span: 20 }}
          style={{ marginTop: '2em' }}
        >
          <Row>
            {data.projectImages && data.projectImages.length > 0 ? (
              <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
                <Title
                  level={2}
                  style={{
                    ...TitleStyle,
                    textAlign: 'center',
                    marginBottom: '1em'
                  }}
                >
                  {t('business.projectTitle')}
                </Title>
              </Col>
            ) : null}
            <Col span={24}>
              <PilarThumb
                gallery={data.projectImages}
                galleryApi={galleryApi}
                thumbApi={thumbApi}
              />
            </Col>
          </Row>
        </Col>
        {dataIndex < businessList.length - 1 ? (
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            className='WrapperDividerClass'
          />
        ) : null}
      </Row>
    )
  }

  const RenderBusinessList = () => {
    if (errorBusinessList) {
      return <RenderErrorBusinessList />
    } else if (!loadingBusinessList && !errorBusinessList) {
      return businessList.map((data, index) => {
        return (
          <div key={data.id}>
            <RenderBusinessContent data={data} dataIndex={index} />
          </div>
        )
      })
    } else {
      return <RenderLoadingBusinessList />
    }
  }

  const RenderLoadingBusinessList = () => {
    let dummyLoading = [0, 1, 2]
    return (
      <Row className='WrapperContentClass' gutter={[16, 16]}>
        <Col xs={24} md={12} lg={{ offset: 2, span: 10 }}>
          <PilarLoading type='black' height='30px' borderRadius='5px' />
        </Col>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          {dummyLoading.map((data) => {
            return (
              <div key={data} style={{ marginBottom: '10px' }}>
                <PilarLoading type='black' height='20px' borderRadius='5px' />
              </div>
            )
          })}
        </Col>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row gutter={[16]}>
            {dummyLoading.map((data) => {
              return (
                <Col span={8} key={data}>
                  <div key={data} style={{ marginBottom: '10px' }}>
                    <PilarLoading
                      type='black'
                      height='100px'
                      borderRadius='5px'
                    />
                  </div>
                </Col>
              )
            })}
          </Row>
        </Col>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <PilarLoading type='black' height='200px' borderRadius='10px' />
        </Col>
      </Row>
    )
  }

  const RenderErrorBusinessList = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col span={24} className='PilarErrorLoadingWrapper'>
          <RenderLoadingBusinessList />
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingBusinessList}
              onClick={() => getBusinessList()}
            >
              {t('button.reload')}
            </Button>
          </div>
        </Col>
      </Row>
    )
  }

  return (
    <div>
      <RenderHeaderContent />
      <RenderBusinessList />
    </div>
  )
}

export default Business

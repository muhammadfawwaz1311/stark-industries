// dependencies
import React, { useState, useEffect } from 'react'
import {
  Row,
  Col,
  Typography,
  Form,
  Input,
  Card,
  Button,
  notification
} from 'antd'
import { PilarLoading } from 'components'
import { textTranslate, checkDevice } from 'utils'
import { ContactBackground } from 'assets'
import { PhoneFilled, MailFilled } from '@ant-design/icons'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'
import { dummyHeaderContact, dummyContactContent } from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'left',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const VacancyLabelStyle = { color: '#AFAFAF', fontWeight: 'bold' }
const PilarButtonStyle = {
  marginTop: '1em',
  padding: '0.5em 1em',
  height: 'auto',
  width: 'auto',
  backgroundColor: '#E7B12E',
  color: '#fff',
  border: 0
}
const FormInputStyle = { borderRadius: '0.5em' }
const NewsDateStyle = { color: '#A6A6A6' }

const MapsOption = {
  width: '100%',
  height: '480',
  frameBorder: '0',
  style: { border: 0 },
  allowFullScreen: true,
  tabIndex: '0'
}

const Contact = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()
  // Header Content
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // Contact Content
  const [contactContent, setContactContent] = useState({})
  const [loadingContactContent, setLoadingContactContent] = useState(false)
  const [errorContactContent, setErrorContactContent] = useState(false)
  const [mapsURL, setMapsURL] = useState('')
  // Contact Content

  // Form
  const formType = 'contact'
  const [loadingForm, setLoadingForm] = useState(false)
  // Form

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
      getContactContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      if (dummyHeaderContact.data) {
        let data = dummyHeaderContact.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getContactContent = async () => {
    setLoadingContactContent(true)
    try {
      if (dummyContactContent.data) {
        let data = dummyContactContent.data
        setContactContent(data)
        setMapsURL(data?.eng?.maps?.value || data?.ina?.maps?.value)
        setErrorContactContent(false)
      } else {
        setErrorContactContent(true)
      }
    } catch (error) {
      setErrorContactContent(true)
    }
    setLoadingContactContent(false)
  }

  const handleForm = async (data) => {
    setLoadingForm(true)
    data.type = formType
    try {
      const sendMessage = true
      if (sendMessage) {
        showAlert('success', t('alert.success'))
      } else {
        showAlert('error', t('alert.error'))
      }
    } catch (error) {
      showAlert('error', t('alert.error'))
    }
    setLoadingForm(false)
  }

  const showAlert = (type, message) => {
    notification[type]({
      message: message,
      duration: 5
    })
  }

  const joinAddress = (joinAddress) => {
    joinAddress = joinAddress.filter(function (address) {
      return address
    })
    return joinAddress.join(', ')
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl
                ? headerContent.imageUrl
                : ContactBackground
            })`
          }}
        >
          <Col xs={24} md={24} lg={12}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Col xs={24} md={24} lg={{ span: 20, offset: 4 }}>
                <Title
                  level={2}
                  style={HeaderCarouselTitleStyle}
                  className='trimString'
                >
                  <div
                    dangerouslySetInnerHTML={{
                      __html: textTranslate(
                        language,
                        headerContent?.eng?.contents[0].description
                          ? headerContent?.eng?.contents[0].description
                          : '',
                        headerContent?.ina?.contents[0].description
                          ? headerContent?.ina?.contents[0].description
                          : ''
                      )
                    }}
                  ></div>
                </Title>
              </Col>
            </Row>
          </Col>
          <Col xs={0} md={0} lg={12}>
            <RenderFormContent formName='largeForm' />
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderFormContent = ({ formName }) => {
    const [form] = Form.useForm()
    return (
      <Row className='ContactFormWrapClass'>
        <Col
          xs={24}
          md={{ offset: 2, span: 20 }}
          lg={{ offset: 2, span: 20 }}
          className='ContactFormAbsoluteClass'
        >
          <Card className='ContactFormAbsoluteCardClass '>
            <Form
              form={form}
              layout='vertical'
              name={formName}
              requiredMark={false}
              onFinish={(values) => handleForm(values)}
            >
              <Form.Item
                name='name'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.name.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.name.message')
                  }
                ]}
              >
                <Input style={FormInputStyle} />
              </Form.Item>
              <Form.Item
                name='email'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.email.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.email.message')
                  }
                ]}
              >
                <Input style={FormInputStyle} />
              </Form.Item>
              <Form.Item
                name='phoneNumber'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.phoneNumber.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.phoneNumber.message')
                  }
                ]}
              >
                <Input style={FormInputStyle} />
              </Form.Item>
              <Form.Item
                name='message'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.message.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.message.message')
                  }
                ]}
              >
                <Input.TextArea
                  style={{ ...FormInputStyle, minHeight: '100px' }}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  htmlType='submit'
                  shape='round'
                  size={'small'}
                  style={{ ...PilarButtonStyle, width: '100%' }}
                  loading={loadingForm}
                >
                  <Text style={{ color: '#fff' }}>{t('button.submit')}</Text>
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Col>
      </Row>
    )
  }

  const RenderFormMobile = () => {
    return (
      <Col xs={24} md={24} lg={0}>
        <Row className='WrapperContentClass'>
          <Col span={24}>
            <RenderFormContent formName='smallForm' />
          </Col>
        </Row>
      </Col>
    )
  }

  const RenderContactContent = () => {
    let address = {
      city: textTranslate(
        language,
        contactContent?.eng?.address?.city,
        contactContent?.ina?.address?.city
      ),
      detailAddress: textTranslate(
        language,
        contactContent?.eng?.address?.detailAddress,
        contactContent?.ina?.address?.detailAddress
      ),
      district: textTranslate(
        language,
        contactContent?.eng?.address?.district,
        contactContent?.ina?.address?.district
      ),
      province: textTranslate(
        language,
        contactContent?.eng?.address?.province,
        contactContent?.ina?.address?.province
      ),
      street: textTranslate(
        language,
        contactContent?.eng?.address?.street,
        contactContent?.ina?.address?.street
      ),
      subDistrict: textTranslate(
        language,
        contactContent?.eng?.address?.subDistrict,
        contactContent?.ina?.address?.subDistrict
      ),
      zipcode: textTranslate(
        language,
        contactContent?.eng?.address?.zipcode,
        contactContent?.ina?.address?.zipcode
      )
    }
    if (errorContactContent) {
      return <RenderErrorContactContent />
    } else if (!loadingContactContent && !errorContactContent) {
      return (
        <Row
          className='WrapperContentClass'
          style={{ minHeight: checkDevice() === 'Desktop' ? '65vh' : 0 }}
        >
          <Col xs={24} md={24} lg={{ offset: 2, span: 10 }}>
            <Row style={{ marginBottom: '2em' }}>
              <Col span={24}>
                <Title level={4} style={{ ...TitleStyle, color: '#E7B12E' }}>
                  {textTranslate(
                    language,
                    contactContent?.eng?.phone?.label,
                    contactContent?.ina?.phone?.label
                  )}
                </Title>
              </Col>
              <Col span={24}>
                <Text style={NewsDateStyle}>
                  <PhoneFilled />
                  <span style={{ marginLeft: '0.5em' }}>
                    {textTranslate(
                      language,
                      contactContent?.eng?.phone?.value,
                      contactContent?.ina?.phone?.value
                    )}
                  </span>
                </Text>
              </Col>
            </Row>
            <Row style={{ marginBottom: '2em' }}>
              <Col span={24}>
                <Title level={4} style={{ ...TitleStyle, color: '#E7B12E' }}>
                  {textTranslate(
                    language,
                    contactContent?.eng?.email?.label,
                    contactContent?.ina?.email?.label
                  )}
                </Title>
              </Col>
              <Col span={24}>
                <Text style={NewsDateStyle}>
                  <MailFilled />
                  <span style={{ marginLeft: '0.5em' }}>
                    {textTranslate(
                      language,
                      contactContent?.eng?.email?.value,
                      contactContent?.ina?.email?.value
                    )}
                  </span>
                </Text>
              </Col>
            </Row>
            <Row>
              <Col span={24}>
                <Title level={4} style={{ ...TitleStyle, color: '#E7B12E' }}>
                  {textTranslate(
                    language,
                    contactContent?.eng?.address?.label,
                    contactContent?.ina?.address?.label
                  )}
                </Title>
              </Col>
              {address.detailAddress ? (
                <Col span={24}>
                  <Text style={NewsDateStyle}>{address.detailAddress}</Text>
                </Col>
              ) : null}
              {address.street ? (
                <Col span={24}>
                  <Text style={NewsDateStyle}>{address.street}</Text>
                </Col>
              ) : null}
              {address.subDistrict ? (
                <Col span={24}>
                  <Text style={NewsDateStyle}>{address.subDistrict}</Text>
                </Col>
              ) : null}
              {address.district ? (
                <Col span={24}>
                  <Text style={NewsDateStyle}>{address.district}</Text>
                </Col>
              ) : null}
              {address.city || address.province || address.zipcode ? (
                <Col span={24}>
                  <Text style={NewsDateStyle}>
                    {joinAddress([
                      address.city,
                      address.province,
                      address.zipcode
                    ])}
                  </Text>
                </Col>
              ) : null}
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingContactContent />
    }
  }

  const RenderLoadingContactContent = () => {
    let dummyLoading = [1, 2, 3]
    return (
      <Row className='WrapperContentClass'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 10 }}>
          {dummyLoading.map((data) => {
            return (
              <Row style={{ marginBottom: '20px' }} key={data}>
                <Col span={12}>
                  <PilarLoading type='black' height='20px' borderRadius='5px' />
                </Col>
                <Col span={24} style={{ marginTop: '10px' }}>
                  <PilarLoading type='black' height='20px' borderRadius='5px' />
                </Col>
              </Row>
            )
          })}
        </Col>
      </Row>
    )
  }

  const RenderErrorContactContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingContactContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingContactContent}
            onClick={() => getContactContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderMapsContent = () => {
    return (
      <Row className='WrapperContentClass'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <iframe
            {...MapsOption}
            src={mapsURL}
            title='address'
            aria-hidden='false'
          ></iframe>
        </Col>
      </Row>
    )
  }

  return (
    <div>
      <RenderHeaderContent />
      <RenderFormMobile />
      <RenderContactContent />
      <RenderMapsContent />
    </div>
  )
}

export default Contact

// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button } from 'antd'
import { PilarLoading } from 'components'
import { textTranslate } from 'utils'
import { AboutBackground } from 'assets'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'
import {
  ObjectiveContent,
  PartnerContent,
  PerformanceContent,
  SummaryContent,
  PrefaceContent,
  ManagementContent,
  VideoContent
} from '../Shares'
import { dummyHeaderContent, dummyAboutUsContent } from '../../assets/dummy'

const { Title, Text } = Typography
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}

const About = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Header Content
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // Introduction Content
  const [introductionContent, setIntroductionContent] = useState({})
  const [loadingIntroductionContent, setLoadingIntroductionContent] =
    useState(false)
  const [errorIntroductionContent, setErrorIntroductionContent] =
    useState(false)
  // Introduction Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
      getIntroductionContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      if (dummyHeaderContent.data) {
        setHeaderContent(dummyHeaderContent.data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getIntroductionContent = async () => {
    setLoadingIntroductionContent(true)
    try {
      if (dummyAboutUsContent.data) {
        setIntroductionContent(dummyAboutUsContent.data)
        setErrorIntroductionContent(false)
      } else {
        setErrorIntroductionContent(true)
      }
    } catch (error) {
      setErrorIntroductionContent(true)
    }
    setLoadingIntroductionContent(false)
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : AboutBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderIntroductionContent = () => {
    if (!loadingIntroductionContent && !errorIntroductionContent) {
      if (Object.keys(introductionContent).length !== 0) {
        return (
          <Col
            xs={24}
            md={24}
            lg={{ offset: 2, span: 20 }}
            style={{ textAlign: 'justify' }}
          >
            <Text>
              <div
                dangerouslySetInnerHTML={{
                  __html: textTranslate(
                    language,
                    introductionContent?.eng?.description,
                    introductionContent?.ina?.description
                  )
                }}
              ></div>
            </Text>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorLoadingIntroductionContent />
    }
  }

  const RenderErrorLoadingIntroductionContent = () => {
    let dummyLoading = [0, 1, 2, 3]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row>
            {dummyLoading.map((data) => {
              return (
                <Col span={24} key={data} style={{ marginBottom: '10px' }}>
                  <PilarLoading type='black' height='20px' borderRadius='5px' />
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorIntroductionContent ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingIntroductionContent}
              onClick={() => getIntroductionContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperIntroductionContent = () => {
    if (
      !loadingIntroductionContent &&
      !errorIntroductionContent &&
      Object.keys(introductionContent).length === 0
    ) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <RenderIntroductionContent />
        </Row>
      )
    }
  }

  return (
    <div>
      <RenderHeaderContent />
      <RenderWrapperIntroductionContent />
      <PerformanceContent />
      <ObjectiveContent />
      <PrefaceContent />
      <SummaryContent />
      <PartnerContent />
      <VideoContent />
      <ManagementContent type={'key'} />
      <ManagementContent type={'team'} />
    </div>
  )
}

export default About

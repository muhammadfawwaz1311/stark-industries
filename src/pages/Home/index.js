// dependencies
import React, { useState, useEffect } from 'react'
import { Row, Col, Typography, Button, Carousel } from 'antd'
import { PilarLoading } from 'components'
import { HomeBackground } from 'assets'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import {
  IntroductionContent,
  ObjectiveContent,
  BusinessContent,
  AboutUsContent,
  PartnerContent,
  CareerContactContent,
  GalleryContent
} from '../Shares'
import { dummyHeaderHome } from '../../assets/dummy'

const { Title, Text } = Typography
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em'
}
const HeaderCarouselContentStyle = {
  color: '#fff',
  textAlign: 'center',
  paddingTop: '1em'
}

const Home = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Header Content
  const [headerContent, setHeaderContent] = useState({})
  const [currentHeaderContent, setCurrentHeaderContent] = useState([])
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      if (dummyHeaderHome.data) {
        let data = dummyHeaderHome.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  useEffect(() => {
    let subscribe = true
    let currentData = []

    if (subscribe) {
      if (language === 'ENG') {
        currentData = headerContent?.eng?.contents
          ? headerContent?.eng?.contents
          : []
      } else {
        currentData = headerContent?.ina?.contents
          ? headerContent?.ina?.contents
          : []
      }
      setCurrentHeaderContent(currentData)
    }

    return () => {
      subscribe = false
    }
  }, [headerContent, language])

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            <Text>{t('button.reload')}</Text>
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : HomeBackground
            })`
          }}
        >
          <Col span={24}>
            <Carousel
              className='PilarCarouselClass HeaderCarouselClass Home'
              autoplay
              autoplaySpeed={5000}
            >
              {currentHeaderContent.map((data, index) => {
                return (
                  <Row key={index}>
                    <Title
                      level={2}
                      className='CarouselTitleClass trimString'
                      style={HeaderCarouselTitleStyle}
                    >
                      <div
                        dangerouslySetInnerHTML={{
                          __html: data.description ? data.description : ''
                        }}
                      ></div>
                    </Title>
                    <Title level={5} style={HeaderCarouselContentStyle}>
                      {data.title ? data.title : ''}
                    </Title>
                  </Row>
                )
              })}
            </Carousel>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  return (
    <div>
      <RenderHeaderContent />
      <IntroductionContent />
      <ObjectiveContent />
      <BusinessContent />
      <PartnerContent />
      <AboutUsContent />
      <GalleryContent />
      <CareerContactContent />
    </div>
  )
}

export default Home

// dependencies
import React, { useState, useEffect } from 'react'
import {
  Row,
  Col,
  Typography,
  Card,
  Comment,
  Avatar,
  Form,
  Input,
  Button,
  Pagination,
  notification
} from 'antd'
import { PilarLoading } from 'components'
import { textTranslate, checkDevice } from 'utils'
import { CareerBackground, QuoteIcon } from 'assets'
import { ArrowRightOutlined } from '@ant-design/icons'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'
import {
  dummyHeaderCareer,
  dummyAboutUsContent,
  dummyTestmoniesContent,
  dummyRecruitmentContent,
  dummyContactContent
} from '../../assets/dummy'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const HeaderCarouselTitleStyle = {
  color: '#fff',
  textAlign: 'center',
  fontFamily: 'PT Serif',
  paddingTop: '1em',
  width: '100%'
}
const ReviewContentStyle = { textAlign: 'center', marginBottom: '1em' }
const ReviewAuthorStyle = {
  color: '#000',
  fontWeight: 'bold',
  fontSize: '1rem',
  margin: 0
}
const RenderRecruitmentListStyle = {
  textAlign: 'left',
  display: 'flex',
  flexDirection: 'column'
}
const VacancyLabelStyle = { color: '#AFAFAF', fontWeight: 'bold' }
const PilarButtonStyle = {
  marginTop: '1em',
  padding: '0.5em 1em',
  height: 'auto',
  width: 'auto',
  backgroundColor: '#E7B12E',
  color: '#fff',
  border: 0
}
const FormAbsoluteCardStyle = {
  borderRadius: '0.5em',
  boxShadow: '0 5px 5px rgb(0, 0, 0, 0.5)'
}
const FormInputStyle = { borderRadius: '0.5em' }
const PaginationButtonStyle = { color: '#B0B0B0', margin: '0 0.5em' }

const Career = () => {
  const [{ language }] = LanguageContext()
  const { t } = useTranslation()

  // Header Content
  const [headerContent, setHeaderContent] = useState({})
  const [loadingHeaderContent, setLoadingHeaderContent] = useState(false)
  const [errorHeaderContent, setErrorHeaderContent] = useState(false)
  // Header Content

  // Introduction Content
  const [introductionContent, setIntroductionContent] = useState({})
  const [loadingIntroductionContent, setLoadingIntroductionContent] =
    useState(false)
  const [errorIntroductionContent, setErrorIntroductionContent] =
    useState(false)
  // Introduction Content

  // Recruitment Content
  const recruitmentPer = 12
  const [recruitmentList, setRecruitmentList] = useState([])
  const [loadingRecruitmentList, setLoadingRecruitmentList] = useState(false)
  const [errorRecruitmentList, setErrorRecruitmentList] = useState(false)
  const [recruitmentPage, setRecruitmentPage] = useState(1)
  const [recruitmentTotal, setRecruitmentTotal] = useState(0)
  // Recruitment Content

  // Email Recruitment
  const [emailRecruitment, setEmailRecruitment] = useState('')
  const [loadingEmailRecruitment, setLoadingEmailRecruitment] = useState(false)
  const [errorEmailRecruitment, setErrorEmailRecruitment] = useState(false)
  // Email Recruitment

  // Testimonies Content
  // const testimoniesPage = 1
  // const testimoniesPer = 3
  const [testimoniesList, setTestimoniesList] = useState([])
  const [loadingTestimoniesList, setLoadingTestimoniesList] = useState(false)
  const [errorTestimoniesList, setErrorTestimoniesList] = useState(false)
  // Testimonies Content

  // Form
  const formType = 'openRecruitment'
  const [loadingForm, setLoadingForm] = useState(false)
  // Form

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getHeaderContent()
      getIntroductionContent()
      getTestimoniesList()
      getEmailRecruitment()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getRecruitmentList(recruitmentPage)
    }

    return () => {
      subscribe = false
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [recruitmentPage])

  const getHeaderContent = async () => {
    setLoadingHeaderContent(true)
    try {
      if (dummyHeaderCareer.data) {
        let data = dummyHeaderCareer.data
        setHeaderContent(data)
        setErrorHeaderContent(false)
      } else {
        setErrorHeaderContent(true)
      }
    } catch (error) {
      setErrorHeaderContent(true)
    }
    setLoadingHeaderContent(false)
  }

  const getIntroductionContent = async () => {
    setLoadingIntroductionContent(true)
    try {
      if (dummyAboutUsContent.data) {
        let data = dummyAboutUsContent.data
        setIntroductionContent(data)
        setErrorIntroductionContent(false)
      } else {
        setErrorIntroductionContent(true)
      }
    } catch (error) {
      setErrorIntroductionContent(true)
    }
    setLoadingIntroductionContent(false)
  }

  const getTestimoniesList = async () => {
    setLoadingTestimoniesList(true)
    try {
      if (dummyTestmoniesContent.data) {
        let data = dummyTestmoniesContent.data.employeeTestimonies
        setTestimoniesList(data)
        setErrorTestimoniesList(false)
      } else {
        setErrorTestimoniesList(true)
      }
    } catch (error) {
      setErrorTestimoniesList(true)
    }
    setLoadingTestimoniesList(false)
  }

  const getEmailRecruitment = async () => {
    setLoadingEmailRecruitment(true)
    try {
      if (dummyContactContent.data) {
        let data = dummyContactContent.data
        let email = data?.eng?.email?.value || data?.ina?.email?.value
        setEmailRecruitment(email)
        setErrorEmailRecruitment(false)
      } else {
        setErrorEmailRecruitment(true)
      }
    } catch (error) {
      setErrorEmailRecruitment(true)
    }
    setLoadingEmailRecruitment(false)
  }

  const getRecruitmentList = async (recruitmentPage) => {
    setLoadingRecruitmentList(true)
    try {
      if (dummyRecruitmentContent.data) {
        let data = dummyRecruitmentContent.data.openRecruitments
        setRecruitmentList(data)
        setRecruitmentTotal(getRecruitmentList?.data?.pagination?.totalCount)
        setErrorRecruitmentList(false)
      } else {
        setErrorRecruitmentList(true)
      }
    } catch (error) {
      setErrorRecruitmentList(true)
    }
    setLoadingRecruitmentList(false)
  }

  const handleForm = async (data) => {
    setLoadingForm(true)
    data.type = formType
    try {
      const sendMessage = true
      if (sendMessage) {
        showAlert('success', t('alert.success'))
      } else {
        showAlert('error', t('alert.error'))
      }
    } catch (error) {
      showAlert('error', t('alert.error'))
    }
    setLoadingForm(false)
  }

  const handleSendFiles = (recruitment) => {
    let subject = `Apply for a ${recruitment} position`
    window.open(`mailto:${emailRecruitment}?subject=${subject}`)
  }

  const showAlert = (type, message) => {
    notification[type]({
      message: message,
      duration: 5
    })
  }

  const RenderLoadingHeaderContent = () => {
    return (
      <Row>
        <Col span={24}>
          <PilarLoading type='black' height='50vh' borderRadius='0' />
        </Col>
      </Row>
    )
  }

  const RenderErrorHeaderContent = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingHeaderContent />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingHeaderContent}
            onClick={() => getHeaderContent()}
          >
            {t('button.reload')}
          </Button>
        </div>
      </div>
    )
  }

  const RenderHeaderContent = () => {
    if (errorHeaderContent) {
      return <RenderErrorHeaderContent />
    } else if (!loadingHeaderContent && !errorHeaderContent) {
      return (
        <Row
          className='HeaderContentClass'
          style={{
            backgroundImage: `url(${
              headerContent.imageUrl ? headerContent.imageUrl : CareerBackground
            })`
          }}
        >
          <Col span={24}>
            <Row className='PilarCarouselClass HeaderCarouselClass'>
              <Title
                level={2}
                style={HeaderCarouselTitleStyle}
                className='trimString'
              >
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      headerContent?.eng?.contents[0].description
                        ? headerContent?.eng?.contents[0].description
                        : '',
                      headerContent?.ina?.contents[0].description
                        ? headerContent?.ina?.contents[0].description
                        : ''
                    )
                  }}
                ></div>
              </Title>
            </Row>
          </Col>
        </Row>
      )
    } else {
      return <RenderLoadingHeaderContent />
    }
  }

  const RenderIntroductionContent = () => {
    if (!loadingIntroductionContent && !errorIntroductionContent) {
      if (Object.keys(introductionContent).length !== 0) {
        return (
          <Row>
            <Col span={24}>
              <Title level={2} style={{ ...TitleStyle, textAlign: 'center' }}>
                {textTranslate(
                  language,
                  introductionContent?.eng?.title,
                  introductionContent?.ina?.title
                )}
              </Title>
            </Col>
            <Col offset={2} span={20} style={{ textAlign: 'center' }}>
              <Text>
                <div
                  dangerouslySetInnerHTML={{
                    __html: textTranslate(
                      language,
                      introductionContent?.eng?.description,
                      introductionContent?.ina?.description
                    )
                  }}
                ></div>
              </Text>
            </Col>
          </Row>
        )
      } else return null
    } else {
      return <RenderErrorLoadingIntroductionContent />
    }
  }

  const RenderErrorLoadingIntroductionContent = () => {
    let dummyLoading = [0, 1, 2, 3]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col xs={24} md={24} lg={{ offset: 2, span: 20 }}>
          <Row>
            {dummyLoading.map((data) => {
              return (
                <Col span={24} key={data} style={{ marginBottom: '10px' }}>
                  <PilarLoading type='black' height='20px' borderRadius='5px' />
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorIntroductionContent ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingIntroductionContent}
              onClick={() => getIntroductionContent()}
            >
              <Text>{t('button.reload')}</Text>
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperIntroductionContent = () => {
    if (
      !loadingIntroductionContent &&
      !errorIntroductionContent &&
      Object.keys(introductionContent).length === 0
    ) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <RenderIntroductionContent />
        </Row>
      )
    }
  }

  const RenderTestimoniesList = () => {
    if (!loadingTestimoniesList && !errorTestimoniesList) {
      if (testimoniesList.length !== 0) {
        return (
          <Col span={24}>
            <Row>
              {testimoniesList.map((data) => {
                return (
                  <Col
                    xs={24}
                    md={8}
                    lg={8}
                    key={data.id}
                    style={{ padding: '0.5em' }}
                  >
                    <Card
                      hoverable
                      style={{ height: '100%' }}
                      bodyStyle={{
                        height: '100%',
                        display: 'flex',
                        flexDirection: 'column'
                      }}
                    >
                      <div style={ReviewContentStyle}>
                        <img
                          src={QuoteIcon}
                          alt=''
                          style={{ height: '2rem', width: '2rem' }}
                        />
                      </div>
                      <div
                        className='ReviewContentClass'
                        style={{
                          ...ReviewContentStyle,
                          paddingBottom: '1.5em',
                          flex: 1
                        }}
                      >
                        <Text>
                          <div
                            dangerouslySetInnerHTML={{
                              __html: textTranslate(
                                language,
                                data?.eng?.testimony
                                  ? data?.eng?.testimony
                                  : '',
                                data?.ina?.testimony ? data?.ina?.testimony : ''
                              )
                            }}
                          ></div>
                        </Text>
                      </div>
                      <div
                        style={{
                          marginLeft: 'auto',
                          marginRight: 'auto',
                          justifyContent: 'flex-end'
                        }}
                      >
                        <Comment
                          key={data.id}
                          author={<p style={ReviewAuthorStyle}>{data.name}</p>}
                          avatar={<Avatar src={data.avatar} alt={data.name} />}
                          content={
                            <p>
                              {textTranslate(
                                language,
                                data.eng.position,
                                data.ina.position
                              )}
                            </p>
                          }
                        />
                      </div>
                    </Card>
                  </Col>
                )
              })}
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorLoadingTestimoniesList />
    }
  }

  const RenderErrorLoadingTestimoniesList = () => {
    let dummyLoading = checkDevice() === 'Mobile' ? [0] : [0, 1, 2]
    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col span={24}>
          <Row>
            {dummyLoading.map((data) => {
              return (
                <Col
                  xs={24}
                  md={8}
                  lg={8}
                  key={data}
                  style={{ padding: '0.5em' }}
                >
                  <Card hoverable style={{ height: '100%' }}>
                    <Row>
                      <Col offset={8} span={8} style={ReviewContentStyle}>
                        <PilarLoading
                          type='image'
                          height='50px'
                          borderRadius='50%'
                        />
                      </Col>
                      {[0, 1, 2].map((data) => {
                        return (
                          <Col
                            key={data}
                            span={24}
                            style={{
                              paddingBottom: '10px'
                            }}
                          >
                            <PilarLoading
                              type='black'
                              height='10px'
                              borderRadius='5px'
                            />
                          </Col>
                        )
                      })}
                      <Col
                        key={data}
                        span={8}
                        offset={8}
                        style={{
                          paddingBottom: '30px'
                        }}
                      >
                        <PilarLoading
                          type='black'
                          height='10px'
                          borderRadius='5px'
                        />
                      </Col>
                      <Col span={24}>
                        <Row>
                          <Col span={8}>
                            <PilarLoading
                              type='image'
                              height='30px'
                              borderRadius='50%'
                            />
                          </Col>
                          <Col span={16}>
                            <Row>
                              <Col
                                span={24}
                                style={{
                                  paddingBottom: '10px'
                                }}
                              >
                                <PilarLoading
                                  type='black'
                                  height='10px'
                                  borderRadius='5px'
                                />
                              </Col>
                              <Col span={24}>
                                <PilarLoading
                                  type='black'
                                  height='10px'
                                  borderRadius='5px'
                                />
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      </Col>
                    </Row>
                  </Card>
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorTestimoniesList ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingTestimoniesList}
              onClick={() => getTestimoniesList()}
            >
              {t('button.reload')}
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperTestimoniesList = () => {
    if (
      !loadingTestimoniesList &&
      !errorTestimoniesList &&
      testimoniesList.length === 0 &&
      !loadingEmailRecruitment &&
      !errorEmailRecruitment &&
      emailRecruitment
    ) {
      return null
    } else {
      return (
        <Row
          className='WrapperContentClass'
          style={{ backgroundColor: '#FAFAFA' }}
        >
          <Col span={24}>
            <Title
              level={2}
              className='CarouselTitleClass'
              style={{ textAlign: 'center', paddingTop: '0.5em' }}
            >
              {t('career.testimonies')}
            </Title>
          </Col>
          <RenderTestimoniesList />
        </Row>
      )
    }
  }

  const RenderRecruitmentList = () => {
    if (
      !loadingRecruitmentList &&
      !errorRecruitmentList &&
      !loadingEmailRecruitment &&
      !errorEmailRecruitment
    ) {
      if (recruitmentList.length !== 0 && emailRecruitment) {
        return (
          <Col
            xs={{ span: 20, offset: 2 }}
            md={{ span: 18, offset: 3 }}
            lg={{ span: 18, offset: 3 }}
          >
            <Row gutter={[16, 24]}>
              {recruitmentList.map((data) => {
                return (
                  <Col
                    key={data.id}
                    style={RenderRecruitmentListStyle}
                    xs={12}
                    md={8}
                    lg={8}
                  >
                    <div style={{ flex: 1 }}>
                      <Title level={5} style={{ color: '#565656' }}>
                        {textTranslate(
                          language,
                          data?.eng?.title ? data?.eng?.title : '',
                          data?.ina?.title ? data?.ina?.title : ''
                        )}
                      </Title>
                    </div>
                    <div
                      style={{ cursor: 'pointer', width: 'fit-content' }}
                      onClick={() => handleSendFiles(data?.eng?.title)}
                    >
                      <Text
                        style={{
                          color: '#e7b12e',
                          textDecoration: 'underline'
                        }}
                      >
                        {t('button.sendFiles')}
                      </Text>
                      <ArrowRightOutlined
                        style={{ color: '#e7b12e', marginLeft: '0.5em' }}
                      />
                    </div>
                  </Col>
                )
              })}
            </Row>
          </Col>
        )
      } else return null
    } else {
      return <RenderErrorLoadingRecruitmentList />
    }
  }

  const RenderErrorLoadingRecruitmentList = () => {
    let dummyLoading =
      checkDevice() === 'Mobile' ? [0, 1, 2, 3] : [0, 1, 2, 3, 4, 5]

    return (
      <Col span={24} className='PilarErrorLoadingWrapper'>
        <Col
          xs={{ span: 24 }}
          md={{ span: 18, offset: 3 }}
          lg={{ span: 18, offset: 3 }}
        >
          <Row gutter={[16, 32]}>
            {dummyLoading.map((data) => {
              return (
                <Col
                  key={data}
                  style={RenderRecruitmentListStyle}
                  xs={12}
                  md={8}
                  lg={8}
                >
                  <Row gutter={[16, 16]}>
                    <Col span={24}>
                      <PilarLoading
                        type='black'
                        height='30px'
                        borderRadius='5px'
                      />
                    </Col>
                    <Col span={12}>
                      <PilarLoading
                        type='black'
                        height='20px'
                        borderRadius='5px'
                      />
                    </Col>
                  </Row>
                </Col>
              )
            })}
          </Row>
        </Col>
        {errorRecruitmentList || errorEmailRecruitment ? (
          <div className='PilarErrorLoading'>
            <Button
              className='PilarButton'
              shape='round'
              loading={loadingRecruitmentList}
              onClick={() => {
                getRecruitmentList(recruitmentPage)
                getEmailRecruitment()
              }}
            >
              {t('button.reload')}
            </Button>
          </div>
        ) : null}
      </Col>
    )
  }

  const RenderWrapperRecruitmentList = () => {
    if (
      !loadingRecruitmentList &&
      !errorRecruitmentList &&
      recruitmentList.length === 0
    ) {
      return null
    } else {
      return (
        <Row className='WrapperContentClass'>
          <Col span={24}>
            <Title
              level={2}
              style={{
                ...TitleStyle,
                textAlign: 'center',
                marginBottom: '1em'
              }}
            >
              {t('career.position')}
            </Title>
          </Col>
          <RenderRecruitmentList />
          <RenderPagination />
        </Row>
      )
    }
  }

  const itemRender = (current, type, originalElement) => {
    if (type === 'prev') {
      return <span style={PaginationButtonStyle}>{t('pagination.prev')}</span>
    }
    if (type === 'next') {
      return <span style={PaginationButtonStyle}>{t('pagination.next')}</span>
    }
    return originalElement
  }

  const RenderPagination = () => {
    return (
      <Col
        xs={24}
        md={24}
        lg={{ offset: 2, span: 20 }}
        style={{ textAlign: 'center' }}
      >
        <Pagination
          className='PilarPagination'
          responsive
          hideOnSinglePage
          current={recruitmentPage}
          total={recruitmentTotal}
          pageSize={recruitmentPer}
          itemRender={itemRender}
          onChange={(page) => {
            setRecruitmentPage(page)
          }}
        />
      </Col>
    )
  }

  const RenderVacanyFormContent = () => {
    const [form] = Form.useForm()
    return (
      <Row className='WrapperContentClass'>
        <Col span={24}>
          <Title level={2} style={{ ...TitleStyle, textAlign: 'center' }}>
            {t('career.positionNotFound')}
          </Title>
        </Col>
        <Col xs={24} md={24} lg={{ offset: 4, span: 16 }}>
          <Title
            level={4}
            style={{ ...TitleStyle, textAlign: 'center', color: '#565656' }}
          >
            {t('career.inputPosition')}
          </Title>
        </Col>
        <Col
          xs={24}
          md={{ offset: 4, span: 16 }}
          lg={{ offset: 6, span: 12 }}
          style={{ marginTop: '2em' }}
        >
          <Card style={FormAbsoluteCardStyle}>
            <Form
              form={form}
              layout='vertical'
              name='vacancyForm'
              requiredMark={false}
              onFinish={(values) => handleForm(values)}
            >
              <Form.Item
                name='name'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.name.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.name.message')
                  }
                ]}
              >
                <Input style={FormInputStyle} />
              </Form.Item>
              <Form.Item
                name='email'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.email.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.email.message')
                  }
                ]}
              >
                <Input style={FormInputStyle} />
              </Form.Item>
              <Form.Item
                name='phoneNumber'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.phoneNumber.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.phoneNumber.message')
                  }
                ]}
              >
                <Input style={FormInputStyle} />
              </Form.Item>
              <Form.Item
                name='message'
                label={
                  <label style={VacancyLabelStyle}>
                    {t('form.message.label')}
                  </label>
                }
                rules={[
                  {
                    required: true,
                    message: t('form.message.message')
                  }
                ]}
              >
                <Input.TextArea
                  style={{ ...FormInputStyle, minHeight: '100px' }}
                />
              </Form.Item>
              <Form.Item>
                <Button
                  htmlType='submit'
                  shape='round'
                  size={'small'}
                  style={{ ...PilarButtonStyle, width: '100%' }}
                  loading={loadingForm}
                >
                  <Text style={{ color: '#fff' }}>{t('button.submit')}</Text>
                </Button>
              </Form.Item>
            </Form>
          </Card>
        </Col>
      </Row>
    )
  }

  return (
    <div>
      <RenderHeaderContent />
      <RenderWrapperIntroductionContent />
      <RenderWrapperTestimoniesList />
      <RenderWrapperRecruitmentList />
      <RenderVacanyFormContent />
    </div>
  )
}

export default Career

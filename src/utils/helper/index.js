import moment from 'moment'

export const goTo = ({ location, state }) => {
  window.directTo.push({
    pathname: location,
    state: state
  })
}

export const checkLanguage = () => {
  const local = localStorage.getItem('language')
  const language = local ? local : 'INA'
  return language
}

export const setLanguage = (language) => {
  localStorage.setItem('language', language)
}

export const textTranslate = (language, textEng, textInd) => {
  if (language === 'ENG') {
    return textEng
  }
  return textInd
}

export const checkDevice = () => {
  const device = localStorage.getItem('device')
  return device
}

export const trimString = (str, max) => {
  if (!str) return ''
  let trimmedString = ''
  if (str && str.length > max) {
    trimmedString = `${str.substring(0, max)}...`
  } else {
    trimmedString = str
  }
  return trimmedString
}

export const convertDate = (date) => {
  if (!date) return ''
  return moment(date).format('DD MMMM YYYY')
}

export const goToLink = (link) => {
  if (link) {
    console.log({ link })
    window.open(link, '_blank')
  }
}

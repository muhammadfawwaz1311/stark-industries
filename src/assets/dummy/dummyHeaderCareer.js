const dummyHeaderCareer = {
  data: {
    id: '5fc866d559f6ee0004c7137a',
    page: 'career',
    imageUrl:
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234658/pilar/headers/tvcxloikwcpgb9gghizw.jpg',
    eng: {
      contents: [
        {
          title: '<p>Career at Stark Industries</p>',
          description: '<p>Career at Stark Industries</p>'
        }
      ]
    },
    ina: {
      contents: [
        {
          title: '<p>Career at Stark Industries</p>',
          description: '<p>Career at Stark Industries</p>'
        }
      ]
    }
  },
  message: ''
}
export default dummyHeaderCareer

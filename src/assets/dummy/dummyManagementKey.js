const dummyManagementKey = {
  data: {
    managements: [
      {
        id: '5fd900c0e0728e0004667730',
        name: 'Howard Stark',
        eng: { position: 'Commissionaire' },
        ina: { position: 'Komisioner' },
        imageUrl:
          'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/3/31/Howard_Stark.png',
        displayOrder: 1,
        type: 'key'
      },
      {
        id: '5fd90192e0728e0004667731',
        name: 'Tony Stark',
        eng: { position: 'Chairman' },
        ina: { position: 'Chairman' },
        imageUrl:
          'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/3/35/IronMan-EndgameProfile.jpg',
        displayOrder: 2,
        type: 'key'
      },
      {
        id: '5fd901b3e0728e0004667732',
        name: 'Pepper Potts',
        eng: { position: 'Director' },
        ina: { position: 'Direktur' },
        imageUrl:
          'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/8/86/F_153752.jpg',
        displayOrder: 3,
        type: 'key'
      },
      {
        id: '5fd901c8e0728e0004667733',
        name: 'Happy Hogan',
        eng: { position: 'Director' },
        ina: { position: 'Direktur ' },
        imageUrl:
          'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/7/7b/Happy_Hogan_Endgame_Textless.jpg',
        displayOrder: 4,
        type: 'key'
      }
    ]
  },
  pagination: { page: 1, per: 10, totalCount: 4, totalPages: 1 },
  message: ''
}
export default dummyManagementKey

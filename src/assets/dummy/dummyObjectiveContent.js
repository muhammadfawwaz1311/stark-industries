const dummyObjectiveContent = {
  data: {
    ina: {
      mission: {
        id: '5fd261d9ceb52500042ee268',
        code: 'mission',
        label: 'Misi',
        values: [
          'Memperkuat Posisi Perusahaan dengan Kekuatan Visi dan Nilai untuk Menjadi Perusahan  pengembang properti terbaik. ',
          'Mengembangkan Sumber Daya Manusia yang berkualitas tinggi, profesional, sejahtera, sesuai dengan nilai dan budaya perusahaan. ',
          'Mengembangkan produk yang bermanfaat dan bernilai tinggi bagi konsumen dengan menerapkan prinsip tata kelola perusahaan yang baik dan senantiasa melakukan perbaikan berkelanjutan. ',
          'Menjalin kerjasama strategis dan memastikan tingkat pengembalian investasi yang maksimal bagi partner bisnis.',
          'Membangun lingkungan kerja yang kondusif agar membangkitkan motivasi dan memberikan ketenangan bagi seluruh karyawan untuk bekerja secara professional '
        ]
      },
      vission: {
        id: '5fd26178ceb52500042ee265',
        code: 'vission',
        label: 'Visi',
        values: [
          'Berkesungguhan menjadi perusahaan pengembang property terbaik dalam mewujudkan nilai dan kemanfaatan sebesar-besarnya bagi stakeholder secara berkelanjutan'
        ]
      },
      value: {
        id: '5fd26256ceb52500042ee26e',
        code: 'value',
        label: 'Nilai',
        values: [
          'Berbuat yang Terbaik',
          'Teguh Dalam Kebenaran',
          'Amanah',
          'Harmoni',
          'Bijaksana',
          'Ikhlas'
        ]
      }
    },
    eng: {
      mission: {
        id: '5fd261d9ceb52500042ee268',
        code: 'mission',
        label: 'Mission',
        values: [
          "Strengthening the Company's Position with the Strength of Vision and Value to Become the best property development company. ",
          "Developing high quality, professional, prosperous Human Resources in accordance with the company's values and culture.",
          'Developing products that are useful and of high value to consumers by applying the principles of good corporate governance and continuously making continuous improvements.',
          'Establish strategic partnerships and ensure maximum return on investment for business partners.',
          'Developing a conducive work environment in order to inspire motivation and provide comfortability for all employees to work professionally'
        ]
      },
      vission: {
        id: '5fd26178ceb52500042ee265',
        code: 'vission',
        label: 'Vission',
        values: [
          'Dedicated towards becoming the best property development company in realizing maximum values and benefits for stakeholders in a sustainable manner'
        ]
      },
      value: {
        id: '5fd26256ceb52500042ee26e',
        code: 'value',
        label: 'Value',
        values: [
          'Excellence',
          'Firm in the truth',
          'Trusted',
          'Harmony',
          'Wise',
          'Sincere'
        ]
      }
    }
  },
  message: ''
}

export default dummyObjectiveContent

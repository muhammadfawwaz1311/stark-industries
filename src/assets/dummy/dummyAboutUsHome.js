const dummyAboutUsHome = {
  data: {
    id: '5fd6512a15a6890004deb6f2',
    code: 'aboutUs',
    imageUrls: [
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608236635/pilar/landingPageImages/d9ewoxvcgc1icwqfhihj.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608236635/pilar/landingPageImages/agyplvh1wfeivqrvcgfj.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608236636/pilar/landingPageImages/uz4kdkxvtexccfygu5gg.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608236637/pilar/landingPageImages/itskuzsn4ndqqhw86enx.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608531972/pilar/landingPageImages/jtw2tyiqvraaljh0n0jh.jpg'
    ],
    ina: {
      title: 'Tentang Kami',
      description:
        '<p>Stark Industries didirikan oleh Howard Stark pada awal abad kedua puluh, pelopor besar dalam berbagai jenis teknologi dan terus-menerus membantu Angkatan Bersenjata Amerika Serikat dengan senjata yang berbeda dan inovatif, membantu pemerintah untuk mengembangkan Proyek Kelahiran Kembali pada tahun 1940-an.</p>'
    },
    eng: {
      title: 'About Us',
      description:
        '<p>Stark Industries was founded by Howard Stark during the early twentieth century, a great pioneer in different types of technology and constantly assisted the United States Armed Forces with different and innovative weapons, helping the government to develop the Project Rebirth in the 1940s.</p>'
    }
  },
  message: ''
}
export default dummyAboutUsHome

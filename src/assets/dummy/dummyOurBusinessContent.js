const dummyOurBusinessContent = {
  data: {
    id: '5fd6442615a6890004deb6f0',
    code: 'business',
    ina: {
      title: 'Bisnis Kami',
      description:
        '<p>Stark Industries memiliki tiga sub-bidang usaha yaitu Jasa Konstruksi, Pengembang Properti, dan pengelola properti. Dengan memilik sub perusahaan yang bergerak di berbagai macam bidang tersebut, Stark Industries siap melebarkan sayap untuk berbagai macam proyek di sektor industri konstruksi dan properti.&nbsp;</p>'
    },
    eng: {
      title: 'Our Business',
      description:
        '<p>Stark Industries has three sub-fields of business:&nbsp; construction services, property development and property management. By having subsidiary companies that engaged in various kinds, Stark Industries set sail to expand into various kinds of project in the construction and property industry sectors.</p>'
    }
  },
  message: ''
}
export default dummyOurBusinessContent

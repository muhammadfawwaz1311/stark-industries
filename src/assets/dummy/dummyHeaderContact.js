const dummyHeaderContact = {
  data: {
    id: '5fc866d059f6ee0004c71378',
    page: 'contact',
    imageUrl:
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234643/pilar/headers/j4xg0n5in9shfwhfujcp.jpg',
    eng: {
      contents: [
        {
          title: '<p>Stay connected with us</p>',
          description: '<p>Stay connected with us</p>'
        }
      ]
    },
    ina: {
      contents: [
        {
          title: '<p>Stay connected with us</p>',
          description: '<p>Stay connected with us</p>'
        }
      ]
    }
  },
  message: ''
}
export default dummyHeaderContact

const dummyHeaderGallery = {
  data: {
    id: '5fc866cb59f6ee0004c71376',
    page: 'album',
    imageUrl:
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234535/pilar/headers/f1iabksbrhbodogjmf9f.jpg',
    eng: {
      contents: [
        {
          title: '<p>Stark Industries Moment Captured in Frame</p>',
          description: '<p>Stark Industries Moment Captured in Frame</p>'
        }
      ]
    },
    ina: {
      contents: [
        {
          title: '<p>Stark Industries Moment Captured in Frame</p>',
          description: '<p>Stark Industries Moment Captured in Frame</p>'
        }
      ]
    }
  },
  message: ''
}
export default dummyHeaderGallery

const dummyManagementTeam = {
  data: {
    managements: [
      {
        id: '5fd903b0e0728e0004667737',
        name: 'Management Team',
        eng: { position: 'Team' },
        ina: { position: 'Tim' },
        imageUrl:
          'https://i.insider.com/5d6543572e22af124c595c55?width=1000&format=jpeg&auto=webp',
        displayOrder: 1,
        type: 'team'
      }
    ]
  },
  pagination: { page: 1, per: 10, totalCount: 1, totalPages: 1 },
  message: ''
}
export default dummyManagementTeam

import { StarkIndustriesCircle } from 'assets/images'

const dummyBusinessContent = {
  data: {
    businesses: [
      {
        id: '5fd10cc30ddebf000437e98c',
        ina: {
          name: 'Stark Construction',
          description:
            '<p>Stark Construction adalah sub-holding usaha dari Stark Industries yang bergerak dibidang jasa konstruksi. Proyek kami meliputi pembangunan apartemen, gedung, perumahan, juga infrastruktur seperti jalan, infrastruktur tambang, dan jembatan. Fokus untuk memberikan dampak yang baik untuk masyarakat dan lingkungan serta memberikan lapangan kerja yang menggerakan ekonomi lokal.&nbsp;</p>',
          summaries: []
        },
        eng: {
          name: 'Stark Construction',
          description:
            '<p>Stark Construction is a sub-holding business of Stark Industries which engaged in construction services. Our projects portfolio consists of apartments construction, buildings, housing, infrastructures such as roads, mining infrastructure, and bridges. Focus on making a beneficial impact on society and the environment by providing jobs that drive the local economy.</p>',
          summaries: []
        },
        projectImages: [
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609034054/pilar/projects/btc7qvd1pknytnoau1sn.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609033814/pilar/projects/e9dsu4pk4neyyct1b4kh.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609034454/pilar/projects/vgwohrctf8spby76lpta.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609033990/pilar/projects/svz3lk5mwmtdhhgvg1y2.jpg'
        ],
        logo: StarkIndustriesCircle,
        portraitImage:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608996140/pilar/managements/f3lc9tn9nnvl5zvbnds0.png'
      },
      {
        id: '5fd113600ddebf000437e9a3',
        ina: {
          name: 'Stark Development',
          description:
            '<p>Perusahaan pengembang properti yang mengembangkan lahan sehingga menjadi tempat tinggal dan tempat beraktivitas manusia yang layak dan nyaman. Setiap proyeknya mempunyai konsep dan competitive advantage yang kuat terhadap pesaing sehingga dapat menjadi yang terbaik di kategorinya. mewujudkan nilai secara maksimal untuk stakeholder, Menciptakan harmoni disetiap proyeknya baik harmoni di dalam hunian sehingga menciptakan peace of mind bagi customer, ataupun harmoni dengan masyarakat sekitar. Stark Development melalui PT. Avengers Corporation sebagai salah satu perusahaan afiliasinya sedang mengembangkan Office Tower di Los Angeles, California.</p>',
          summaries: []
        },
        eng: {
          name: 'Stark Development',
          description:
            '<p>Property development companies develop the land to become a proper and comfortable place to live and activities. Each project has a strong concept and a strong competitive advantage against competitors to be the best in its category. Maximizing the realization of value for stakeholders, Creating harmony in each project, both harmony with the dwelling to create peace of mind for customers, or harmony with the surrounding community. Avengers Corporation is an affiliated company with Stark Industries that builds an Office Tower in Los Angeles, California.</p>',
          summaries: []
        },
        projectImages: [
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609034297/pilar/projects/u34t6qwvjmyr8gbzt67i.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609034657/pilar/projects/z50bvsjqgsgmpbhu8ebt.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609034805/pilar/projects/bkjkjnammcwaqrthjszf.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609033990/pilar/projects/svz3lk5mwmtdhhgvg1y2.jpg'
        ],
        logo: StarkIndustriesCircle,
        portraitImage:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608992773/pilar/businesses/afq3f0qnjolfks7fhtu3.png'
      },
      {
        id: '5fd237a402ab7100042d7185',
        ina: {
          name: 'Stark Property',
          description:
            '<p>Stark Property menjalankan bisnis pengelolaan dan penyewaan apartemen, gedung perkantoran, dan estate management dengan standard tinggi. Kami mengelola sumber daya manusia untuk menyediakan layanan berkualitas tinggi agar aset yang dikelola dapat di tingkatkan dalam fungsi, nilai dan bermanfaat untuk setiap stakeholder. Stark Property mengembangkan bidang bisnisnya ke industri hospitality seperti pengelolaan hotel, tempat wisata, dan lain-lain.</p>',
          summaries: []
        },
        eng: {
          name: 'Stark Property',
          description:
            '<p>Stark Property operates the business of managing and leasing apartments, office buildings and estates managements with high standards. We manage human resources to provide high-quality services so that the assets under management can be increased in function, value and benefit to each stakeholder. Stark Property develops its business into the hospitality industry such as managing hotels, tourist attractions, and others.</p>',
          summaries: []
        },
        projectImages: [
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1609033909/pilar/projects/tudx5pllbowuu2yuhn2t.jpg'
        ],
        logo: StarkIndustriesCircle,
        portraitImage:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608992812/pilar/businesses/oxa0kufne6bsi5xgkm94.png'
      }
    ]
  },
  message: ''
}
export default dummyBusinessContent

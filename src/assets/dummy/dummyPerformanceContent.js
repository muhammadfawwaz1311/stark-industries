const dummyPerformanceContent = {
  data: {
    id: '5fd651b915a6890004deb6f4',
    code: 'performance',
    imageUrls: [
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608237245/pilar/landingPageImages/mlhzfi9vbvcehvkocpjj.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608237246/pilar/landingPageImages/sdmwshguxmsswqxmt5dc.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608237261/pilar/landingPageImages/ogcbieq50tv5ek02iwe2.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608237261/pilar/landingPageImages/ddoknnb9b6ilwc0jzikg.jpg',
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608532262/pilar/landingPageImages/d9jd6ol9ybbsokizri4i.jpg'
    ],
    ina: {
      title: 'Kinerja Stark Industries',
      description:
        '<p>Sebagai salah satu perusahaan yang bergerak di sektor industri properti, Stark Industries selalu membuktikan bahwa hasil setiap proyek terjamin kualitasnya. Stark Industries sukses mengembang dan mengerjakan apartemen, perumahan, proyek infrastruktur, bangunan rumah sakit, masjid, fasilitas umum, dan berbagai macam proyek dengan kualitas yang baik dan memiliki dampak positif bagi masyarakat.&nbsp;</p>'
    },
    eng: {
      title: 'Stark Industries Performance',
      description:
        '<p>As one of the companies operating in property industry sector, Stark Industries always proves that the quality of each project is guaranteed. Stark Industries has succeeded in making apartments with good quality and had a tremendous impact on the lives of Indonesians. More than that, Stark Industries takes part in a various infrastructure project, hospitals, public facilities, and more.&nbsp;</p>'
    }
  },
  message: ''
}

export default dummyPerformanceContent

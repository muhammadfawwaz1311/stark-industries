const dummyTestmoniesContent = {
  data: {
    employeeTestimonies: [
      {
        id: '5fc7d0ab8104fa000426f256',
        name: 'M. Mahardhika Zein',
        avatar:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608651555/pilar/managements/acufwonkuv4wohoiysaq.jpg',
        ina: {
          position: 'Business Development',
          testimony:
            '<p>Di Stark Industries, saya merasa setiap pekerjaan yang saya lakukan memiliki makna dan manfaat. Perusahaan ini juga memberi kesempatan belajar, berkembang, dan mengaktualisasi diri sehingga tidak peduli tua atau muda, semua dapat berkontribusi dengan kelebihannya.</p>'
        },
        eng: {
          position: 'Business Development',
          testimony:
            '<p>At Stark Industries, I feel that every job I do has meaning and benefits. This company also provides opportunities to learn, develop, and self-actualize so that no matter how old or young, everyone can contribute with their strengths.</p>'
        }
      },
      {
        id: '5fc7d0738104fa000426f255',
        name: 'Titis F. Parasyantri',
        avatar:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608651737/pilar/managements/af3ogxahnex8vv7apvn2.jpg',
        ina: {
          position: 'Chief of Personnel and General Affair',
          testimony:
            '<p>Bergabung menjadi salah satu bagian dari Pilarcorp seperti menemukan rumah kedua dengan keluarga baru di dalamnya<br></p>'
        },
        eng: {
          position: 'Chief of Personnel and General Affair',
          testimony:
            '<p>Joining being a part of Pilarcorp is like finding a second home with a new family in it</p>'
        }
      },
      {
        id: '5fc711472b0c5c0004459b8b',
        name: 'Ratna Sri Utami ',
        avatar:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608649345/pilar/employeeTestimonies/svqxutwcu3izwmg4h4ed.jpg',
        ina: {
          position: 'General Affair Manager HQ',
          testimony:
            '<p>Selama saya di Stark Industries, saya menerima: pembelajaran, kebersamaan, amal. Belajar bagaimana menjadi orang yang berguna bagi banyak orang. Ciptakan suasana kerja keluarga. Membuat semua pekerjaan memiliki nilai pemujaan.</p>'
        },
        eng: {
          position: 'General Affair Manager HQ',
          testimony:
            '<p>During my time at Stark Industries, I received: learning, togetherness, charity. Learning about how to be a person who is useful to many people. Get a family working atmosphere. Making all work has worship value.</p>'
        }
      }
    ]
  },
  pagination: { page: 1, per: 3, totalCount: 3, totalPages: 1 },
  message: ''
}
export default dummyTestmoniesContent

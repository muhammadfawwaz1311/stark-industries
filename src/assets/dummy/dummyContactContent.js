const dummyContactContent = {
  data: {
    ina: {
      phone: {
        label: 'Telepon',
        value: '831-901-9935    - (Fax : 831-901-3599)'
      },
      email: { label: 'Email Perusahaan', value: 'admin@starkindustries.com' },
      address: {
        detailAddress: 'Stark Industries',
        label: 'Alamat Kantor',
        province: 'New York',
        street: 'Los Angeles ',
        subDistrict: 'California',
        zipcode: '12435'
      },
      maps: {
        label: 'Maps',
        value:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d205566.73268640306!2d-119.79013691078393!3d36.385473397057716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fb9fe5f285e3d%3A0x8b5109a227086f55!2sCalifornia%2C%20USA!5e0!3m2!1sen!2sid!4v1658247937567!5m2!1sen!2sid'
      },
      videoUrl: {
        label: 'Youtube',
        value: 'https://www.youtube.com/embed/oKUdLlfOd8c'
      }
    },
    eng: {
      phone: {
        label: 'Phone Number',
        value: '831-901-9935    - (Fax : 831-901-3599)'
      },
      email: { label: 'Company Email', value: 'admin@starkindustries.com' },
      address: {
        detailAddress: 'Stark Industries',
        label: 'Address',
        province: 'New York',
        street: 'Los Angeles ',
        subDistrict: 'California',
        zipcode: '12435'
      },
      maps: {
        label: 'Maps',
        value:
          'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d205566.73268640306!2d-119.79013691078393!3d36.385473397057716!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x808fb9fe5f285e3d%3A0x8b5109a227086f55!2sCalifornia%2C%20USA!5e0!3m2!1sen!2sid!4v1658247937567!5m2!1sen!2sid'
      },
      videoUrl: {
        label: 'Youtube',
        value: 'https://www.youtube.com/embed/oKUdLlfOd8c'
      }
    }
  },
  message: ''
}
export default dummyContactContent

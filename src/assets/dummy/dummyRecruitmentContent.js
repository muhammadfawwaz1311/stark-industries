const dummyRecruitmentContent = {
  data: {
    openRecruitments: [
      {
        id: '5fc13145602cf600046a05f0',
        eng: { title: 'Marketing Communcation Staff (Los Angeles)' },
        ina: { title: 'Marketing Communication Staff (Los Angeles)' }
      },
      {
        id: '5fc66ab12d8f210004c750c4',
        eng: { title: 'Sales Marketing Property (Los Angeles)' },
        ina: { title: 'Sales Marketing Property (Los Angeles)' }
      },
      {
        id: '5fc76d6a92f9db0004c3b5b5',
        eng: { title: 'Marketing and Communication' },
        ina: { title: 'Marketing and Communication' }
      },
      {
        id: '5fc76dab92f9db0004c3b5b6',
        eng: { title: 'Business Development Officer (California)' },
        ina: { title: 'Business Development Officer (California)' }
      },
      {
        id: '5fe349c911863b0004aa4872',
        eng: { title: 'Tenant Relation (Los Angeles)' },
        ina: { title: 'Tenant Relation (Los Angeles)' }
      }
    ]
  },
  pagination: { page: 1, per: 12, totalCount: 5, totalPages: 1 },
  message: ''
}
export default dummyRecruitmentContent

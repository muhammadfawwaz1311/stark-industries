const dummySummaryContent = {
  data: {
    id: '5fd661a315a6890004deb6f5',
    code: 'progress',
    ina: {
      title: 'Kinerja Stark Industries dalam angka',
      description:
        '<p>Stark Industries dengan visi misi yang kuat dan didukung oleh sumber daya manusia yang profesional, bermodalkan pengalaman dan integritas yang terjaga telah berhasil membangun dan mengembangkan proyek-proyek dengan baik.&nbsp;</p>',
      summaries: [
        { label: 'Proyek', value: '54' },
        { label: 'Kota', value: '23' },
        { label: 'Jumlah unit hunian terbangun ', value: '700' },
        { label: 'Nilai proyek', value: '700 M' }
      ]
    },
    eng: {
      title: "Stark Industries's Achievement in numbers",
      description:
        '<p>Stark Industries proves itself as a company that is at the forefront of performance and also with&nbsp; satisfying results, with years of experience and integrity in the world of construction, and proven reputation in property development and management, Stark Industries continue to enhance the benefit through its business.</p>',
      summaries: [
        { label: 'Project', value: '54' },
        { label: 'Cities', value: '23' },
        { label: 'Total unit built', value: '700' },
        { label: 'Project value', value: '700 M' }
      ]
    }
  },
  message: ''
}
export default dummySummaryContent

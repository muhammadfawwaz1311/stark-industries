import dummyHeaderContent from './dummyHeaderContent'
import dummyPerformanceContent from './dummyPerformanceContent'
import dummyObjectiveContent from './dummyObjectiveContent'
import dummyPrefaceContent from './dummyPrefaceContent'
import dummySummaryContent from './dummySummaryContent'
import dummyPartnerContent from './dummyPartnerContent'
import dummyContactContent from './dummyContactContent'
import dummyManagementKey from './dummyManagementKey'
import dummyManagementTeam from './dummyManagementTeam'
import dummyBusinessContent from './dummyBusinessContent'
import dummyGalleryContent from './dummyGalleryContent'
import dummyAboutUsContent from './dummyAboutUsContent'
import dummyTestmoniesContent from './dummyTestimoniesContent'
import dummyRecruitmentContent from './dummyRecruitmentContent'
import dummyHeaderHome from './dummyHeaderHome'
import dummyWelcomeContent from './dummyWelcomeContent'
import dummyOurBusinessContent from './dummyOurBusinessContent'
import dummyAboutUsHome from './dummyAboutUsHome'
import dummyGalleryHome from './dummyGalleryHome'
import dummyHeaderBusiness from './dummyHeaderBusiness'
import dummyHeaderCareer from './dummyHeaderCareer'
import dummyHeaderContact from './dummyHeaderContact'
import dummyHeaderGallery from './dummyHeaderGallery'

export {
  dummyHeaderContent,
  dummyPerformanceContent,
  dummyObjectiveContent,
  dummyPrefaceContent,
  dummySummaryContent,
  dummyPartnerContent,
  dummyContactContent,
  dummyManagementKey,
  dummyManagementTeam,
  dummyBusinessContent,
  dummyGalleryContent,
  dummyAboutUsContent,
  dummyTestmoniesContent,
  dummyRecruitmentContent,
  dummyHeaderHome,
  dummyWelcomeContent,
  dummyOurBusinessContent,
  dummyAboutUsHome,
  dummyGalleryHome,
  dummyHeaderBusiness,
  dummyHeaderCareer,
  dummyHeaderContact,
  dummyHeaderGallery
}

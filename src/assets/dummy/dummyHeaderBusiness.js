const dummyHeaderBusiness = {
  data: {
    id: '5fc866c059f6ee0004c71372',
    page: 'business',
    imageUrl:
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234456/pilar/headers/ysqsw96eslg3mkhtd04v.jpg',
    eng: {
      contents: [
        {
          title: '<p>Engaged in several industrial sectors</p>',
          description: '<p>Engaged in several industrial sectors</p>'
        }
      ]
    },
    ina: {
      contents: [
        {
          title: '<p>Engaged in several industrial sectors</p>',
          description: '<p>Engaged in several industrial sectors</p>'
        }
      ]
    }
  },
  message: ''
}

export default dummyHeaderBusiness

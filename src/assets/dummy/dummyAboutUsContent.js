const dummyAboutUsContent = {
  data: {
    id: '5fd643be15a6890004deb6ef',
    code: 'aboutUs',
    ina: {
      title: 'STARK INDUSTRIES',
      description:
        '<p><span style="font-weight: 400;" data-mce-style="font-weight: 400;">Stark Industries didirikan oleh Howard Stark pada awal abad kedua puluh, pelopor besar dalam berbagai jenis teknologi dan terus-menerus membantu Angkatan Bersenjata Amerika Serikat dengan senjata yang berbeda dan inovatif, membantu pemerintah untuk mengembangkan Proyek Kelahiran Kembali pada tahun 1940-an.<em>.</em></span></p>'
    },
    eng: {
      title: 'STARK INDUSTRIES',
      description:
        '<p>Stark Industries was founded by Howard Stark during the early twentieth century, a great pioneer in different types of technology and constantly assisted the United States Armed Forces with different and innovative weapons, helping the government to develop the Project Rebirth in the 1940s.&nbsp;</p><p><br></p>'
    }
  },
  message: ''
}
export default dummyAboutUsContent

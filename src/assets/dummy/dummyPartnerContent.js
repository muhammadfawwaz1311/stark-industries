const dummyPartnerContent = {
  data: {
    partners: [
      {
        id: '5fc7c65e8104fa000426f24f',
        name: 'Isalam Development Bank',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234928/pilar/partners/sdqsowuw9gki65tdp42m.png'
      },
      {
        id: '5fc7c6798104fa000426f250',
        name: 'Pertamina',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234865/pilar/partners/uj40gnfjmidqenjttcag.png'
      },
      {
        id: '5fc7c68b8104fa000426f251',
        name: 'Parkside Hotel Group',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235023/pilar/partners/mokf1vpllrxtuukmsb4i.png'
      },
      {
        id: '5fc7c6a18104fa000426f252',
        name: 'Adaro Energy',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235072/pilar/partners/v3qcuuh0fhbdddh1dmff.png'
      },
      {
        id: '5fc7c6b38104fa000426f253',
        name: 'Adhi Persada Gedung',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235099/pilar/partners/qxiksvifkzvsd8apo518.png'
      },
      {
        id: '5fdbb873b76903000479e3a7',
        name: 'Shell',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235122/pilar/partners/q8xaqbid1hcbs6joy2r0.png'
      },
      {
        id: '5fdbb882b76903000479e3a8',
        name: 'Atalian Global Service',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235137/pilar/partners/zvb1xu3cygg4eoc3fyn6.png'
      },
      {
        id: '5fdbb893b76903000479e3a9',
        name: 'Centre Park',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235153/pilar/partners/jknvm0mxkm2zpmacrkmx.png'
      },
      {
        id: '5fdbb8a5b76903000479e3aa',
        name: 'Dana Pensiun Pertamina',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235172/pilar/partners/pmvxqccxa3p3okiloge3.png'
      },
      {
        id: '5fdbb8bab76903000479e3ab',
        name: 'HIMPERRA',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235191/pilar/partners/ea0ehgcqyip8ikzp0nq9.png'
      },
      {
        id: '5fdbb8cab76903000479e3ac',
        name: 'Revive Hotel Indonesia',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235209/pilar/partners/biwlgdaeymdbcboupcd9.jpg'
      },
      {
        id: '5fdbb8e5b76903000479e3ad',
        name: 'Spanish RedCross',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235236/pilar/partners/xmtqcshad1obho8iw6r6.gif'
      },
      {
        id: '5fdbb900b76903000479e3ae',
        name: 'jayakonstruksi',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235263/pilar/partners/ehl8xzufi1jlchd7ifou.jpg'
      },
      {
        id: '5fdbb92eb76903000479e3af',
        name: 'Qatar Charity Indonesia',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235309/pilar/partners/r269igfnguza4ixgxant.jpg'
      },
      {
        id: '5fdbb94fb76903000479e3b0',
        name: 'Dompet Dhuafa',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235342/pilar/partners/kzvmrx7y7ycdwph1zppa.png'
      },
      {
        id: '5fdbb970b76903000479e3b1',
        name: 'Aksi Cepat Tanggap',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235375/pilar/partners/ldgg38vk4htbjqo1lfqc.jpg'
      },
      {
        id: '5fdbb981b76903000479e3b2',
        name: 'Palang Merah Indonesia',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235392/pilar/partners/utoaqyclvyoubpiz8o4m.png'
      },
      {
        id: '5fdbb99bb76903000479e3b3',
        name: 'PKPU',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235418/pilar/partners/ryd63chtf6wns3mdkgfx.jpg'
      },
      {
        id: '5fdbb9beb76903000479e3b4',
        name: 'BNI Syariah',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235452/pilar/partners/czau55oho7ujc9mh1x2f.png'
      },
      {
        id: '5fdbb9d9b76903000479e3b5',
        name: 'Bank Muamalat',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235480/pilar/partners/yz6mmw8qvgys4nyq2zu2.jpg'
      },
      {
        id: '5fdbb9f6b76903000479e3b6',
        name: 'BTN Syariah',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235509/pilar/partners/qjlnjafism0sj3zogbbt.png'
      },
      {
        id: '5fdbba09b76903000479e3b7',
        name: 'Mandiri Syariah',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235527/pilar/partners/a1kvuozsbvvvlkyrufsk.png'
      },
      {
        id: '5fdbba27b76903000479e3b8',
        name: 'Cluttons Consultant',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235557/pilar/partners/nakjnuvgno5jersvvzta.png'
      },
      {
        id: '5fdbba35b76903000479e3b9',
        name: 'Wiratman',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235572/pilar/partners/soyobpvoozigcsosw0ha.jpg'
      },
      {
        id: '5fdbba45b76903000479e3ba',
        name: 'Provalindo Nusa',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235588/pilar/partners/kgndywiogxa85ntoyttd.png'
      },
      {
        id: '5fdbba5db76903000479e3bb',
        name: 'Quantity Surveyor Indonesia',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235611/pilar/partners/uhsbddmvsobhm39e9aj1.png'
      },
      {
        id: '5fdbba71b76903000479e3bc',
        name: 'LTR',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235632/pilar/partners/vyzyf3wg9psgaaeeiazk.png'
      },
      {
        id: '5fdbba8ab76903000479e3bd',
        name: 'Baganusa Dayaprima',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235657/pilar/partners/uletutrrzoz4zf1o3qrf.png'
      },
      {
        id: '5fdbbaa5b76903000479e3be',
        name: 'Gamma Beta Alpha Consultant',
        imageUrl:
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608235682/pilar/partners/iro3itia2mg3klsq6e1u.jpg'
      }
    ]
  },
  message: ''
}
export default dummyPartnerContent

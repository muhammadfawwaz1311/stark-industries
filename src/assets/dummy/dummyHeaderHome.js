const dummyHeaderHome = {
  data: {
    id: '5fc8668159f6ee0004c7136c',
    page: 'home',
    imageUrl:
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234370/pilar/headers/wyiezxtxhmimy0veekfb.jpg',
    eng: {
      contents: [
        {
          title: 'Stark Construction',
          description:
            '<p>Credible, build solid and the best construction&nbsp;</p>'
        },
        {
          title: 'Stark Development ',
          description: '<p>Developing a harmonious and advanced environment</p>'
        },
        {
          title: 'Stark Property',
          description:
            '<p>With high quality standards providing the best service</p>'
        }
      ]
    },
    ina: {
      contents: [
        {
          title: 'Stark Construction',
          description:
            '<p>Credible, build solid and the best construction&nbsp;</p>'
        },
        {
          title: 'Stark Development ',
          description: '<p>Developing a harmonious and advanced environment</p>'
        },
        {
          title: 'Stark Property',
          description:
            '<p>With high quality standards providing the best service</p>'
        }
      ]
    }
  },
  message: ''
}
export default dummyHeaderHome

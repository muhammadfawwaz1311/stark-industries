import { StarkIndustriesCircle } from 'assets/images'

const dummyFooterContent = {
  data: {
    logoImageUrl: StarkIndustriesCircle,
    detailAddress: 'Stark Industries',
    province: 'New York',
    street: 'Los Angeles ',
    subDistrict: 'California',
    zipcode: '12435'
  },
  message: ''
}
export default dummyFooterContent

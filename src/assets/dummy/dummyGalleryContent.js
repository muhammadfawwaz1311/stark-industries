const dummyGalleryContent = {
  data: {
    albums: [
      {
        id: '5fdbca7eb76903000479e432',
        imageUrls: [
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239739/pilar/managements/lqmc84yoid7coupavvxy.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239739/pilar/managements/cuqrte9qyi4c9o6xcdkq.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728979/pilar/managements/jeanzjpoyloovlmxregr.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728980/pilar/managements/wbewfztli0jh5b4yojuw.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728988/pilar/managements/xpew9uyfrtplmhpkkuf8.jpg'
        ],
        createdAt: '2022-07-18T21:15:42.209Z',
        ina: {
          title: 'Tinjauan Proyek',
          description: '<p><br data-mce-bogus="1"></p>'
        },
        eng: {
          title: 'Project Visit ',
          description: '<p><br data-mce-bogus="1"></p>'
        }
      },
      {
        id: '5fdbca3cb76903000479e431',
        imageUrls: [
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239674/pilar/managements/ijoy5hcp4towjh2ymcnr.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239674/pilar/managements/z5t5qskoh72dtygfgk3j.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728033/pilar/managements/klqnamzwoffthhyg5dia.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728034/pilar/managements/siiwrxuvx2fqnse2pdhd.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728042/pilar/managements/j0pjfycgzosgwtmfep28.jpg'
        ],
        createdAt: '2022-07-18T21:14:36.645Z',
        ina: {
          title: 'Kegiatan Berbagi dari Stark Industries ',
          description: '<p><br data-mce-bogus="1"></p>'
        },
        eng: {
          title: 'Kegiatan Berbagi dari Stark Industries',
          description: '<p><br data-mce-bogus="1"></p>'
        }
      },
      {
        id: '5fdbca0fb76903000479e430',
        imageUrls: [
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239628/pilar/managements/yekntxl1rnjr1trlotih.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239628/pilar/managements/sutcxxbvgto8palgcffs.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239628/pilar/managements/zos60zftxehvgwgz8in6.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239629/pilar/managements/qj3ev70rcwvpkxqamwhc.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728337/pilar/managements/ye3tgwinxecp3fgwt48o.jpg'
        ],
        createdAt: '2022-07-18T21:13:51.568Z',
        ina: {
          title: 'Liputan dan Award dari Housing Estate ',
          description: '<p>&nbsp;</p>'
        },
        eng: {
          title: 'Featured and Awarded by Housing Estate',
          description: '<p><br data-mce-bogus="1"></p>'
        }
      }
    ]
  },
  pagination: { page: 1, per: 3, totalCount: 3, totalPages: 1 },
  message: ''
}
export default dummyGalleryContent

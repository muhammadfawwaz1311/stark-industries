const dummyPrefaceContent = {
  data: {
    id: '5fd26020ceb52500042ee264',
    name: 'Tony Stark',
    imageUrl:
      'https://static.wikia.nocookie.net/marvelcinematicuniverse/images/3/35/IronMan-EndgameProfile.jpg',
    ina: {
      position: 'Chairman',
      preface:
        '<p>Kami meyakini bahwa hidup ini adalah pengabdian dan kemanfaatan. Stark Industries adalah sarana yang kami pilih dalam pengabdian dan kemanfaatan tersebut sehingga visi perusahaan selalu sejalan dengan keyakinan tersebut. Kedalaman pemikiran dan kepahaman untuk mengoptimalkan manfaat perusahaan mendorong kami untuk berusaha terus menerus meningkatkan profesionalitas.</p><p>Tantangan bisnis kedepan semakin besar, perusahaan tanpa nilai yang kokoh akan hanyut dan keluar dari tujuan awalnya. Untuk itulah sejak didirikan, perusahaan selalu berpegang kepada nilai kebenaran, berbuat yang terbaik, amanah, harmoni, bijaksana dan ikhlas. Hal tersebut merupakan nilai-nilai perusahaan yang mengokohkan keyakinan kami dalam menghadapi persaingan.</p><p>Kami berharap tujuan mulia ini dapat kami realisasikan sebagai pengabdian dan kemanfaatan sebesar besarnya untuk seluruh <em>stakeholder</em> perusahaan.</p>'
    },
    eng: {
      position: 'Chairman',
      preface:
        "<p>We believe that life is a devotion and making impacts. Stark Industries is the vehicle we have chosen to put our dedication and providing benefits so the company's vision always in line with our beliefs. Profound thinking and knowledge in optimizing the company usefulness are the factors that encourage us to continuously improve our professionalism.&nbsp;</p><p>The business landscape is getting more challenging, a company without a virtuous value will lose its ways from the original goals. Because of that, since its establishment, the company has always adhered to the value of truth, doing the best, trustworthy, harmonious, wise, and sincere. These are company values that strengthen our confidence in facing competition.</p><p>We hope that our noble goals can be achieved as a form of dedication and profuse benefits for company stakeholders.</p>"
    }
  },
  message: ''
}

export default dummyPrefaceContent

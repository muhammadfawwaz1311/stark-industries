const dummyGalleryHome = {
  data: {
    albums: [
      {
        id: '5fdbca7eb76903000479e432',
        imageUrls: [
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239739/pilar/managements/lqmc84yoid7coupavvxy.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608239739/pilar/managements/cuqrte9qyi4c9o6xcdkq.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728979/pilar/managements/jeanzjpoyloovlmxregr.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728980/pilar/managements/wbewfztli0jh5b4yojuw.jpg',
          'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608728988/pilar/managements/xpew9uyfrtplmhpkkuf8.jpg'
        ],
        createdAt: '2022-07-18T21:15:42.209Z',
        ina: {
          title: 'Tinjauan Proyek',
          description: '<p><br data-mce-bogus="1"></p>'
        },
        eng: {
          title: 'Project Visit ',
          description: '<p><br data-mce-bogus="1"></p>'
        }
      }
    ]
  },
  pagination: { page: 1, per: 1, totalCount: 3, totalPages: 3 },
  message: ''
}
export default dummyGalleryHome

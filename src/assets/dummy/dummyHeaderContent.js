const dummyHeaderContent = {
  data: {
    id: '5fc866b959f6ee0004c71370',
    page: 'about',
    imageUrl:
      'https://res.cloudinary.com/dkoezs7u5/image/upload/v1608234429/pilar/headers/fu4a6mpytnsfg8wyxaso.jpg',
    eng: {
      contents: [
        {
          title: '<p>Believe, is where it all started</p>',
          description: '<p>Believe, is where it all started</p>'
        }
      ]
    },
    ina: {
      contents: [
        {
          title: '<p>Believe, is where it all started</p>',
          description: '<p>Believe, is where it all started</p>'
        }
      ]
    }
  },
  message: ''
}

export default dummyHeaderContent

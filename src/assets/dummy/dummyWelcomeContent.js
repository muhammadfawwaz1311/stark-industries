const dummyWelcomeContent = {
  data: {
    id: '5fd7114495bf5a000469a782',
    code: 'welcome',
    ina: {
      title: 'STARK INDUSTRIES',
      description:
        '<p>Dengan pengalaman puluhan tahun dan didukung oleh tim yang profesional dibidangnya, Kami telah mengerjakan berbagai jenis proyek konstruksi dan pembangunan properti dengan kualitas pelayanan dan hasil yang maksimal sehingga mewujudkan nilai bagi konsumen dan stakeholder kami.&nbsp;</p>'
    },
    eng: {
      title: 'STARK INDUSTRIES',
      description:
        '<p>With decades of experience and supported by a team of professionals in their field, we have worked on various types of construction and property development projects with maximum service quality and results so as to create value for our consumers and stakeholders.</p>'
    }
  },
  message: ''
}
export default dummyWelcomeContent

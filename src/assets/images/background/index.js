import HomeBackground from './Background-Home.jpg'
import AboutBackground from './Background-About.jpg'
import BusinessBackground from './Background-Business.jpg'
import CareerBackground from './Background-Career.jpg'
import ContactBackground from './Background-Contact.jpg'
import GalleryBackground from './Background-Gallery.jpg'
import NewsBackground from './Background-News.jpg'
import NewsDetailBackground from './Background-NewsDetail.jpg'
import ProjectBackground from './Background-Project.jpg'
import PilarConstruction from './Background-PilarConstruction.jpg'
import PilarDevelopment from './Background-PilarDevelopment.jpg'
import CareerContactCard from './Background-CareerContact.jpg'
import ProjectHomeCard from './Background-ProjectHome.jpg'
import CSRBackground from './Background-CSR.jpg'

export {
  HomeBackground,
  AboutBackground,
  BusinessBackground,
  CareerBackground,
  ContactBackground,
  GalleryBackground,
  NewsBackground,
  NewsDetailBackground,
  ProjectBackground,
  PilarConstruction,
  PilarDevelopment,
  CareerContactCard,
  ProjectHomeCard,
  CSRBackground
}

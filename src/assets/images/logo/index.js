import PilarLogo from './Pilar-Logo.svg'
import PilarLogoWhite from './Pilar-Logo-White.svg'
import PilarLogoLandscape from './Pilar-Logo-Landscape.png'
import StarkIndustriesLogo from './Stark-Industries-Logo.png'
import StarkIndustriesCircle from './Stark-Industries-Circle.png'

export {
  PilarLogo,
  PilarLogoWhite,
  PilarLogoLandscape,
  StarkIndustriesLogo,
  StarkIndustriesCircle
}

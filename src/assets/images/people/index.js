import DummyPeople1 from './dummyPeople1.jpg'
import DummyPeople2 from './dummyPeople2.jpg'
import DummyPeople3 from './dummyPeople3.jpg'
import DummyPeople4 from './dummyPeople4.jpg'

export { DummyPeople1, DummyPeople2, DummyPeople3, DummyPeople4 }

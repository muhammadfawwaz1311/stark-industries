import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'
import './libraries/i18n'

// Style
import 'assets/styles/index.css'
// Style

ReactDOM.render(<App />, document.getElementById('root'))

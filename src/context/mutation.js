import * as types from './types'

import { setLanguage } from 'utils'

const Mutation = (state, action) => {
  const { data } = action

  return {
    [types.CHANGE_LANGUAGE]: () => {
      setLanguage(data)
      return {
        ...state,
        language: data
      }
    }
  }
}

export { Mutation }

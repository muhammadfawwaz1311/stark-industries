import React, { useReducer, useContext } from 'react'
import PropTypes from 'prop-types'
import { checkLanguage } from 'utils'
import { Mutation } from './mutation'

const Context = React.createContext()
const INITIAL_STATE = {
  language: 'ENG'
}

const useLanguageReducer = (reducer, defaultState) => {
  const hookVars = useReducer(reducer, defaultState, (initState) => {
    return { language: checkLanguage() }
  })
  return hookVars
}

const Reducer = (state, action) => {
  const { type } = action

  const mutation = Mutation(state, action)
  return mutation[type]()
}

const LanguageProvider = (props) => {
  const { children } = props
  const LanguageStateContext = INITIAL_STATE
  return (
    <Context.Provider value={useLanguageReducer(Reducer, LanguageStateContext)}>
      {children}
    </Context.Provider>
  )
}

LanguageProvider.propTypes = {
  children: PropTypes.node.isRequired
}

const LanguageContext = () => useContext(Context)

export { LanguageContext, LanguageProvider }

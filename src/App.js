import React, { Suspense, useEffect, useState, useRef } from 'react'
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  withRouter,
  Switch
} from 'react-router-dom'
import { Layout } from 'antd'
import { PublicRoutes } from 'routes'
import {
  PilarNavbar,
  PilarFooter,
  PilarSidebar,
  ScrollIntoView
} from 'components'
import 'assets/styles/index.css'
import 'assets/styles/mobile.css'
import 'assets/styles/tablet.css'
import 'assets/styles/desktop.css'
import { LanguageProvider } from 'context'
// import { StarkIndustriesLogo } from 'assets'

export const PublicRoute = (props) => {
  const { location, path } = props
  if (location.pathname === '/' || path === '*') return <Redirect to='/home' />

  return <Route {...props} />
}

const { Content } = Layout
const AppStackRoute = withRouter(({ history }) => {
  const [SidebarCollapse, setSidebarCollapse] = useState(true)
  const [deviceWidth, setDeviceWidth] = useState(window.innerWidth)
  const [device, setDevice] = useState('')
  const [loadingPage, setLoadingPage] = useState(true)
  window.directTo = history

  const pageRef = useRef(null)

  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      setTimeout(() => {
        setLoadingPage(false)
      }, 1000)
    }

    return () => {
      subscribe = false
    }
  }, [])

  useEffect(() => {
    function handleResize() {
      setDeviceWidth(window.innerWidth)
      let device = 'Mobile'
      if (window.innerWidth > 768) {
        device = 'Desktop'
      } else if (window.innerWidth > 600) {
        device = 'Tablet'
      } else {
        device = 'Mobile'
      }
      setDevice(device)
      localStorage.setItem('device', device)
    }

    window.addEventListener('resize', handleResize)
    handleResize()

    return () => window.removeEventListener('resize', handleResize)
  }, [deviceWidth])

  const onSidebarCollapse = () => {
    setSidebarCollapse(!SidebarCollapse)
  }

  const RenderSidebar = () => {
    if (device === 'Mobile' || device === 'Tablet') {
      return (
        <PilarSidebar
          sidebarCollapse={SidebarCollapse}
          showSidebar={() => onSidebarCollapse()}
        />
      )
    }
    return null
  }

  if (!loadingPage) {
    return (
      <Layout className='layout-wrapper'>
        <PilarNavbar
          sidebarCollapse={SidebarCollapse}
          showSidebar={() => onSidebarCollapse()}
        />
        <ScrollIntoView pageRef={pageRef} />
        <div ref={pageRef} className='layout-content-wrapper'>
          <Layout style={{ flex: 1 }}>
            <RenderSidebar />
            <Layout className='layout-content'>
              <Content>
                <Suspense
                  fallback={
                    <div className='loading-page'>
                      {/* <img
                        src={StarkIndustriesLogo}
                        alt='Stark Loading Page'
                      ></img> */}
                    </div>
                  }
                >
                  <Switch>
                    {PublicRoutes.map((route) => (
                      <PublicRoute key={route} {...route} />
                    ))}
                  </Switch>
                </Suspense>
                <PilarFooter />
              </Content>
            </Layout>
          </Layout>
        </div>
      </Layout>
    )
  } else {
    return (
      <div className='loading-page'>
        {/* <img src={StarkIndustriesLogo} alt='Stark Loading Page'></img> */}
      </div>
    )
  }
})

const App = () => {
  return (
    <LanguageProvider>
      <Router>
        <AppStackRoute />
      </Router>
    </LanguageProvider>
  )
}

export default App

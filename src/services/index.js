import axios from 'axios'
import config from './config'
import LanguageService from './language'

const client = axios.create(config.api)

const HeaderService = {
  getHeaderContent(page) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/headers?page=${page}`
    })
  }
}

const SEOService = {
  getSEOContent(type) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/seos?type=${type}`
    })
  }
}

const FooterService = {
  getFooterContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/footers`
    })
  }
}

const LandingPageServices = {
  getIntroductionContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/landing_page_texts/welcome`
    })
  },
  getBusinessContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/landing_page_texts/business`
    })
  },
  getAboutUsTextContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/landing_page_texts/aboutUs`
    })
  },
  getAboutUsImagesContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/landing_page_images/aboutUs`
    })
  },
  getProjectImagesContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/landing_page_images/project`
    })
  },
  getCompanyObjective() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/company_objectives`
    })
  },
  getPartnerList() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/partners?per=0`
    })
  },
  getPerformanceContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/landing_page_images/performance`
    })
  },
  getSummaryContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/landing_page_summaries/progress`
    })
  }
}

const PrefaceService = {
  getPrefaceContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/prefaces`
    })
  }
}

const ManagementService = {
  getManagementList(type) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/managements?type=${type}`
    })
  }
}

const BusinessService = {
  getBusinessList() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/businesses`
    })
  },
  getBusinessNameList() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/businesses/get_names`
    })
  }
}

const ProjectService = {
  getProjectList(page, per, status, orderBy, dir) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/projects?page=${page}&per=${per}&status=${status}&orderBy=${orderBy}&dir=${dir}`
    })
  },
  getFeaturedProjectList(page, per, business) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/projects?page=${page}&per=${per}&business=${business}&type=featured`
    })
  },
  getAllFeaturedProjectList(page, per, business, orderBy, dir) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/projects?page=${page}&per=${per}&business=${business}&orderBy=${orderBy}&dir=${dir}&type=featured`
    })
  },
  getProjectSearch(page, per, search, lang) {
    lang = lang.toLowerCase()
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/projects?page=${page}&per=${per}&keyword=${search}&lang=${lang}`
    })
  }
}

const GalleryService = {
  getGalleryList(page, per) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/albums?page=${page}&per=${per}`
    })
  }
}

const ContactService = {
  getContactContent() {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/contacts`
    })
  }
}

const CareerService = {
  getRecruitmentList(page, per) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/open_recruitments?page=${page}&per=${per}`
    })
  },
  getTestimoniesList(page, per) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/employee_testimonies?page=${page}&per=${per}`
    })
  }
}

const NewsService = {
  getNewsList(page, per) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/news?page=${page}&per=${per}`
    })
  },
  getNewsDetail(id) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/news/${id}`
    })
  },
  getNewsAnother(id) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/news/${id}/another`
    })
  },
  getNewsSearch(page, per, search, lang) {
    lang = lang.toLowerCase()
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/news?page=${page}&per=${per}&keyword=${search}&lang=${lang}`
    })
  }
}

const CSRService = {
  getCSRList(page, per, orderBy, dir) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/csrs?page=${page}&per=${per}&orderBy=${orderBy}&dir=${dir}`
    })
  },
  getCSRDetail(id) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/csrs/${id}`
    })
  },
  getCSRAnother(id) {
    return client.request({
      method: 'get',
      url: `${config.api.baseURL}/api/csrs/${id}/another`
    })
  }
}

const FormServices = {
  sendMessage(data) {
    return client.request({
      method: 'post',
      url: `${config.api.baseURL}/api/contact_buckets`,
      data
    })
  }
}

export {
  LanguageService,
  HeaderService,
  FooterService,
  LandingPageServices,
  PrefaceService,
  ManagementService,
  BusinessService,
  ProjectService,
  GalleryService,
  ContactService,
  CareerService,
  NewsService,
  CSRService,
  FormServices,
  SEOService
}

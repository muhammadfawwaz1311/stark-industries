import * as types from './types'

const changeLanguage = async (dispatch, data) => {
  dispatch({
    type: types.CHANGE_LANGUAGE,
    data: data
  })
}

const LanguageService = { changeLanguage }

export default LanguageService

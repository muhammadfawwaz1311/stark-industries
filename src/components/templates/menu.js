export const MENU = [
  {
    key: 'home',
    header: false
  },
  {
    key: 'about',
    header: true
  },
  {
    key: 'business',
    header: true
  },
  // {
  //   key: 'project',
  //   header: true
  // },
  {
    key: 'album',
    header: true
  },
  {
    key: 'contact',
    header: true
  },
  {
    key: 'career',
    header: true
  },
  // {
  //   key: 'news',
  //   header: true
  // },
  // {
  //   key: 'csr',
  //   header: true
  // }
]

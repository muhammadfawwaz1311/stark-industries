import React from 'react'
import { useLocation } from 'react-router-dom'
import { Layout, Menu, Row, Col, Select, Input } from 'antd'
import { goTo, checkDevice } from 'utils'
import { StarkIndustriesLogo, UKFlag, IndoFlag, SearchIcon } from 'assets'
import { SearchOutlined, MenuOutlined, CloseOutlined } from '@ant-design/icons'
import { MENU } from '../menu'
import './navbar.css'
import { useTranslation } from 'react-i18next'
import { LanguageContext } from 'context'
import { LanguageService } from 'services'

const { Header } = Layout
const { Option } = Select
const NavStyle = { color: '#fff' }
const ImageStyle = { cursor: 'pointer', width: '100%' }
const languageStyle = { verticalAlign: 'middle' }
const flagStyle = {
  width: '1rem',
  marginRight: '10px'
}
const SearchInputStyle = { borderRadius: '5px' }
const MenuIconStyle = { color: '#fff', fontSize: '1.5em' }

const PilarNavbar = (props) => {
  const currentPage = useLocation().pathname.replace('/', '')
  const [languageContext, dispatchLanguage] = LanguageContext()
  const { sidebarCollapse, showSidebar } = props
  const { t, i18n } = useTranslation()
  const currentLanguage = i18n.language

  const handleClickMenu = (selectedMenu, state) => {
    const params = {
      location: `/${selectedMenu}`,
      state: state
    }
    goTo(params)
  }

  const handleSearch = (value) => {
    // handleClickMenu('search', { type: 'project', searchValue: value })
    console.log('Search Page')
  }

  const handleLanguange = async (languageKey) => {
    await i18n.changeLanguage(languageKey)
    let newLanguageContext = languageContext.language
    newLanguageContext = languageKey
    LanguageService.changeLanguage(dispatchLanguage, newLanguageContext)
  }

  const LanguangeMenu = () => {
    return (
      <Select
        className='pilar-select'
        style={{ width: '100%' }}
        bordered={false}
        showArrow={true}
        value={currentLanguage}
        onChange={(value) => handleLanguange(value)}
      >
        <Option value='ENG'>
          <img style={flagStyle} src={UKFlag} alt='ENG' />
          <span style={languageStyle}>ENG</span>
        </Option>
        <Option value='INA'>
          <img style={flagStyle} src={IndoFlag} alt='INA' />
          <span style={languageStyle}>INA</span>
        </Option>
      </Select>
    )
  }

  const RenderMenu = () => {
    switch (checkDevice()) {
      case 'Mobile':
        return <RenderMobileMenu />
      case 'Tablet':
        return <RenderTabletMenu />
      default:
        return <RenderDesktopMenu />
    }
  }

  const RenderMenuIcon = () => {
    let collapse = sidebarCollapse
    if (collapse) {
      return <MenuOutlined />
    }
    return <CloseOutlined />
  }

  const RenderMobileMenu = () => {
    return (
      <Row>
        <Col
          flex='100px'
          className='logo'
          onClick={() => {
            handleClickMenu('home')
          }}
        >
          <img
            style={ImageStyle}
            src={StarkIndustriesLogo}
            alt='StarkIndustriesLogo'
          />
        </Col>
        <Col flex='auto'>
          <Row gutter={[16]} style={{ justifyContent: 'flex-end' }}>
            <Col flex='50px'>
              <LanguangeMenu />
            </Col>
            <Col style={MenuIconStyle} onClick={() => showSidebar()}>
              <RenderMenuIcon />
            </Col>
          </Row>
        </Col>
        <Col span={24}>
          <Input
            allowClear
            placeholder={t('project.searchPlaceholder')}
            prefix={<SearchOutlined />}
            style={SearchInputStyle}
            onPressEnter={(event) => handleSearch(event.target.value)}
            disabled={currentPage === 'search'}
          />
        </Col>
      </Row>
    )
  }

  const RenderTabletMenu = () => {
    return (
      <Row gutter={[16]}>
        <Col
          flex='100px'
          className='logo'
          onClick={() => {
            handleClickMenu('home')
          }}
        >
          <img
            style={ImageStyle}
            src={StarkIndustriesLogo}
            alt='StarkIndustriesLogo'
          />
        </Col>
        <Col flex='auto'>
          <Input
            allowClear
            placeholder={t('project.searchPlaceholder')}
            prefix={<SearchOutlined />}
            style={SearchInputStyle}
            onPressEnter={(event) => handleSearch(event.target.value)}
            disabled={currentPage === 'search'}
          />
        </Col>
        <Col flex='50px'>
          <LanguangeMenu />
        </Col>
        <Col style={MenuIconStyle} onClick={() => showSidebar()}>
          <RenderMenuIcon />
        </Col>
      </Row>
    )
  }

  const RenderDesktopMenu = () => {
    return (
      <Row>
        <Col
          flex='100px'
          className='logo'
          onClick={() => {
            handleClickMenu('home')
          }}
        >
          <img
            style={ImageStyle}
            src={StarkIndustriesLogo}
            alt='StarkIndustriesLogo'
          />
        </Col>
        <Col flex='auto'>
          <Row>
            <Col flex='auto'>
              <Menu
                className='pilar-menu'
                mode='horizontal'
                selectedKeys={currentPage}
                style={NavStyle}
                onClick={({ key }) => {
                  handleClickMenu(key)
                }}
              >
                {MENU.filter((menu) => menu.header).map((menuItem) => {
                  return (
                    <Menu.Item key={menuItem.key}>
                      {t(`menu.${menuItem.key}`)}
                    </Menu.Item>
                  )
                })}
              </Menu>
            </Col>
            <Col flex='100px' className='language'>
              <LanguangeMenu />
            </Col>
            <Col
              className='language'
              onClick={() => {
                // handleClickMenu('search', { type: 'project', searchValue: '' })
                console.log('Search Page')
              }}
            >
              <img
                style={{ ...ImageStyle, ...flagStyle }}
                src={SearchIcon}
                alt='SearchIcon'
              />
            </Col>
          </Row>
        </Col>
      </Row>
    )
  }

  return (
    <Header className='pilar-header'>
      <RenderMenu />
    </Header>
  )
}

export default PilarNavbar

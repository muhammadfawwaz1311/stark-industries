import React, { useState, useEffect } from 'react'
import { Layout, Row, Col, Divider, Button, Typography } from 'antd'
import { PilarLoading } from 'components'
import { useTranslation } from 'react-i18next'
import dummyFooterContent from 'assets/dummy/dummyFooterContent'

const { Footer } = Layout
const { Text } = Typography
const FooterContentWrapStyle = { color: '#fff', textAlign: 'left' }
const FooterContentStyle = { paddingBottom: '10px' }
const dividerStyle = { borderTop: '1px solid #fff', margin: '1rem 0' }

const PilarFooter = () => {
  const { t } = useTranslation()

  const [footerContent, setFooterContent] = useState({})
  const [loadingFooter, setLoadingFooter] = useState(false)
  const [errorFooter, setErrorFooter] = useState(false)
  const [copywright] = useState(
    'Copyright 2022 Stark Industries. All Rights Reserved'
  )

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe) {
      getFooterContent()
    }

    return () => {
      subscribe = false
    }
  }, [])
  // Initial

  const getFooterContent = async () => {
    setLoadingFooter(true)
    try {
      if (dummyFooterContent.data) {
        let data = dummyFooterContent.data
        setFooterContent(data)
        setErrorFooter(false)
      } else {
        setErrorFooter(true)
      }
    } catch (error) {
      setErrorFooter(true)
    }
    setLoadingFooter(false)
  }

  const joinAddress = (addressArr) => {
    let separator = ', '
    addressArr = addressArr.filter(function (address) {
      return address
    })
    return addressArr.join(separator)
  }

  const RenderLoadingFooter = () => {
    let dummyLoading = [0, 1, 2]
    return (
      <Footer className='pilar-footer'>
        <Row align='center' gutter={[16]}>
          <Col span={20}>
            {dummyLoading.map((item) => (
              <div
                key={item}
                style={
                  item < dummyLoading.length - 1
                    ? { marginBottom: '20px' }
                    : null
                }
              >
                <PilarLoading type='black' height='20px' borderRadius='5px' />
              </div>
            ))}
          </Col>
          <Col span={4}>
            <PilarLoading type='black' height='100px' borderRadius='10px' />
          </Col>
        </Row>
        <Divider style={dividerStyle} />
        <Row style={{ color: '#E7B12E' }}>
          <Col span={24}>
            <PilarLoading type='black' height='20px' borderRadius='5px' />
          </Col>
        </Row>
      </Footer>
    )
  }

  const RenderErrorFooter = () => {
    return (
      <div className='PilarErrorLoadingWrapper'>
        <RenderLoadingFooter />
        <div className='PilarErrorLoading'>
          <Button
            className='PilarButton'
            shape='round'
            loading={loadingFooter}
            onClick={() => getFooterContent()}
          >
            <Text>{t('button.reload')}</Text>
          </Button>
        </div>
      </div>
    )
  }

  const RenderFooter = () => {
    if (errorFooter) {
      return <RenderErrorFooter />
    } else if (!loadingFooter && !errorFooter) {
      return (
        <Footer className='pilar-footer'>
          <Row align='middle'>
            <Col xs={0} md={20} lg={22}>
              {footerContent.detailAddress ? (
                <Row style={FooterContentStyle}>
                  <Text style={FooterContentWrapStyle}>
                    {footerContent.detailAddress}
                  </Text>
                </Row>
              ) : null}
              {footerContent.street ||
              footerContent.subDistrict ||
              footerContent.district ? (
                <Row style={FooterContentStyle}>
                  <Text style={FooterContentWrapStyle}>
                    {joinAddress([
                      footerContent.street,
                      footerContent.subDistrict,
                      footerContent.district
                    ])}
                  </Text>
                </Row>
              ) : null}
              {footerContent.city ||
              footerContent.province ||
              footerContent.zipcode ? (
                <Row style={FooterContentStyle}>
                  <Text style={FooterContentWrapStyle}>
                    {joinAddress([
                      footerContent.city,
                      footerContent.province,
                      footerContent.zipcode
                    ])}
                  </Text>
                </Row>
              ) : null}
            </Col>
            <Col xs={0} md={4} lg={2} style={{ textAlign: 'center' }}>
              {footerContent.logoImageUrl ? (
                <div className='PilarLogo'>
                  <img
                    src={footerContent.logoImageUrl}
                    alt='PilarLogoWhite'
                    width='100%'
                  />
                </div>
              ) : null}
            </Col>
            <Col xs={24} md={0} lg={0} style={{ textAlign: 'center' }}>
              <table>
                <tbody>
                  {footerContent.logoImageUrl ? (
                    <tr>
                      <td></td>
                      <td
                        rowSpan={6}
                        width='30%'
                        style={{ verticalAlign: 'top' }}
                      >
                        <div className='PilarLogo'>
                          <img
                            src={footerContent.logoImageUrl}
                            alt='PilarLogoWhite'
                            width='100%'
                          />
                        </div>
                      </td>
                    </tr>
                  ) : null}
                  {footerContent.detailAddress ? (
                    <tr style={FooterContentStyle}>
                      <td style={FooterContentWrapStyle}>
                        <Text style={FooterContentWrapStyle}>
                          {footerContent.detailAddress}
                        </Text>
                      </td>
                    </tr>
                  ) : null}
                  {footerContent.street ? (
                    <tr style={FooterContentStyle}>
                      <td style={FooterContentWrapStyle}>
                        <Text style={FooterContentWrapStyle}>
                          {footerContent.street}
                        </Text>
                      </td>
                    </tr>
                  ) : null}
                  {footerContent.subDistrict ? (
                    <tr style={FooterContentStyle}>
                      <td style={FooterContentWrapStyle}>
                        <Text style={FooterContentWrapStyle}>
                          {footerContent.subDistrict}
                        </Text>
                      </td>
                    </tr>
                  ) : null}
                  {footerContent.district ? (
                    <tr style={FooterContentStyle}>
                      <td style={FooterContentWrapStyle}>
                        <Text style={FooterContentWrapStyle}>
                          {footerContent.district}
                        </Text>
                      </td>
                    </tr>
                  ) : null}
                  {footerContent.city ||
                  footerContent.province ||
                  footerContent.zipcode ? (
                    <tr style={FooterContentStyle}>
                      <td style={FooterContentWrapStyle}>
                        <Text style={FooterContentWrapStyle}>
                          {joinAddress([
                            footerContent.city,
                            footerContent.province,
                            footerContent.zipcode
                          ])}
                        </Text>
                      </td>
                    </tr>
                  ) : null}
                </tbody>
              </table>
            </Col>
          </Row>
          <Divider style={dividerStyle} />
          <Row>
            <Col xs={0} md={24} lg={24} style={{ textAlign: 'left' }}>
              <Text style={{ color: '#E7B12E' }}>{copywright}</Text>
            </Col>
            <Col xs={24} md={0} lg={0} style={{ textAlign: 'center' }}>
              <Text style={{ color: '#E7B12E' }}>{copywright}</Text>
            </Col>
          </Row>
        </Footer>
      )
    } else {
      return <RenderLoadingFooter />
    }
  }

  return <RenderFooter />
}

export default PilarFooter

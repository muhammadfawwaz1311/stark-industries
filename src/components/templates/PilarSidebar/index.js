import React from 'react'
import { useLocation } from 'react-router-dom'
import { Layout, Menu } from 'antd'
import { goTo } from 'utils'
import { MENU } from '../menu'
import '../PilarNavbar/navbar.css'
import { useTranslation } from 'react-i18next'

const { Sider } = Layout

const NavStyle = { color: '#fff' }

const PilarSidebar = (props) => {
  const currentPage = useLocation().pathname.replace('/', '')
  const { sidebarCollapse, showSidebar } = props

  const { t } = useTranslation()

  const handleClickMenu = (selectedMenu) => {
    const params = {
      location: `/${selectedMenu}`
    }
    showSidebar()
    goTo(params)
  }

  const RenderMenu = () => {
    return (
      <Menu
        className='pilar-menu'
        mode='inline'
        selectedKeys={currentPage}
        style={NavStyle}
        onClick={({ key }) => {
          handleClickMenu(key)
        }}
      >
        {MENU.map((menuItem) => {
          return (
            <Menu.Item key={menuItem.key}>
              {t(`menu.${menuItem.key}`)}
            </Menu.Item>
          )
        })}
      </Menu>
    )
  }

  return (
    <div
      className={`pilar-sidebar-wrapper ${!sidebarCollapse ? 'active' : ''}`}
    >
      <Sider
        className='pilar-sidebar'
        collapsible
        collapsedWidth={0}
        width='100%'
        collapsed={sidebarCollapse}
        trigger={null}
      >
        <RenderMenu />
      </Sider>
    </div>
  )
}

export default PilarSidebar

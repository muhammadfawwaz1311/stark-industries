import React from 'react'
import { PilarLoadingPage } from 'assets'

const PilarSplashScreen = () => {
  return (
    <div className='splash-screen-page'>
      <img src={PilarLoadingPage} alt='Pilar Loading Page'></img>
    </div>
  )
}

export default PilarSplashScreen

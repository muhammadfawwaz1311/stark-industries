import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const PilarLoadingListStyled = styled.div`
  background: #777;
  &.animate {
    animation: shimmer 2s infinite linear;
    background: linear-gradient(to right, #eff1f3 4%, #e2e2e2 25%, #eff1f3 36%);
    background-size: 1000px 100%;
  }

  @keyframes shimmer {
    0% {
      background-position: -1000px 0;
    }
    100% {
      background-position: 1000px 0;
    }
  }
`

const PilarLoadingCardStyled = styled.div`
  width: 100%;
  background: #777;
  &.animate {
    animation: shimmer 2s infinite linear;
    background: linear-gradient(to right, #eff1f3 4%, #e2e2e2 25%, #eff1f3 36%);
    background-size: 1000px 100%;
  }

  @keyframes shimmer {
    0% {
      background-position: -1000px 0;
    }
    100% {
      background-position: 1000px 0;
    }
  }
`

const PilarLoadingBlackStyled = styled.div`
  background: #ccc;
  &.animate {
    animation: shimmer 2s infinite linear;
    background: linear-gradient(
      to right,
      rgba(204, 204, 204, 1) 5%,
      rgba(204, 204, 204, 0.5) 100%,
      rgba(204, 204, 204, 0.75) 100%
    );
    background-size: 1000px 100%;
  }

  @keyframes shimmer {
    0% {
      background-position: -1000px 0;
    }
    100% {
      background-position: 1000px 0;
    }
  }
`

const PilarLoading = (props) => {
  const { type, children, height, borderRadius } = props

  const RenderLoadingList = ({ height, borderRadius }) => {
    return (
      <PilarLoadingListStyled
        className='animate'
        style={{ height: height, borderRadius: borderRadius }}
      />
    )
  }

  const RenderLoadingCard = ({ height, borderRadius }) => {
    return (
      <PilarLoadingCardStyled
        className='animate'
        style={{ height: height, borderRadius: borderRadius }}
      />
    )
  }

  const RenderLoadingBlack = ({ height, borderRadius }) => {
    return (
      <PilarLoadingBlackStyled
        className='animate'
        style={{ height: height, borderRadius: borderRadius }}
      />
    )
  }

  const RenderLoadingImage = ({ height, borderRadius }) => {
    return (
      <PilarLoadingBlackStyled
        className='animate'
        style={{
          height: height,
          width: height,
          borderRadius: borderRadius,
          marginLeft: 'auto',
          marginRight: 'auto'
        }}
      />
    )
  }

  const RenderLoadingCustom = () => {
    return <div>{children}</div>
  }

  const RenderShimmer = () => {
    if (type === 'list')
      return <RenderLoadingList height={height} borderRadius={borderRadius} />
    else if (type === 'card')
      return <RenderLoadingCard height={height} borderRadius={borderRadius} />
    else if (type === 'black')
      return <RenderLoadingBlack height={height} borderRadius={borderRadius} />
    else if (type === 'image')
      return <RenderLoadingImage height={height} borderRadius={borderRadius} />
    else return <RenderLoadingCustom />
  }

  return (
    <div>
      <RenderShimmer />
    </div>
  )
}

PilarLoading.propTypes = {
  type: PropTypes.string,
  children: PropTypes.node,
  height: PropTypes.string,
  borderRadius: PropTypes.string
}

PilarLoading.defaultProps = {
  type: 'image',
  height: '10px',
  borderRadius: '5px'
}

export default PilarLoading

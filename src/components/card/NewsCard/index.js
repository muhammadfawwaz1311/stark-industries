import React from 'react'
import PropTypes from 'prop-types'
import { Typography, Image } from 'antd'
import { textTranslate, convertDate } from 'utils'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const NewsHeaderTitleStyle = { marginBottom: '1em' }
const NewsDateStyle = { color: '#A6A6A6' }
const NewsContentStyle = {
  color: '#565656',
  textAlign: 'left',
  marginBottom: '1em'
}
const NewsCardWrapper = {
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  height: '100%'
}

const NewsCard = (props) => {
  const [{ language }] = LanguageContext()
  const { data } = props
  const { t } = useTranslation()

  return (
    <div className='news-card-wrapper'>
      <div style={NewsHeaderTitleStyle} className='news-card-image-wrapper'>
        <Image src={data.imageUrl} alt='' />
      </div>
      <div style={NewsCardWrapper}>
        <div style={{ flex: 1 }}>
          <Title level={4} style={TitleStyle}>
            {textTranslate(
              language,
              data?.eng?.title ? data?.eng?.title : '',
              data?.ina?.title ? data?.ina?.title : ''
            )}
          </Title>
        </div>
        <div>
          <Text style={NewsDateStyle}>
            {`${t('text.uploaded')} ${convertDate(data.createdAt)}`}
          </Text>
        </div>
        <div style={NewsContentStyle}>
          <Text className='trimString'>
            <div
              dangerouslySetInnerHTML={{
                __html: textTranslate(
                  language,
                  data?.eng?.body ? data?.eng?.body : '',
                  data?.ina?.body ? data?.ina?.body : ''
                )
              }}
            ></div>
          </Text>
        </div>
      </div>
    </div>
  )
}

export default NewsCard

NewsCard.propTypes = {
  data: PropTypes.object
}

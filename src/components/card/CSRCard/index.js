import React from 'react'
import PropTypes from 'prop-types'
import { Typography, Card, Image } from 'antd'
import { textTranslate, convertDate, goTo } from 'utils'
import { LanguageContext } from 'context'
import { useTranslation } from 'react-i18next'
import { RightOutlined } from '@ant-design/icons'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const CSRRedirectStyle = { cursor: 'pointer', paddingTop: '1em' }
const CSRCardWrapper = {
  display: 'flex',
  flexDirection: 'column',
  flex: 1,
  height: '100%'
}
const CSRDateStyle = {
  color: '#A6A6A6'
}
const CSRDividerStyle = {
  borderBottom: '1px dashed #B2B2B2',
  paddingBottom: '1em'
}

const CSRCard = (props) => {
  const { t } = useTranslation()
  const [{ language }] = LanguageContext()
  const { data } = props

  const RedirectPage = (url) => {
    let param = {
      location: `/csr/detail/${url}`
    }
    goTo(param)
  }

  return (
    <div key={data.id} className='csr-card-wrapper'>
      <Card
        hoverable
        style={{ height: '100%' }}
        bodyStyle={{ ...CSRCardWrapper, padding: 0 }}
      >
        <div className='csr-card-image-wrapper'>
          <Image src={data.imageUrl} alt='' />
        </div>
        <div style={{ ...CSRCardWrapper, padding: '1em' }}>
          <div style={{ flex: 1 }}>
            <Title level={4} style={TitleStyle}>
              {textTranslate(
                language,
                data?.eng?.title ? data?.eng?.title : '',
                data?.ina?.title ? data?.ina?.title : ''
              )}
            </Title>
          </div>
          <div style={CSRDividerStyle}>
            <Text style={CSRDateStyle}>{`${t('text.uploaded')} ${convertDate(
              data.createdAt
            )}`}</Text>
          </div>
          <div
            style={CSRRedirectStyle}
            onClick={() => {
              RedirectPage(data.id)
            }}
          >
            <Text style={{ color: '#e7b12e' }}>{t('button.seeAll')}</Text>
            <RightOutlined style={{ color: '#e7b12e', marginLeft: '0.5em' }} />
          </div>
        </div>
      </Card>
    </div>
  )
}

export default CSRCard

CSRCard.propTypes = {
  data: PropTypes.object
}

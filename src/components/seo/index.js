import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { SEOService } from 'services'
import { Helmet } from 'react-helmet'
import { trimString } from 'utils'

const PilarSEO = ({ type }) => {
  // SEO Content
  const [seoContent, setSEOContent] = useState({})
  // SEO Content

  // Initial
  useEffect(() => {
    let subscribe = true

    if (subscribe && type) {
      getSEOContent(type)
    }

    return () => {
      subscribe = false
    }
  }, [type])
  // Initial

  const getSEOContent = async (type) => {
    try {
      const getSEOContent = await SEOService.getSEOContent(type)
      if (getSEOContent && getSEOContent.status === 200) {
        let data = getSEOContent?.data?.data
        setSEOContent(data)
      } else {
        console.log('SEO Error')
      }
    } catch (error) {
      console.log('SEO Error')
    }
  }

  return (
    <Helmet>
      <title>{seoContent?.title}</title>
      <meta
        name='description'
        content={trimString(seoContent?.description, 150)}
      />
      <meta name='keywords' content={seoContent?.keywords} />
      <meta name='robots' content='index,follow' />
      <meta name='url' content={`https://www.pilarcorporation.com/${type}`} />
      <meta
        name='identifier-URL'
        content={`https://www.pilarcorporation.com/${type}`}
      />
    </Helmet>
  )
}

PilarSEO.propTypes = {
  type: PropTypes.string
}

export default PilarSEO

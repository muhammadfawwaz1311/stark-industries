import { useEffect } from 'react'
import { useLocation } from 'react-router-dom'

export default function ScrollToTop({ pageRef }) {
  const { pathname } = useLocation()

  useEffect(() => {
    if (pageRef) {
      pageRef?.current?.scrollTo(0, 0)
    }
  }, [pathname, pageRef])

  return null
}

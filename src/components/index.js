import PilarLoading from './loading'
import PilarSplashScreen from './splashScreen'
import ScrollIntoView from './scrollIntoView'
import PilarSEO from './seo'
export * from './templates'
export * from './slider'
export * from './card'

export { PilarLoading, PilarSplashScreen, ScrollIntoView, PilarSEO }

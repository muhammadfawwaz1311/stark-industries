import React, { useRef } from 'react'
import Swiper from 'react-id-swiper'
import 'swiper/css/swiper.css'

const centerGalleryApi = {
  spaceBetween: 30,
  lazy: true,
  centeredSlides: true,
  slidesPerView: 1.75,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false
  }
}

const PilarCenterSlider = (props) => {
  const { gallery } = props

  const gallerySwiperRef = useRef()

  return (
    <div style={{ height: '100%' }}>
      <Swiper
        {...centerGalleryApi}
        ref={gallerySwiperRef}
        containerClass='swiper-container PilarCenterSliderClass'
      >
        {gallery.map((galleryItem, galleryIndex) => {
          return (
            <img
              key={galleryIndex}
              src={galleryItem}
              alt=''
              className='PilarCenterImageClass'
            />
          )
        })}
      </Swiper>
    </div>
  )
}

export default PilarCenterSlider

import React, { useRef } from 'react'
import PropTypes from 'prop-types'
import { Image } from 'antd'
import Swiper from 'react-id-swiper'
import 'swiper/css/swiper.css'

const PilarThumb = (props) => {
  const { gallery, galleryApi, thumbApi } = props

  const gallerySwiperRef = useRef()

  const handleThumbClick = (index) => {
    gallerySwiperRef.current.swiper.slideTo(index)
  }

  return (
    <div>
      <Swiper
        {...galleryApi}
        ref={gallerySwiperRef}
        containerClass='pilar-thumbnail-wrapper swiper-container'
      >
        {gallery?.map((galleryItem, galleryIndex) => {
          return galleryItem ? (
            <Image
              key={galleryIndex}
              src={galleryItem}
              alt={galleryIndex}
              className='pilar-thumbnail-image'
            />
          ) : (
            <div key={galleryIndex}></div>
          )
        })}
      </Swiper>
      {gallery.length > 1 ? (
        <div style={{ marginTop: '1em' }}>
          <Swiper
            {...thumbApi}
            containerClass='pilar-thumb-wrapper swiper-container'
          >
            {gallery?.map((thumbItem, thumbIndex) => {
              return thumbItem ? (
                <img
                  key={thumbIndex}
                  src={thumbItem}
                  alt={thumbIndex}
                  className='pilar-thumb-image'
                  onClick={() => handleThumbClick(thumbIndex)}
                />
              ) : (
                <div key={thumbIndex}></div>
              )
            })}
          </Swiper>
        </div>
      ) : null}
    </div>
  )
}

export default PilarThumb

PilarThumb.propTypes = {
  gallery: PropTypes.array,
  galleryApi: PropTypes.object,
  thumbApi: PropTypes.object
}

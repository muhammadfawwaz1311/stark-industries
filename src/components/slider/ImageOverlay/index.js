import './imageOverlay.css'
import React from 'react'
import { Row, Col, Typography } from 'antd'
import { textTranslate } from 'utils'
import { LanguageContext } from 'context'

const { Text } = Typography
const TitleStyle = { fontWeight: 'bold', color: '#fff' }

const ImageOverlay = (props) => {
  const { ImageData } = props
  const [{ language }] = LanguageContext()

  return (
    <div className='PilarImageOverlayWrapper'>
      <img className='PilarImageOverlay' src={ImageData.portraitImage} alt='' />
      <Row className='PilarImageOverlayContent'>
        <Col span={24}>
          <div className='homeLogo-card-image-wrapper '>
            <img src={ImageData.logo} alt='' />
          </div>
        </Col>
        <Col span={24}>
          <Text className='PilarImageOverlayText' style={TitleStyle}>
            {textTranslate(
              language,
              ImageData?.eng?.name,
              ImageData?.ina?.name
            )}
          </Text>
        </Col>
      </Row>
    </div>
  )
}

export default ImageOverlay

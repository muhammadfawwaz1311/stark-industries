import React, { useRef } from 'react'
import PropTypes from 'prop-types'
import { Row, Col, Card, Typography } from 'antd'
import { checkDevice, textTranslate } from 'utils'
import { LanguageContext } from 'context'
import Swiper from 'react-id-swiper'
import 'swiper/css/swiper.css'
import './pilarMultiContentSlider.css'

const { Title, Text } = Typography
const TitleStyle = { fontWeight: 'bold', fontFamily: 'PT Serif' }
const NewsContentStyle = {
  color: '#565656',
  textAlign: 'justify'
}
const CardBodyStyle = { padding: 0, height: '100%', width: '100%', flex: 1 }

const centerGalleryApi = {
  spaceBetween: 30,
  lazy: true,
  centeredSlides: true,
  slidesPerView: checkDevice() === 'Desktop' ? 2 : 1,
  autoplay: {
    delay: 5000,
    disableOnInteraction: false
  },
  navigation: {
    nextEl: '.swiper-button-next.PilarNavigation',
    prevEl: '.swiper-button-prev.PilarNavigation'
  }
}

const PilarMultiContentSlider = (props) => {
  const { gallery } = props
  const [{ language }] = LanguageContext()

  const gallerySwiperRef = useRef()

  return (
    <div style={{ height: '100%' }}>
      <Swiper
        {...centerGalleryApi}
        ref={gallerySwiperRef}
      >
        {gallery?.map((galleryItem, galleryIndex) => {
          return (
            <Card
              key={galleryIndex}
              bodyStyle={CardBodyStyle}
              style={{ height: '100%' }}
            >
              <Row gutter={16} style={{ height: '100%' }}>
                <Col span={12}>
                  <div className='PilarMultiContentWrapper'>
                    <img src={galleryItem?.imageUrls[0]} alt='' />
                  </div>
                </Col>
                <Col span={12}>
                  <Row style={{ padding: '0.5em' }}>
                    <Col span={24}>
                      <Title level={5} style={TitleStyle}>
                        {textTranslate(
                          language,
                          galleryItem?.eng?.title,
                          galleryItem?.ina?.title
                        )}
                      </Title>
                    </Col>
                    <Col span={24} style={NewsContentStyle}>
                      <Text className='trimString-5'>
                        <div
                          dangerouslySetInnerHTML={{
                            __html: textTranslate(
                              language,
                              galleryItem?.eng?.description,
                              galleryItem?.ina?.description
                            )
                          }}
                        ></div>
                      </Text>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Card>
          )
        })}
      </Swiper>
    </div>
  )
}

export default PilarMultiContentSlider

PilarMultiContentSlider.propTypes = {
  gallery: PropTypes.array
}

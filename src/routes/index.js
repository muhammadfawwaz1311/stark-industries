import {
  HomePage,
  AboutPage,
  BusinessPage,
  CareerPage,
  ContactPage,
  GalleryPage,
  NewsPage,
  NewsDetailPage,
  ProjectPage,
  CSRPage,
  CSRDetailPage,
  SearchPage
} from 'pages'

const PublicRoutes = [
  {
    name: '',
    exact: true,
    path: '/'
  },
  {
    name: 'Home',
    component: HomePage,
    exact: true,
    path: '/home'
  },
  {
    name: 'About',
    component: AboutPage,
    exact: true,
    path: '/about'
  },
  {
    name: 'Business',
    component: BusinessPage,
    exact: true,
    path: '/business'
  },
  {
    name: 'Project',
    component: ProjectPage,
    exact: true,
    path: '/project'
  },
  {
    name: 'Gallery',
    component: GalleryPage,
    exact: true,
    path: '/album'
  },
  {
    name: 'Contact',
    component: ContactPage,
    exact: true,
    path: '/contact'
  },
  {
    name: 'Career',
    component: CareerPage,
    exact: true,
    path: '/career'
  },
  {
    name: 'News',
    component: NewsPage,
    exact: true,
    path: '/news'
  },
  {
    name: 'NewsDetail',
    component: NewsDetailPage,
    exact: true,
    path: '/news/detail/:paramId'
  },
  {
    name: 'CSR',
    component: CSRPage,
    exact: true,
    path: '/csr'
  },
  {
    name: 'CSRDetail',
    component: CSRDetailPage,
    exact: true,
    path: '/csr/detail/:paramId'
  },
  {
    name: 'Search',
    component: SearchPage,
    exact: true,
    path: '/search'
  },
  {
    name: 'Not Found',
    component: HomePage,
    exact: true,
    path: '*'
  }
]

export { PublicRoutes }
